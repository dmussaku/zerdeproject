__VERSION__ = 'z0.0.40'
# import os
# import sys
from setuptools import setup, find_packages
# from setuptools import Command


install_requires = []
with open('requires.txt', 'r') as fh:
    install_requires = map(lambda s: s.strip(), filter(
        lambda l: not l.startswith('-e'), fh.readlines()))

tests_requires = []
with open('requires-test.txt', 'r') as fh:
    tests_requires = map(lambda s: s.strip(), filter(
        lambda l: not l.startswith('-e'), fh.readlines()))

readme = []
with open('README.md', 'r') as fh:
    readme = fh.readlines()


# class RunTests(Command):
#     """From django-celery"""
#     description = "Run the django test suite from the tests dir."
#     user_options = []
#     extra_env = {}

#     def run(self):
#         if self.distribution.install_requires:
#             self.distribution.fetch_build_eggs(
#                 self.distribution.install_requires)
#         if self.distribution.tests_require:
#             self.distribution.fetch_build_eggs(self.distribution.tests_require)

#         for env_name, env_value in self.extra_env.items():
#             os.environ[env_name] = str(env_value)

#         this_dir = os.getcwd()
#         testproj_dir = os.path.join(this_dir, 'synplan')
#         os.chdir(testproj_dir)
#         sys.path.append(testproj_dir)

#         os.environ['DJANGO_SETTINGS_MODULE'] = os.environ.get(
#             'DJANGO_SETTINGS_MODULE', 'synplan.settings')
#         os.environ['DJANGO_CONFIGURATION'] = os.environ.get(
#             'DJANGO_CONFIGURATION', 'TestSettings')

#         from configurations.management import execute_from_command_line
#         execute_from_command_line([__file__, 'test'])

#     def initialize_options(self):
#         pass

#     def finalize_options(self):
#         pass

setup(
    name='synplan',
    version=__VERSION__,
    author='German Ilyin',
    author_email='germanilyin@gmail.com',
    url='http://bitbucket.org/yun_man_ger/zerplan',
    description='A synplan.kz site',
    long_description=''.join(readme),
    packages=find_packages(),
    zip_safe=False,
    install_requires=install_requires,
    tests_require=tests_requires,
    # extras_require={'test': tests_requires},
    # cmdclass={"test": RunTests},
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'synplan = synplan.runner:main'
        ]
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Operating System :: OS Independent',
        'Topic :: Software Development',
    ],
)
