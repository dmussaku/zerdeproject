# Welcome to Synplan

# MONGODB DATABASE INSTALLATION

# refer to 
# http://docs.mongodb.org/manual/administration/install-on-linux/
## Ubuntu
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
sudo bash -c "echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' > /etc/apt/sources.list.d/10gen.list"
sudo apt-get update
sudo apt-get install mongodb-10gen
sudo locale-gen en_US en_US.UTF-8


# SYNPLAN INSTALLATION

sudo apt-get -y install build-essential python python-dev python-setuptools libxml2-dev libxslt-dev
sudo easy_install virtualenv
virtualenv ~/synplan
cd ~/synplan
. bin/activate
pip install ~/synplan-x.x.x.tar.gz
pip install uwsgi supervisor

# SYNPLAN FIRST TIME INIT
cd ~/synplan
. bin/activate
synplan init
synplan collectstatic
mkdir logs

# SYNPLAN CONFIGURATION
To override default config edit this file ``~/.synplan/synplan.conf.py``. Common configs:
 
 + DEBUG = False
 + SYNERGY_HOST = "http://test-s.v3na.kz:8080"
 + SYNERGY_MODULE_ID = 'external_planning'

# SYNPLAN START
cd ~/synplan
. bin/activate
supervisord -c supervisor.conf

# SYNPLAN STOP
cd ~/synplan
. bin/activate
supervisorctl -c supervisor.conf shutdown

# SYNPLAN VERSION UPDATE
cd ~/synplan
. bin/activate
pip install ~/synplan-x.x.x.tar.gz
synplan collectstatic
