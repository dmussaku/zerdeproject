#!/bin/bash

is_mongodb=$(docker ps | grep mongodb | wc -l)

if [[ $is_mongodb = 0 ]] ;then
	mkdir -p `pwd`/../mount/mongodb/data
	MONGODB=$(docker run -d -p=27017:27017 -p=2222:22 -v `pwd`/../mount/mongodb/data:/data/db lmdb)
	echo "MONGODB" $MONGODB
fi
