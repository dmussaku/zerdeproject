#!/bin/sh
#
# init.d script with LSB support.
#
# Copyright (c) 2007 Javier Fernandez-Sanguino <jfs@debian.org>
#
# This is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2,
# or (at your option) any later version.
#
# This is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License with
# the Debian operating system, in /usr/share/common-licenses/GPL;  if
# not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307 USA
#
### BEGIN INIT INFO
# Provides:          synplan
# Required-Start:    $network $local_fs $remote_fs mongodb
# Required-Stop:     $network $local_fs $remote_fs
# Should-Start:      $named
# Should-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: synplan project
# Description:       synplan.com web site
### END INIT INFO

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
PROJECT_DIR=/home/kvv/synplan/synplan
DAEMON=${PROJECT_DIR}/bin/supervisord
CTLDAEMON=${PROJECT_DIR}/bin/supervisorctl
DESC=project

NAME=synplan
# Defaults.  Can be overridden by the /etc/default/$NAME
# Other configuration options are located in $CONF file. See here for more:
PIDFILE=${PROJECT_DIR}/logs/supervisord.pid
CONF=${PROJECT_DIR}/supervisor.conf

OLD_PWD=`pwd`

# Include defaults if available
if [ -f /etc/default/$NAME ] ; then
    . /etc/default/$NAME
fi

if test ! -x $DAEMON; then
    echo "Could not find $DAEMON"
    exit 0
fi

. $PROJECT_DIR/bin/activate

STARTTIME=1
DIETIME=10                   # Time to wait for the server to die, in seconds
                            # If this value is set too low you might not
                            # let some servers to die gracefully and
                            # 'restart' will not work
cd $PROJECT_DIR
DAEMONUSER=${DAEMONUSER:-kvv}
DAEMON_OPTS=${DAEMON_OPTS:-"-c $CONF -j $PIDFILE"}
CTLDAEMON_OPTS=${CTLDAEMON_OPTS:-"-c $CONF"}

set -e

running_pid() {
# Check if a given process pid's cmdline matches a given name
    pid=$1
    name=$2
    [ -z "$pid" ] && return 1
    [ ! -d /proc/$pid ] &&  return 1
    cmd=$(basename `cat /proc/$pid/cmdline | tr "\000" "\n"| sed -n '2p'`)
    # echo "DEBUG: "$cmd
    # Is this the expected server
    [ "$cmd" != "supervisord" ] &&  return 1
    return 0
}

running() {
# Check if the process is running looking at /proc
# (works for all users)
    # No pidfile, probably no daemon present
    [ ! -f "$PIDFILE" ] && return 1
    pid=`cat $PIDFILE`
    running_pid $pid $DAEMON || return 1
    return 0
}

start_server() {
# Start the process using the wrapper
            su -c "$DAEMON $DAEMON_OPTS" $DAEMONUSER
            errcode=$?
    return $errcode
}

stop_server() {
# Stop the process using the wrapper
            su -c "$CTLDAEMON $CTLDAEMON_OPTS shutdown" $DAEMONUSER
            errcode=$?
    return $errcode
}

restart_server(){
            su -c "$CTLDAEMON $CTLDAEMON_OPTS restart all" $DAEMONUSER
    return $?
}

force_stop() {
# Force the process to die killing it manually
    [ ! -e "$PIDFILE" ] && return
    if running ; then
        kill -15 $pid
    # Is it really dead?
        sleep "$DIETIME"s
        if running ; then
            kill -9 $pid
            sleep "$DIETIME"s
            if running ; then
                echo "Cannot kill $NAME (pid=$pid)!"
                exit 1
            fi
        fi
    fi
    rm -f $PIDFILE
}


case "$1" in
  start)
    echo "Starting $DESC" "$NAME"
        # Check if it's running first
        if running ;  then
            echo "apparently already running"
            echo 0
            exit 0
        fi
        if start_server ; then
            # NOTE: Some servers might die some time after they start,
            # this code will detect this issue if STARTTIME is set
            # to a reasonable value
            [ -n "$STARTTIME" ] && sleep $STARTTIME # Wait some time 
            if  running ;  then
                # It's ok, the server started and is running
                echo 0
            else
                # It is not running after we did start
                echo 1
            fi
        else
            # Either we could not start it
            echo 1
        fi
    ;;
  stop)
        echo "Stopping $DESC" "$NAME"
        if running ; then
            # Only stop the server if we see it running
            errcode=0
            stop_server || errcode=$?
            echo $errcode
        else
            # If it's not running don't do anything
            echo "apparently not running"
            echo 0
            exit 0
        fi
        ;;
  force-stop)
        # First try to stop gracefully the program
        $0 stop
        if running; then
            # If it's still running try to kill it more forcefully
            echo "Stopping (force) $DESC" "$NAME"
            errcode=0
            force_stop || errcode=$?
            echo $errcode
        fi
    ;;
  restart|force-reload)
        echo "Restarting $DESC" "$NAME"
        errcode=0
        restart_server || errcode=$?
        [ -n "$STARTTIME" ] && sleep $STARTTIME
        running || errcode=$?
        echo $errcode
    ;;
  status)

        echo "Checking status of $DESC" "$NAME"
        if running ;  then
            su -c "$CTLDAEMON $CTLDAEMON_OPTS status" $DAEMONUSER
            echo 0
        else
            echo "apparently not running"
            echo 1
            exit 1
        fi
        ;;
  reload)
        echo "Reloading $NAME daemon: not implemented, as the daemon"
        echo "cannot re-read the config file (use restart)."
        ;;

  *)
    N=/etc/init.d/$NAME
    echo "Usage: $N {start|stop|force-stop|restart|force-reload|status}" >&2
    exit 1
    ;;
esac
cd $OLD_PWD
exit 0

