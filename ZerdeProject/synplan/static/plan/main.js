

var app = angular.module('myApp', [
    'ngGrid', 'ui.bootstrap', 'angularTreeview',
    'restangular', 'ngProgress', 'ngCookies',
    'ui.select2', 'angularFileUpload', 'xeditable']);
//'ngGroupTable' 'uiSlider'



var error_print = function(error) {
    console.log(error.status);
    console.log(error.data.traceback);
};

app.config(function($httpProvider) {
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
}).config(function(RestangularProvider) {
    RestangularProvider.setBaseUrl('api/v1');
    RestangularProvider.setMethodOverriders(['delete', 'put', 'patch']);
    RestangularProvider.setResponseExtractor(function(response, operation, what, url, fullResponse) {
        var newResponse = null;
        if (operation === "getList") {
            newResponse = response.objects;
            newResponse.metadata = response.meta;
        } else if (operation == "post") {
            var tmp = fullResponse.headers('Location');
            if (tmp) {
                newResponse = {
                    'resource_uri': tmp.replace(/\\/g, '/').replace(/http(s)?:\/\/[^\/]*/, ''),
                    'id': tmp.replace(/\\/g, '/').replace(/.*\//, '')
                };
            }
        } else {
            newResponse = response;
        }
        return newResponse != null && newResponse || response;
    });
    RestangularProvider.setDefaultHeaders({'Content-Type': 'application/json'});
    // RestangularProvider.setRequestSuffix('/?');
    RestangularProvider.setRestangularFields({
        selfLink: 'resource_uri'
    });
    RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
        if (operation === 'put' || operation === 'patch') {
            elem._id = undefined;
            elem.id = undefined;
            return elem;
        }
        if (operation === 'delete' || operation === 'remove') {
            return undefined;
        }
        return elem;
    })
});


app.directive('adjustable', ['$window', function($window) {
        return function($scope, $elem, $attrs) {
            var w = angular.element($window);
            // $scope.getWindowDimensions = function () {
            //   return { 'h': w.height(), 'w': w.width() };
            // };
            // $scope.$watch($scope.getWindowDimensions, function (newValue, oldValue) {

            //   $scope.style = function () {
            //     return {
            //       'height': (newValue.h - 100) + 'px',
            //       'width': (newValue.w - 100) + 'px' 
            //     };
            //   };

            // }, true);
            var $el = angular.element($elem);
            var width_exp = $attrs.adjustWidth,
                    height_exp = $attrs.adjustHeight,
                    adjust_to_selector = $attrs.adjustTo;
            if (adjust_to_selector) {
                var target = angular.element(adjust_to_selector);
            }

            function resizeElement() {
                if (width_exp) {
                    $el.width(eval(width_exp));
                }
                if (height_exp) {
                    $el.height(eval(height_exp));
                }
            }
            ;

            w.bind('resize', function() {
                resizeElement();
            });

            if (adjust_to_selector) {
                target.bind('mousemove', function() {
                    resizeElement();
                });
                setInterval(function() {
                    resizeElement();
                }, 3000);
            }

            resizeElement();
        }
    }]);

app.factory('Synergy', ['Restangular', function(Restangular) {
        return Restangular.withConfig(function(RestangularConfigurer) {
            RestangularConfigurer.setBaseUrl('synergy');
            RestangularConfigurer.setMethodOverriders(['delete', 'put', 'patch']);
            RestangularConfigurer.setResponseExtractor(function(response, operation, what, url, fullResponse) {
                return response;
            });
        });
    }]);

app.filter('reverse', function() {
    return function(items) {
        return items && items.slice().reverse() || items;
    };
});

app.filter('pluck', function() {
    return function(items, fields) {
        return items && _.pluck(items, fields.split(",")) || items;
    };
});

app.filter('join', function() {
    return function(items, sep) {
        sep = sep || ', ';
        return items && items.join(sep) || "";
    };
});

app.filter('o2t', function() {
    return function(items, sep) {
        sep = sep || ', ';
        if (items) {
            var res = [];
            angular.forEach(items, function(value, key) {
                res.push(key + ": " + value);
            });
            return res.join(sep);
        }
        return items;
    };
});

app.filter('ass_duration', ['$filter', 'DateUtils', function($filter, DateUtils) {
        return function(obj) {
            var dates = _.map(obj.dates, function(o) {
                if (obj.period == "once") {
                    if (typeof (o) === "string") {
                        o = new Date(o);
                    }
                    return $filter('date')(o);
                } else if (obj.period == "monthly_days") {
                    if (typeof (o) === "string") {
                        o = _.map(o.split(","), function(x) {
                            return parseInt(x);
                        });
                    }
                    return DateUtils.getWeekLabel(o[0] - 1, o[1] - 1);
                } else if (obj.period == "weekly") {
                    if (typeof (o) === "string") {
                        o = parseInt(o);
                    }
                    return DateUtils.getWeekDayLabel(o - 1);
                } else if (obj.period == "yearly") {
                    if (typeof (o) === "string") {
                        o = _.map(o.split(","), function(x) {
                            return parseInt(x);
                        });
                    }
                    return DateUtils.getMonthDayLabel(o[0] - 1, o[1] - 1);
                }
                return o;
            });
            if (obj.period == 'quarterly') {
                return DateUtils.quartals[obj.dates[0]] + ', ' + obj.dates[1];
            }
            var range = (obj.period == "once" ? "" : ' с ' + $filter('date')(obj.from_date) + ' по ' + $filter('date')(obj.to_date));
            return DateUtils.periods[obj.period] + range + ': ' + dates.join((obj.period === "once" ? " - " : ", "));
        };
    }]);

app.filter('toTree', ['$tree', function($tree) {
        return $tree.build_tree2;
    }]);

app.directive('datepickerManualInput', function() {
    return {
        require: "ngModel",
        link: function($scope, $element, $attrs, ngModel) {
            ngModel.$parsers.unshift(function(data) {
                // convert data from view format to model format
                if (typeof (data) !== "string")
                    return data;
                // dd.mm.yy
                var splitted = data.split('.');
                // mm/dd/yyyy
                var convertedDate = splitted[1] + "/" + splitted[0] + "/" + splitted[2];
                var res = new Date(Date.parse(convertedDate)); // converted
                // console.log(res);
                return res;
            });
        }
    }
});

app.directive('fixUserSelect', function() {
    return {
        require: "ngModel",
        link: function($scope, $element, $attrs, controller) {
            //allows you to access sub-properties with dot notation (data.subproperty)
            function getDescendantProp(obj, desc) {
                var arr = desc.split(".");
                while (arr.length && (obj = obj[arr.shift()])) {
                }
                return obj;
            }

            function findVal(options, val) {
                if (!val)
                    return null;
                var res = null;
                angular.forEach(options, function(option) {
                    if (option.userID === val.userID) {
                        res = option;
                    }
                });
                return res;
            }
            var changeValue = false;
            var optionsProperty = $attrs.fixUserSelect;
            //if any of the options match the value, replace the ngModel value
            function setValueIfSame() {
                if (changeValue) {
                    changeValue = false;
                    return;
                }
                var options = getDescendantProp($scope, optionsProperty);
                var curVal = controller.$viewValue;
                var newVal = null;
                if (angular.isArray(curVal)) {
                    newVal = [];
                    angular.forEach(curVal, function(val) {
                        var index = options.indexOf(val);
                        if (index == -1) {
                            var res = findVal(options, val);
                            if (res) {
                                changeValue = true;
                                newVal.push(res);
                            }
                        } else {
                            newVal.push(val);
                        }
                    });
                } else {
                    changeValue = true;
                    newVal = findVal(options, curVal);
                }
                if (changeValue) {
                    controller.$setViewValue(newVal);
                }
            }

            if ($attrs.ngChosenGroup) {
                var sel = $($element).next().selector + ' li.group-result';
                $(document).on('click', sel, $element, function(ev) {
                    var options = getDescendantProp($scope, optionsProperty);
                    var el = $(ev.data);
                    var curVal = controller.$viewValue || [];
                    var newVal = [];
                    var changeValue = false;
                    angular.forEach(curVal, function(value) {
                        newVal.push(value);
                    });
                    el.find('optgroup[label="' + $(this).text() + '"] option').each(function() {
                        var obj = options[parseInt($(this).attr('value'))];
                        if (obj) {
                            var present = false;
                            for (i = 0; i < curVal.length; i++) {
                                if (curVal[i].userID === obj.userID) {
                                    present = true;
                                    break;
                                }
                            }
                            if (!present) {
                                newVal.push(obj);
                                changeValue = true;
                            }
                        }
                    });
                    if (changeValue) {
                        controller.$setViewValue(newVal);
                        $scope.$digest();
                    }
                }).
                        on('mouseover', sel, $element, function() {
                            $(this).addClass('highlighted').siblings().removeClass('highlighted');
                        }).
                        on('mouseout', sel, $element, function() {
                            $(this).removeClass('highlighted');
                        });
            }

            // setValueIfSame();

            //watch when the options change
            $scope.$watch($attrs.fixUserSelect, setValueIfSame);
            //watch when ng-model changes
            $scope.$watch(function() {
                return controller.$viewValue;
            }, setValueIfSame);
        }
    };
});

app.filter('join_users', ['Users', function(Users) {
        return function(users) {
            var res = [];
            var user_db = Users.user_map;
            if (user_db) {
                angular.forEach(users, function(o) {
                    if (user_db[o]) {
                        res.push(user_db[o].name);
                    } else {
                        res.push(o);
                    }
                });
                return res.join(', ');
            }
            if (angular.isArray(users)) {
                return users.join(', ');
            } else {
                return users;
            }

        }
    }]);

app.filter('username', ['Users', function(Users) {
        return function(user) {
            var user_db = Users.user_map;
            if (user_db[user]) {
                return user_db[user].name;
            }
            return user;
        }
    }]);

app.filter('permissions', [function() {
        return function(perms) {
            var label_map = {
                read: "Чтение",
                write: "Запись",
                perm: "Назначение прав",
            };
            var res = [];
            angular.forEach(perms, function(value, key) {
                if (value) {
                    res.push(label_map[key]);
                }
            });
            return res.join(", ");
        }
    }]);

app.filter('period', ['DateUtils', '$interpolate', function(DateUtils, $interpolate) {
        return function(repeats) {
            if (repeats) {
                var period = repeats[0],
                        dates = repeats[1];
                var res = [];
                var dateFmt = $interpolate("{{val| date}}");
                angular.forEach(dates, function(o) {
                    val = DateUtils.periodDates[period][o];
                    if (val) {
                        res.push(val.label);
                    } else {
                        res.push(dateFmt({val: new Date(o)}));
                    }

                });
                return DateUtils.periods[period] + (res.length > 0 ? ": " + res.join(", ") : "");
            }
        }
    }]);

app.directive('ngChosen', function() {

    return {
        restrict: 'A',
        require: '?ngModel',
        link: function(scope, element, attrs) {
            // scope.$watch(attrs.ngChosen, function () {
            //     element.trigger('liszt:updated');
            //     element.trigger("chosen:updated");
            // });

            scope.$watch(attrs.ngModel, function() {
                element.trigger('liszt:updated');
                element.trigger("chosen:updated");
            });

            element.chosen({
                no_results_text: "Совпадений не найдено",
                search_contains: true
            });

        }
    };
});

app.factory('Nav', ['$cookieStore', function($cookieStore) {
        var $nav = {};
        $nav.get_cookie = function(key, fallback) {
            var val = $cookieStore.get('nav_' + key);
            if (angular.isDefined(val)) {
                return val;
            }
            return fallback;
        };
        $nav.set_cookie = function(key, value) {
            $cookieStore.put('nav_' + key, value);
        };
        $nav.moveNode = function(node, d) {
            var parent_item = node.parent_item;
            if (!parent_item)
                return false;
            var parent_id = parent_item.id;
            var idx = parent_item.items.indexOf(node);
            if (idx == -1)
                return false;
            // var oa2 = a.order[parent_id];
            // var ob2 = a.order[parent_id];

            if ((idx + d * 2) >= 0 && (idx + d * 2) < parent_item.items.length &&
                    parent_item.items[idx + d * 2].node_type == node.node_type &&
                    parent_item.items[idx + d].node_type == node.node_type) {
                var na = parent_item.items[idx + d * 2], nb = parent_item.items[idx + d];
                // console.log(na);
                var oa = na.order[parent_id] || 0;
                var ob = nb.order[parent_id] || 10000;
                node.order[parent_id] = (oa + ob) / 2.0;
            } else if ((idx + d) >= 0 && (idx + d) < parent_item.items.length &&
                    parent_item.items[idx + d].node_type == node.node_type) {
                var na = parent_item.items[idx + d];
                var oa = na.order[parent_id] || 0;
                node.order[parent_id] = oa + 1000 * d;
            } else {
                return false;
            }
            // $nav.set_cookie(node._order_key, node._cached_order);
            parent_item.items.sort($nav.cmpFunc);
            return parent_id;
        };
        $nav.node_type_order = {
            "plan": 0,
            "plan ext": 1,
            "work": 2,
            "filter": 3,
            "folder": 4,
        };
        $nav.cmpFunc = function(a, b) {
            // console.log(a.node_type, b.node_type);
            var oa = $nav.node_type_order[a.node_type];
            var ob = $nav.node_type_order[b.node_type];
            if (oa < ob) {
                return -1;
            }
            if (ob < oa) {
                return 1;
            }
            if (a.node_type == 'folder' && b.node_type == 'folder') {
                return 0;
            }
            var parent_id = (a.parent_item && a.parent_item.id || null);
            if (a.order && b.order && parent_id) {
                var oa2 = a.order[parent_id];
                var ob2 = b.order[parent_id];
                // console.log(a.parent_item.title, a.title, b.title, oa2, ob2);
                if (oa2 < ob2) {
                    return -1;
                }
                if (oa2 > ob2) {
                    return 1;
                }
            }
            if (a.id < b.id) {
                return -1;
            }
            if (a.id > b.id) {
                return 1;
            }
            return 0;
        };
        $nav.build = function(plan_list, skip, select, add_root) {
            var node_map = {};
            var folder_map = {};
            var res = [];
            var cur = null;
            function addToIndex(item) {
                node_map[item.id] = item;
                if (item.node_type === 'folder') {
                    if (item.obj.seq) {
                        folder_map[item.obj.seq] = item;
                    }
                }
            }
            function getParents(item) {
                var result = [];
                if (item.obj.parent_folder_ids) {
                    angular.forEach(item.obj.parent_folder_ids, function(value) {
                        var res = node_map[value];
                        if (res) {
                            result.push(res);
                        }
                    });
                }
                if (item.obj.parent_node_id) {
                    var res = node_map[item.obj.parent_node_id];
                    if (res) {
                        result.push(res);
                    }
                }
                return result;
            }
            for (var i = 0; i < plan_list.length; i++) {
                var obj = plan_list[i];
                var item = {
                    title: obj.title,
                    id: obj.id,
                    node_type: obj.node_type,
                    items: [],
                    collapsed: $nav.get_cookie(obj.id, true),
                    order: obj.order || {},
                    obj: obj
                };
                if (obj.node_type == 'folder' && obj.manager.userID == current_user.extra.userid) {
                    item.collapsed = false;
                }
                if (obj.detailer && obj.detailer.userID) {
                    item.node_type = item.node_type + ' ' + 'ext';
                }
                addToIndex(item);
            }
            for (var i = 0; i < plan_list.length; i++) {
                var item = node_map[plan_list[i].id];
                if (!item) {
                    continue;
                }
                if (skip) {
                    var shall_skip = false;
                    angular.forEach(skip, function(value, key) {
                        if (angular.isArray(value)) {
                            angular.forEach(value, function(skip_value) {
                                if (item.obj[key] == skip_value)
                                    shall_skip = true;
                            });
                        } else if (item.obj[key] == value) {
                            shall_skip = true;
                        }
                    });
                    if (shall_skip) {
                        continue;
                    }
                }
                if (select) {
                    var shall_select = true;
                    angular.forEach(select, function(value, key) {
                        if (item.obj[key] != value)
                            shall_select = false;
                    });
                    if (shall_select) {
                        cur = item;
                        cur.selected = 'selected';
                        cur.collapsed = false;
                    }
                }
                if (!cur) {
                    var pid = $nav.get_cookie('selected');
                    if (item.id === pid && item.node_type != 'work') {
                        cur = item;
                        cur.selected = 'selected';
                        cur.collapsed = false;
                    }
                }
                if (item.node_type === 'folder' && item.obj.child_nodes) {
                    angular.forEach(item.obj.child_nodes, function(value) {
                        var child_item = folder_map[value];
                        if (child_item) {
                            // delete node_map[child_item.id];
                            item.items.push(child_item);
                            child_item.parent_item = item;
                        }
                    });
                    if (!item.obj.parent_node_id || !node_map[item.obj.parent_node_id]) {
                        res.push(item);
                    }
                } else {
                    var parents = getParents(item);
                    if (parents.length) {
                        angular.forEach(parents, function(parent) {
                            parent.items = parent.items || [];
                            var theParent = null;
                            var found = false;
                            if (item.obj.parent_plan_sc) {
                                var objId = 'filter:' + item.obj.parent_plan_sc.id;
                                for (var i = 0; i < parent.items.length; i++) {
                                    if (parent.items[i].id == objId) {
                                        theParent = parent.items[i];
                                        found = true;
                                        break;
                                    }
                                }
                                if (!theParent) {
                                    theParent = {
                                        title: item.obj.parent_plan_sc.title,
                                        id: objId,
                                        node_type: 'filter',
                                        collapsed: $nav.get_cookie(objId, item.collapsed && !item.selected),
                                        items: [item],
                                        obj: {
                                            resource_type: 'filter',
                                            id: objId,
                                        },
                                        order: {},
                                    };
                                } else {
                                    theParent.items.push(item);
                                    item.parent_item = theParent;
                                }
                            }
                            if (found) {
                                // parent.collapsed = parent.collapsed && theParent.collapsed;
                            } else {
                                var resItem = theParent || item;
                                parent.items.push(resItem);
                                resItem.parent_item = parent;
                            }
                        });
                    } else {
                        res.push(item);
                    }
                }
            }
            // delete node_map;
            function dfs(items) {
                var collapsed = true;
                var selected = false;
                angular.forEach(items, function(item, idx) {
                    var parent_id = (item.parent_item && item.parent_item.id || null);
                    if (item.order && !angular.isDefined(item.order[parent_id])) {
                        item.order[parent_id] = (idx + 10) * 1000.0;
                    }
                    if (item.items.length) {
                        var res = dfs(item.items);
                        if (res[1]) {
                            item.collapsed = false;
                        } else if ($nav.get_cookie(item.id, false)) {
                            item.collapsed = true;
                        } else {
                            item.collapsed = res[0] && item.collapsed;
                        }
                        selected = selected || res[1];
                    }
                    selected = selected || !!item.selected;
                    if (selected) {
                        collapsed = false;
                    } else {
                        collapsed = collapsed && !selected && item.collapsed;
                    }
                });
                items.sort($nav.cmpFunc);
                return [collapsed, selected];
            }
            dfs(res);
            var data = res;
            if (add_root) {
                data = [{
                        items: res,
                        obj: {
                            id: null,
                            resource_uri: null,
                        },
                        id: null,
                        title: "Корень"
                    }];
            }
            var result = {
                data: data,
            };
            if (cur) {
                result.currentNode = cur;
            }
            return result;
        };
        return $nav;
    }]);

app.factory('$tree', [function() {
        var $ss = {
            build_tree2: function(plan_list, skip, select, add_root) {
                var node_map = {};
                var res = [];
                var cur = null;
                for (var i = 0; i < plan_list.length; i++) {
                    var obj = plan_list[i];
                    var item = {
                        title: obj.title,
                        id: obj.id,
                        node_type: obj.resource_type,
                        items: [],
                        obj: obj
                    };
                    if (obj.detailer && obj.detailer.userID) {
                        item.node_type = item.node_type + ' ' + 'ext';
                    }
                    node_map[item.id] = item;
                }
                for (var i = 0; i < plan_list.length; i++) {
                    var item = node_map[plan_list[i].id];
                    if (skip) {
                        var shall_skip = false;
                        angular.forEach(skip, function(value, key) {
                            if (angular.isArray(value)) {
                                angular.forEach(value, function(skip_value) {
                                    if (item.obj[key] == skip_value)
                                        shall_skip = true;
                                });
                            } else if (item.obj[key] == value) {
                                shall_skip = true;
                            }
                        });
                        if (shall_skip) {
                            continue;
                        }
                    }
                    if (select) {
                        var shall_select = true;
                        angular.forEach(select, function(value, key) {
                            if (item.obj[key] !== value)
                                shall_select = false;
                        });
                        if (shall_select) {
                            cur = item;
                        }
                    }
                    if (item.obj.parent_node_id && node_map[item.obj.parent_node_id]) {
                        var parent = node_map[item.obj.parent_node_id];
                        parent.items = parent.items || [];
                        parent.items.push(item);
                    } else {
                        res.push(item);
                    }
                }
                delete node_map;
                var data = res;
                if (add_root) {
                    data = [{
                            items: res,
                            obj: {
                                id: null,
                                resource_uri: null,
                            },
                            id: null,
                            title: "Корень"
                        }];
                }
                var result = {
                    data: data,
                };
                if (cur) {
                    result.currentNode = cur;
                } else if (data.length > 0) {
                    result.currentNode = data[0];
                }
                result.currentNode.selected = 'selected';
                return result;
            },
            build_tree: function(o, skip, select, subtree) {
                skip = skip || null;
                if (!o) {
                    return [];
                }
                if (!angular.isArray(o)) {
                    o = [o];
                }
                var res = [];
                var sel = null;
                angular.forEach(o, function(cur) {
                    if (!skip || cur.resource_type != skip) {
                        var items = $ss.build_tree(cur.items, skip, select, subtree);
                        var item = {
                            id: cur.id,
                            selected: (cur.id == select ? 'selected' : ''),
                            resource_type: cur.resource_type,
                            title: cur.title,
                            obj: cur,
                            items: items.data
                        };
                        if (items.currentNode) {
                            sel = items.currentNode;
                        }
                        if (cur.id == select) {
                            sel = item;
                        }
                        if (subtree && cur.id === subtree) {
                            item.items = [];
                        } else {
                            res.push(item);
                        }
                    }
                });
                var result = {
                    data: res
                };
                if (sel) {
                    result.currentNode = sel;
                }
                return result;
            },
            build_data: function(o, pref, depth, parent_obj) {
                //console.log(o);
                pref = pref || '';
                parent_obj = parent_obj || null;
                depth = depth || 0;
                if (!o) {
                    return [];
                }
                if (!angular.isArray(o)) {
                    o = [o];
                }
                var res = [];
                var count = 1;
                angular.forEach(o, function(cur) {
                    obj = angular.copy(cur);
                    delete obj.items;
                    obj.obj = cur;
                    if (cur.period) {
                        obj.repeats = [cur.period, cur.dates];
                    }
                    obj.parent_obj = parent_obj;
                    if (depth == 0) {
                        obj.num = '';
                    } else {
                        obj.num = (pref && depth > 0 ? pref + '.' + count : '' + count);
                        count += 1;
                    }
                    res.push(obj);
                    Array.prototype.push.apply(res, $ss.build_data(cur.items, obj.num, depth + 1, cur));
                });
                return res;
            }
        };
        return $ss;
    }]);

// ------------------------------------------------------------------------Date Utils Started ------------------------------------------

app.factory('DateUtils', [function() {
        var week_list = [
            ['1-ый', '1-ая', '1-ое', ],
            ['2-ой', '2-ая', '2-ое', ],
            ['3-ий', '3-яя', '3-ее', ],
            ['4-ый', '4-ая', '4-ое', ],
            ['5-ый', '5-ая', '5-ое', ],
            ['Последний', 'Последняя', 'Последнее', ],
        ];

        var day_list = [
            ['понедельник', 0],
            ['вторник', 0],
            ['среда', 1],
            ['четверг', 0],
            ['пятница', 0],
            ['суббота', 1],
            ['воскресенье', 2],
        ];
        var month_list = [
            ['Янв', 31],
            ['Фев', 29],
            ['Мар', 31],
            ['Апр', 30],
            ['Мая', 31],
            ['Июн', 30],
            ['Июл', 31],
            ['Авг', 31],
            ['Сен', 30],
            ['Окт', 31],
            ['Ноя', 30],
            ['Дек', 31],
        ];
        var week_day_list = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс', ];
        var $ss = {
            weekDays: function() {
                var res = {};
                for (i = 1; i <= 7; i++) {
                    res[i] = {label: $ss.getWeekDayLabel(i - 1)};
                }
                return res;
            },
            monthDays: function() {
                var res = {};
                for (i = 1; i <= 31; i++) {
                    res[i] = {label: (i < 10 ? "0" : "") + i};
                }
                return res;
            },
            getWeekNum: function(i) {
                return week_list[i][0];
            },
            getWeekLabel: function(i, j) {
                return week_list[i][day_list[j][1]] + ' ' + day_list[j][0];
            },
            getWeekDayLabel: function(i) {
                return week_day_list[i];
            },
            getMonthDayLabel: function(i, j) {
                return (j + 1 < 10 ? "0" : "") + (j + 1) + " " + month_list[i][0];
            },
            monthWeekDays: function() {
                var res = {};
                for (i = 1; i <= 6; i++) {
                    for (j = 1; j <= 7; j++) {
                        res[[i, j]] = {
                            label: $ss.getWeekLabel(i - 1, j - 1),
                            group: $ss.getWeekNum(i - 1)};
                    }
                }
                return res;
            },
            yearDays: function() {
                var res = {};
                for (i = 1; i <= 12; i++) {
                    for (j = 1; j <= month_list[i - 1][1]; j++) {
                        res[[i, j]] = {
                            "label": $ss.getMonthDayLabel(i - 1, j - 1),
                            "group": month_list[i - 1][0],
                        };
                    }
                }
                return res;
            },
            yearQuartals: function() {
                var res = {};
                angular.forEach($ss.quartals, function(value, key) {
                    res[key] = {'label': value, 'group': 'Все'};
                });
                return res;
            },
            periods: {
                "once": "Однократно",
                // "weekly": "Еженедельно", 
                // "monthly": "Ежемесячно по числам",
                // "monthly_days": "Ежемесячно по дням",
                "quarterly": "Квартал",
                // "yearly": "Ежегодно",
            },
            quartals: {
                '0': 'Квартал I',
                '1': 'Квартал II',
                '2': 'Квартал III',
                '3': 'Квартал IV',
            },
            budget_types: {
                '0': 'Не требует финансирования',
                '1': 'Местный бюджет',
                '2': 'Республиканский бюджет',
                '3': 'Смешанные инвестиции',
                '4': 'Инвестиционный проект',
                '5': 'Собственные средства',
                '6': 'По мере разработки мероприятий и формирования бюджетной заявки',
                '7': 'Собственные средства АО «Казахтелеком»',
                '8': 'Собственные средства РГП «Казгидромет»',
                '9': 'Собственные средства операторов сотовой связи',
                '10': 'Собственные средства оператора связи',
                '11': 'Собственные средства сотовых операторов',
                '12': 'Займы БВУ',
                '13': 'Собственные средства  КФ «Фонд развития ИКТ»',
                '14': 'Собственные средства КФ «Фонд развития ИКТ»',
                '15': 'Собственные средства промышленных предприятий',
                '16': 'Собственные средства субъектов естественных монополий',
            },
            event_types: {// Test even_types 
                '0': 'Создание (развитие) информационной системы',
                '1': 'Подготовка Закона',
                '2': 'Проработка вопроса (различные выходные документы, предложения, аналитические записки)',
                '3': 'Выполнение работ',
                '4': 'Проработка вопроса (международный опыт)',
            }

            /*
             $http.get("http://127.0.0.1:8000/plan/api/v1/event_types/?format=json")
             .then(function(results){
             //Success;
             console.log("Succss: " + results.status);
             $scope.event_types = results.data.objects;
             console.log($scope.event_types);
             
             }, function(results){
             //error
             console.log("Error: " + results.data + "; "
             + results.status);
             })
             */

        };
        $ss["periodDates"] = {
            once: {},
            // weekly: $ss.weekDays(),
            // monthly: $ss.monthDays(),
            // monthly_days: $ss.monthWeekDays(),
            // yearly: $ss.yearDays(),
            quarterly: $ss.yearQuartals(),
        };
        // $ss["periodDates"]["quarterly"] = $ss["periodDates"]["yearly"];
        return $ss;
    }]);

//-----------------------------------Date utils ended---------------------------------------------------------------------

app.factory('Users', ['Synergy', '$q', function(Synergy, $q) {
        var deferred = $q.defer();
        var $ss = {
            users: null,
            user_map: null,
            settings: null,
            select2opts: function($scope) {
                return function(group) {
                    return {
                        data: {
                            results: (group ? $ss.groups : $ss.users),
                            text: function(o) {
                                return o.name || o.text;
                            }
                        },
                        id: function(o) {
                            if (o) {
                                return o.userID || o.id;
                            }
                        },
                        formatSelection: function(o) {
                            return o.name || o.text;
                        },
                        formatResult: function(o) {
                            return o.name || o.text;
                        },
                        initSelection: function(element, callback) {
                            var obj = $scope.$eval(element.attr('ng-model-str') || element.attr('ng-model'));
                            var data = [];
                            if (!obj)
                                return;
                            if (angular.isArray(obj)) {
                                angular.forEach(obj, function(val) {
                                    data.push($ss.user_map[val.userID]);
                                });
                            } else {
                                data = $ss.user_map[obj.userID];
                            }
                            callback(data);
                        }
                    };
                };
            },
            load: deferred.promise
        };
        var cnt = 0;
        var total = 3;
        var resolve = function() {
            if (cnt === total) {
                deferred.resolve($ss);
            }
        };
        Synergy.all('grouped_users').getList({}).then(function(result) {
            $ss.users = result;
            $ss.user_map = {};
            $ss.grouped_users = {};
            $ss.groups = [];
            angular.forEach($ss.users, function(value) {
                $ss.user_map[value.userID] = value;
                if (value.group) {
                    $ss.grouped_users[value.group.groupID] = $ss.grouped_users[value.group.groupID] || {
                        children: [],
                        id: value.group.groupID,
                        text: value.group.name
                    };
                    $ss.grouped_users[value.group.groupID].children.push(value);
                }
            });
            angular.forEach($ss.grouped_users, function(value, key) {
                $ss.groups.push(value);
            });
            cnt += 1;
            resolve();
        }, error_print);
        Synergy.one('settings').get().then(function(result) {
            $ss.settings = result;
            cnt += 1;
            resolve();
        });
        Synergy.one('departments').get().then(function(result) {
            $ss.departments = result;
            cnt += 1;
            resolve();
        });
        return $ss;
    }]);

app.filter('toArray', function() {
    'use strict';

    return function(obj) {
        if (!(obj instanceof Object)) {
            return obj;
        }

        return Object.keys(obj).map(function(key) {
            return Object.defineProperty(obj[key], '$key', {__proto__: null, value: key});
        });
    }
});

app.filter('leafDepartments', function() {
    'use strict';

    return function(obj) {
        return _.filter(obj, function(o) {
            return o.manager && o.manager.managerID;
        });
    }
});

// app.filter('flatten', function(){
//     'use strict';

//     return function (obj) {
//       var res = [];
//       function dive(obj, depth){
//         angular.forEach(obj, function(value){
//           var o = _.omit(value, 'subProcesses');
//           o['tab'] = depth;
//           res.push(o);
//           if (angular.isArray(value.subProcesses) && value.subProcesses.length > 0){
//             dive(value.subProcesses, depth + 1);
//           }
//         });
//       }
//       dive(obj, 0);
//       console.log(res, obj);
//       return res;
//     }
// });

app.run(function($rootScope, $templateCache) {
    $rootScope.$on('$viewContentLoaded', function() {
        $templateCache.removeAll();
    });
});

app.run(function run($http, $cookies) {
    // For CSRF token compatibility with Django
    $http.defaults.headers.common['X-CSRFToken'] = $cookies['csrftoken'];
});

app.directive('context', [function() {
        return {
            restrict: 'A',
            scope: '@&',
            compile: function compile(tElement, tAttrs, transclude) {
                return {
                    post: function postLink(scope, iElement, iAttrs, controller) {
                        var attr = null;
                        if (iAttrs.ngEvalAttr) {
                            attr = scope.$eval(iAttrs.ngEvalAttr);
                        } else {
                            attr = iAttrs.ngAttr;
                        }
                        if (!attr)
                            return;
                        var last = null, lastElement = null;
                        // ul.css({ 'display' : 'none'});
                        var open = function(event) {
                            event.preventDefault();
                            var ul = $('#' + iAttrs.context + '_' + attr);
                            $(event.target).trigger('click');
                            ul.css({
                                position: "fixed",
                                display: "block",
                                left: event.clientX + 'px',
                                top: event.clientY + 'px'
                            }).show();
                            last = event.timeStamp;
                            lastElement = $(event.target);
                        };
                        var hide1 = function(event) {
                            var ul = $('#' + iAttrs.context + '_' + attr);
                            var target = $(event.target);
                            if (!target.is(".popover") && !target.parents().is(".popover")) {
                                if (last === event.timeStamp)
                                    return;
                                ul.css({
                                    'display': 'none'
                                }).hide();
                            }
                        };
                        var hide2 = function(event) {
                            var ul = $('#' + iAttrs.context + '_' + attr);
                            var target = $(event.target);
                            if (!target == lastElement && !target.is(".popover") && !target.parents().is(".popover")) {
                                if (last === event.timeStamp)
                                    return;
                                ul.css({
                                    'display': 'none'
                                }).hide();
                            }
                        };
                        $(iElement).on('contextmenu', open);
                        $(document).on('click', hide1);
                        $(document).on('contextmenu', hide2);

                        $(iElement).on('$destroy', function() {
                            $(iElement).off('contextmenu', open);
                            $(document).off('click', hide1);
                            $(document).off('contextmenu', hide2);
                        });

                    }
                };
            }
        };
    }]);

app.factory('SharedState', [function() {
        var $ss = {
            eventDetail: null
        };
        return $ss;
    }]);


app.directive('popoverEventPopup', ['SharedState', function(SharedState) {
        return {
            restrict: 'EA',
            replace: true,
            scope: {title: '@', content: '@', placement: '@', animation: '&', isOpen: '&'},
            templateUrl: 'spt/event-popup.html',
            // template: '<div class="popover">{{ content }}</div>',
            compile: function() {
                return {
                    pre: function(scope, element, attrs) {
                        scope.state = SharedState;
                        scope.synergy_host = synergy_host;
                    }
                }
            }
        };
    }])

        .directive('popoverEvent', ['$tooltip', function($tooltip) {
                return $tooltip('popoverEvent', 'popover', 'click');
            }]);

app.directive('myTable', ['$compile', function($compile) {
        return function(scope, element, attrs) {

            // apply DataTable options, use defaults if none specified by user
            var options = {};
            if (attrs.myTable.length > 0) {
                options = scope.$eval(attrs.myTable);
            } else {
                options = {
                    "bStateSave": true,
                    "iCookieDuration": 2419200, /* 1 month */
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bInfo": false,
                    "bDestroy": true,
                    "bSortable": false,
                    "aTargets": ["sorting_disabled"],
                    "sWidth": "20%",
                    "aTargets": [0, 1, 2, 3, 4, 5, 6]
                };
            }

            // Tell the dataTables plugin what columns to use
            // We can either derive them from the dom, or use setup from the controller           
            var explicitColumns = [];
            // element.find('th').each(function(index, elem) {
            //     explicitColumns.push($(elem).text());
            // });
            if (explicitColumns.length > 0) {
                options["aoColumns"] = explicitColumns;
            } else if (attrs.aoColumns) {
                options["aoColumns"] = scope.$eval(attrs.aoColumns);
            }
            //console.log(attrs.aoColumnDefs);
            // aoColumnDefs is dataTables way of providing fine control over column config
            if (attrs.aoColumnDefs) {
                options["aoColumnDefs"] = scope.$eval(attrs.aoColumnDefs);
                //options["aoColumnDefs"] = {
                //  "bSortable" : false,
                //  "aTargets" : [ "sorting_disabled" ]
                //};
            }

            if (attrs.fnRowCallback) {
                var rowCallback = scope.$eval(attrs.fnRowCallback);
                options["fnRowCallback"] = function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    row = rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull);
                    scope.$index = iDisplayIndex;
                    $compile(row)(scope);
                    return row;
                }
            }

            options["aaData"] = scope.$eval(attrs.aaData);

            // apply the plugin
            var dataTable = element.dataTable(options);



            // watch for any changes to our data, rebuild the DataTable
            scope.$watch(attrs.aaData, function(value) {
                var val = value || null;
                if (val) {
                    dataTable.fnClearTable();
                    dataTable.fnAddData(scope.$eval(attrs.aaData));
                }
            });
        };
    }]);


app.directive('ngValidateFolder', [function() {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {
                function validate(users, the_map) {
                    ngModel.$setValidity('folder', true);
                    if (!angular.isDefined(users)) {
                        return;
                    }
                    if (!angular.isArray(users)) {
                        users = [users];
                    }
                    angular.forEach(users, function(user) {
                        if (!user) {
                            return;
                        }
                        var tx = the_map[user.userID];
                        if (!angular.isDefined(tx) || (angular.isArray(tx) && tx.length == 0)) {
                            ngModel.$setValidity('folder', false);
                        }
                    });
                }
                ;
                scope.$watch(function() {
                    return ngModel.$modelValue;
                }, function(users) {
                    var the_map = scope.$eval(attrs.ngValidateFolder);
                    validate(users, the_map);
                });
                scope.$watch(attrs.ngValidateFolder, function() {
                    var the_map = scope.$eval(attrs.ngValidateFolder);
                    validate(ngModel.$modelValue, the_map);
                });
            }
        };
    }]);



(function() {
    function naturalSort(a, b) {
        if (a && b) {
            var aa = _.map(a.split('.'), parseInt);
            var ab = _.map(b.split("."), parseInt);
            for (var i = 0; i < Math.min(aa.length, ab.length); i++) {
                if (aa[i] < ab[i]) {
                    return -1;
                } else if (aa[i] > ab[i]) {
                    return 1;
                }
            }
            return aa.length - ab.length;
        } else
            return a.localeCompare(b);
    }

    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "paragraph-asc": function(a, b) {
            return naturalSort(a, b);
        },
        "paragraph-desc": function(a, b) {
            return naturalSort(a, b) * -1;
        }
    });

}());