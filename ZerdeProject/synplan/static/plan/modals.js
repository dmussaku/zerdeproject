app.controller('ModalGroupCtrl', ['$scope', '$tree', '$modalInstance', 'plan', 'current',
    function($scope, $tree, $modalInstance, plan, current) {

        $scope.ok = function() {
            $modalInstance.close({
                obj: $scope.group,
                parent_obj: $scope.parent_obj
            });
        };



        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.group = current.obj || {resource_type: "group"};
        $scope.parent_obj = current.parent_obj || plan;
        $scope.group.parent_node = $scope.parent_obj.resource_uri;
        $scope.$watch('groupTree.currentNode', function(newObj, oldObj) {
            if ($scope.groupTree && angular.isObject($scope.groupTree.currentNode)) {
                $scope.group.parent_node = $scope.groupTree.currentNode.obj.resource_uri;
                $scope.parent_obj = $scope.groupTree.currentNode.obj;
            }
        }, false);
        $scope.groupTree = $tree.build_tree(plan, 'event', $scope.parent_obj.id, $scope.group.id);
    }]);

app.controller('ModalFolderCtrl', ['$scope', '$tree', '$modalInstance', 'current', 'SharedState', 'ext_data',
    function($scope, $tree, $modalInstance, current, SharedState, ext_data) {

        $scope.ok = function() {
            $modalInstance.close($scope.result);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.userSelectOptions = ext_data.select2opts($scope);
        $scope.users = ext_data.users;

        $scope.result = current || {parent_node: null, resource_type: "folder"};
        $scope.folderTree = {};
        $scope.folderTree = $tree.build_tree2(
                SharedState.plan_list,
                {
                    resource_type: ['plan', 'work'],
                    id: $scope.result.id
                },
        {id: $scope.result.parent_node_id},
        true
                );
        $scope.$watch('folderTree.currentNode', function(newObj, oldObj) {
            if ($scope.folderTree && angular.isObject($scope.folderTree.currentNode)) {
                $scope.result.parent_node = $scope.folderTree.currentNode.obj.resource_uri;
            }
        }, false);
    }]);

app.controller('ModalSearchCtrl', ['$scope', '$tree', '$modalInstance', 'ext_data',
    function($scope, $tree, $modalInstance, ext_data) {

        $scope.ok = function() {
            $modalInstance.close($scope.result);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.is_valid = function() {
            return _.any(_.values($scope.result));
        }
        $scope.users = ext_data.users;
        var positions = [];
        angular.forEach(ext_data.users, function(user) {
            var idx = positions.indexOf(user.position);
            if (idx == -1) {
                positions.push(user.position);
            }
        });
        $scope.positions = positions;
        $scope.notBoth = function() {
            if ($scope.result.is_finished && $scope.result.is_not_finished) {
                $scope.result.is_finished = false;
                $scope.result.is_not_finished = false;
            }
        }
        $scope.result = {};
    }]);

app.controller('ModalPlanCtrl', ['$scope', '$timeout', '$modalInstance', '$tree', 'SharedState', 'plan',
    function($scope, $timeout, $modalInstance, $tree, SharedState, plan) {

        $scope.ok = function() {
            $modalInstance.close($scope.result);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.result = angular.copy(plan) || {
            resource_type: "plan",
            from_date: new Date(),
            to_date: new Date(),
        };

        // $scope.folderTree = {};
        // $scope.$watch('folderTree.currentNode', function( newObj, oldObj ) {
        //   if($scope.folderTree && angular.isObject($scope.folderTree.currentNode) ) {
        //     $scope.result.parent_node = $scope.folderTree.currentNode.obj.resource_uri;
        //   }
        // }, false);
        // $scope.folderTree = $tree.build_tree2(SharedState.plan_list, {resource_type: ['plan', 'work']}, null, false);
        $scope.ptree = $tree.build_tree2(SharedState.plan_list, {resource_type: ['plan', 'work']}, null, false);
        $scope.ptree.toggle = function(folder) {
            if (!angular.isDefined(folder.manager.userID))
                return;
            $scope.result.parent_folders = $scope.result.parent_folders || [];
            var lst = $scope.result.parent_folders;
            var idx = lst.indexOf(folder.resource_uri);
            if (idx == -1) {
                lst.push(folder.resource_uri);
            } else {
                lst.splice(idx, 1);
            }
        };
        $scope.ptree.isPresent = function(folder) {
            if (!angular.isDefined(folder.manager.userID))
                return;
            if ($scope.result.detailer) {
                if (folder.manager.userID == $scope.result.detailer.userID)
                    return true;
            }
            if (folder.manager.userID == current_user.extra.userid)
                return true;
            if ($scope.result.ownerID) {
                if (folder.manager.userID == $scope.result.ownerID)
                    return true;
            }
            return false;
        };
        $scope.ptree.isChecked = function(folder) {
            if (!angular.isDefined(folder.manager.userID))
                return;
            $scope.result.parent_folders = $scope.result.parent_folders || [];
            var lst = $scope.result.parent_folders;
            var idx = lst.indexOf(folder.resource_uri);
            return idx != -1;
        };

        $scope.pk = {};

        $scope.to_open = function() {
            $timeout(function() {
                $scope.pk.to_opened = true;
            });
        };

        $scope.from_open = function() {
            $timeout(function() {
                $scope.pk.from_opened = true;
            });
        };

        $scope.dateOptions = {
            'year-format': "'yy'",
            'starting-day': 1
        };
    }]);



//---------------------------------------------started MOdalEventCTrl---------------------------------------------

app.controller('ModalEventCtrl', [
    '$scope', '$tree', '$timeout', '$modalInstance', 'DateUtils', 'plan', 'current',
    'ext_data', '$http', 'SharedState', 'Restangular', 'ngProgress',
    function($scope, $tree, $timeout, $modalInstance, DateUtils, plan, current,
            ext_data, $http, SharedState, Restangular, ngProgress) {


        //--------------------------get event types from db------------------------------


        function $destroy() {
            $(document).off("select2-selecting", 'input[name="coworkers"]');
        }
        ;



        $scope.ok = function() {
            // $scope.result.extra = {};
            // angular.forEach($scope.pk.extra, function(o){
            //   $scope.result.extra[o.key] = o.value;
            // });
            $modalInstance.close({
                obj: $scope.result,
                parent_obj: $scope.parent_obj
            });
            $destroy();
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
            $destroy();
        };

        $scope.result = angular.copy(current.obj) || {
            resource_type: "event",
            is_resp_auto: true,
            from_date: plan.from_date,
            to_date: plan.to_date,
            extra: {
                'Финансирование': '0',
                'Тип_Мероприятия': '1',
                'Код бюджетной программы': '2',
            }
        };
        $scope.completion_form = $scope.completion_form || "no";
        $scope.parent_obj = current.parent_obj || plan;
        $scope.result.plan = plan.resource_uri;
        $scope.result.parent_node = $scope.parent_obj.resource_uri;
        $scope.$watch('result.department', function(newObj) {
            if (newObj && newObj.manager) {
                $scope.result.assignees[0].resp = ext_data.user_map[newObj.manager.managerID];
            }
        });
        $scope.$watch('groupTree.currentNode', function(newObj, oldObj) {
            if ($scope.groupTree && angular.isObject($scope.groupTree.currentNode)) {
                $scope.result.parent_node = $scope.groupTree.currentNode.obj.resource_uri;
                $scope.parent_obj = $scope.groupTree.currentNode.obj;
            }
        }, false);

        $scope.pk = {
            extra: [],
            // ass: [],
            // det: []
        };
        $scope.userSelectOptions = ext_data.select2opts($scope);
        $scope.overlays = {};
        $scope.clearOverlays = function(idx) {
            $scope.overlays[idx] = {is_ready: false};
        };
        $scope.getOverlays = function(idx, assign) {
            ngProgress.set(20)
            Restangular.all('assignment').all('overlays').post({
                'assign': assign,
                'event_id': $scope.result.id
            }).then(function(res) {
                ngProgress.set(70)
                $scope.overlays[idx].data = res.overlays;
                $scope.overlays[idx].is_ready = true;
                $scope.overlays[idx].is_overlayed = _.size(res.overlays) > 0;
                ngProgress.complete();
            }, error_print);
        };

        $(document).on("select2-selecting", 'input[name="coworkers"]', function(e) {
            var idx = parseInt($(this).attr('idx'));
            var coworkers = $scope.result.assignees[idx].coworkers;
            if (!coworkers) {
                return;
            }
            for (var i = 0; i < coworkers.length; i++) {
                var worker = coworkers[i];
                if (worker.userID === e.val) {
                    e.preventDefault();
                    return;
                }
            }
        });

        $scope.result.extra = $scope.result.extra || {};
        // if (!angular.isDefined($scope.result.extra['Место проведения'])){
        //   $scope.result.extra['Место проведения'] = '';
        // }
        angular.forEach($scope.result.extra, function(value, key) {
            $scope.pk.extra.push({key: key, value: value});
        });
        $scope.result.assignees = $scope.result.assignees || [];
        $scope.result.detailers = $scope.result.detailers || [];
        $scope.clearDates = function(ass) {
            if (ass.period == "once") {
                ass.dates = [plan.from_date, plan.to_date];
            } else {
                ass.dates = [];
            }
        };
        $scope.addAssignee = function() {
            // $scope.pk.ass.push({});
            $scope.result.assignees.push({
                period: 'quarterly',
                dates: ["" + Math.floor(((new Date()).getMonth() + 1) / 4), (new Date()).getFullYear()],
                completion_form: 'no',
                from_date: plan.from_date,
                to_date: plan.to_date,
                parent_nodes: {},
            });
        };
        $scope.deleteAssignee = function(index) {
            $scope.recalcResp(index);
            $scope.result.deleted_assignees = $scope.result.deleted_assignees || [];
            if ($scope.result.assignees[index].resource_uri) {
                $scope.result.deleted_assignees.push($scope.result.assignees[index]);
            }
            $scope.result.assignees.splice(index, 1);
        };

        $scope.addDetailer = function() {
            $scope.result.detailers.push({
                plan_title: $scope.result.title,
                duration: 1,
                parent_nodes: {}
            });
        };
        $scope.deleteDetailer = function(index) {
            $scope.result.deleted_detailers = $scope.result.deleted_detailers || [];
            if ($scope.result.detailers[index].resource_uri) {
                $scope.result.deleted_detailers.push($scope.result.detailers[index]);
            }
            $scope.result.detailers.splice(index, 1);
        };
        $scope.addExtra = function() {
            $scope.pk.extra.push({});
        };
        $scope.deleteExtra = function(index) {
            $scope.pk.extra.splice(index, 1);
        };

        $scope.pk.disabled = function(date, mode) {
            return (date < plan.from_date || date > plan.to_date);
        };

        $scope.open_calendar = function(id) {
            $timeout(function(id, sub) {
                $scope.pk[id] = !($scope.pk[id] || false);
            });
        };
        $scope.workers_changed = {};
        $scope.det_changed = {};
        $scope.recalcResp = function(idx, didx) {
            if (angular.isDefined(idx)) {
                $scope.workers_changed[idx] = true;
            }
            if (angular.isDefined(didx)) {
                $scope.det_changed[didx] = true;
            }
            if (!$scope.result.is_resp_auto) {
                return;
            }
            var res = [];
            angular.forEach($scope.result.assignees, function(o) {
                if (o.resp) {
                    var tres = [];
                    angular.forEach(o.coworkers, function(value) {
                        tres.push(value.name);
                    });
                    res.push(o.resp.name + (tres.length ? '(' + tres.join(', ') + ')' : ''));
                }
            });
            angular.forEach($scope.result.detailers, function(o) {
                if (o.resp) {
                    res.push(o.resp.name);
                }
            });
            $scope.result.responsible = res.join(', ');
        };

        $scope.periods = DateUtils.periods;
        $scope.quartals = DateUtils.quartals;
        $scope.budget_types = DateUtils.budget_types;
        $scope.event_types = DateUtils.event_types;
        //$scope.event_types=DateUtils.event_types;

        if (!current.obj) {
            $scope.addAssignee();
        }
        // $scope.result.period = current.obj && current.obj.period || "once";
        // $scope.result.dates = current.obj && current.obj.dates || [];

        $scope.periodDates = DateUtils.periodDates;

        $scope.dateOptions = {
            'year-format': "'yy'",
            'starting-day': 1
        };

        $scope.groupTree = $tree.build_tree(plan, 'event', $scope.parent_obj.id);
        $scope.assTree = {
            tree: {}
        };
        $scope.assTree.initTree = function(idx, assign) {
            $scope.assTree.tree[idx] = $tree.build_tree2(SharedState.plan_list, {node_type: ['plan', 'work']});
        };
        $scope.assTree.toggle = function(folder, assign) {
            if (!angular.isDefined(folder.manager.userID))
                return;
            var lst = assign.parent_nodes[folder.manager.userID];
            if (!angular.isDefined(lst)) {
                lst = [];
                assign.parent_nodes[folder.manager.userID] = lst;
            }
            var idx = lst.indexOf(folder.id);
            if (idx == -1) {
                lst.push(folder.id);
            } else {
                lst.splice(idx, 1);
            }
            assign.parent_nodes = angular.copy(assign.parent_nodes);
        };
        $scope.assTree.workers = function(assign) {
            var res = [];
            if (assign.resp) {
                res.push(assign.resp.userID);
            }
            if (angular.isArray(assign.coworkers)) {
                for (var i = 0; i < assign.coworkers.length; i++) {
                    res.push(assign.coworkers[i].userID);
                }
            }
            return res;
        }
        $scope.assTree.isChecked = function(folder, assign, tree_idx) {
            if (!angular.isDefined(folder.manager.userID))
                return;
            var ids = $scope.assTree.workers(assign);
            var lst = assign.parent_nodes[folder.manager.userID];
            if (!angular.isDefined(lst)) {
                lst = [];
                assign.parent_nodes[folder.manager.userID] = lst;
            }
            if ($scope.workers_changed[tree_idx]) {
                $scope.workers_changed[tree_idx] = false;

                if (lst.length == 0) {
                    $scope.assTree.toggle(folder, assign);
                }
            }
            var idx = lst.indexOf(folder.id);
            return idx != -1;
        };
        $scope.assTree.isPresent = function(folder, assign) {
            var manager = folder.manager;
            if (!angular.isDefined(manager))
                return false;
            var ids = $scope.assTree.workers(assign);
            angular.forEach(assign.parent_nodes, function(value, key) {
                var idx = ids.indexOf(key);
                if (idx == -1) {
                    delete assign.parent_nodes[key];
                }
            });
            var idx = ids.indexOf(manager.userID);
            return idx != -1;
        };

        $scope.detTree = {
            tree: {}
        };
        $scope.detTree.initTree = function(idx, detailer) {
            $scope.detTree.tree[idx] = $tree.build_tree2(SharedState.plan_list, {node_type: ['plan', 'work']});
        };
        $scope.detTree.toggle = function(folder, detailer) {
            if (!angular.isDefined(folder.manager.userID))
                return;
            var lst = detailer.parent_nodes[folder.manager.userID];
            if (!angular.isDefined(lst)) {
                lst = [];
                detailer.parent_nodes[folder.manager.userID] = lst;
            }
            var idx = lst.indexOf(folder.id);
            if (idx == -1) {
                lst.push(folder.id);
            } else {
                lst.splice(idx, 1);
            }
            detailer.parent_nodes = angular.copy(detailer.parent_nodes);
        };
        $scope.detTree.workers = function(detailer) {
            var res = [];
            if (detailer.resp) {
                res.push(detailer.resp.userID);
            }
            return res;
        }
        $scope.detTree.isChecked = function(folder, detailer, tree_idx) {
            if (!angular.isDefined(folder.manager.userID))
                return;
            // var ids = $scope.detTree.workers(detailer);
            var lst = detailer.parent_nodes[folder.manager.userID];
            if (!angular.isDefined(lst)) {
                lst = [];
                detailer.parent_nodes[folder.manager.userID] = lst;
            }
            if ($scope.det_changed[tree_idx]) {
                $scope.det_changed[tree_idx] = false;

                if (lst.length == 0) {
                    $scope.detTree.toggle(folder, detailer);
                }
            }
            var idx = lst.indexOf(folder.id);
            return idx != -1;
        };
        $scope.detTree.isPresent = function(folder, detailer) {
            var manager = folder.manager;
            if (!angular.isDefined(manager))
                return false;
            var ids = $scope.detTree.workers(detailer);
            angular.forEach(detailer.parent_nodes, function(value, key) {
                var idx = ids.indexOf(key);
                if (idx == -1) {
                    delete detailer.parent_nodes[key];
                }
            });
            var idx = ids.indexOf(manager.userID);
            return idx != -1;
        };
        $scope.users = ext_data.users;
        $scope.departments = ext_data.departments;
        $scope.comp_forms = [{id: "no", name: "Нет"}];
        Array.prototype.push.apply($scope.comp_forms, ext_data.settings.work_completion_forms);
    }]);

//---------------------------------------------ended MOdalEventCTrl---------------------------------------------


app.controller('ModalProgressCtrl', [
    '$scope', '$modalInstance', 'current',
    function($scope, $modalInstance, current) {

        $scope.ok = function() {
            $modalInstance.close({
                obj: $scope.result,
            });
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.result = current;
    }]);

app.controller('ModalReassignCtrl', ['$scope', '$tree', '$modalInstance', 'current', 'SharedState', 'ext_data',
    function($scope, $tree, $modalInstance, current, SharedState, ext_data) {

        
        //alert("1123");
        $scope.ok = function() {
            console.log($scope.result.assignees);
            var new_assignee = {};
            //new_assignee = $scope.result.assignees[0];
            
            
            new_assignee.resp = {
                name: $scope.result.user.name,
                userID: $scope.result.user.userID,
                position: $scope.result.user.position
            };
            //console.log(new_assignee);
            $scope.result.assignees.push(new_assignee);
            //console.log($scope.result.assignees);
            $modalInstance.close($scope.result);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.userSelectOptions = ext_data.select2opts($scope);
        //$scope.users = ext_data.users;
        

        $scope.result = angular.copy(current.obj) || {
            resource_type: "event",
        };
        //$scope.result.assignees = $scope.result.assignees || [];
        //var assignee=$scope.result.assignees[0];
        console.log($scope.result.assignees);
    }]);