var csrftoken = getCookie('csrftoken');
var no_access = function() {
    alert('У вас нет доступа');
};
var MyCtrl = app.controller('MyCtrl', [
    '$scope', '$modal', '$log', '$tree', 'Restangular', 'Synergy',
    'ngProgress', '$templateCache', 'SharedState', 'Users', 'Nav',
    '$compile', '$http', '$q', '$filter', 'DateUtils', '$fileUploader',
    function($scope, $modal, $log, $tree, Restangular, Synergy,
            ngProgress, $templateCache, SharedState, Users, Nav,
            $compile, $http, $q, $filter, DateUtils, $fileUploader) {
        // $scope.myData = [plan];
        var Plans = Restangular.all('plan');
        var Events = Restangular.all('event');
        var Nodes = Restangular.all('node');
        var Groups = Restangular.all('group');
        var Folders = Restangular.all('folder');
        //var 


        var state = SharedState;
        state.plan_list = [];
        state.currentPlan = null;
        $scope.gridData = [];
        $scope.mouse = {
            node: null,
            getType: function() {
                if (!$scope.mouse.node)
                    return null;
                if (!$scope.mouse.node.obj)
                    return null;
                return $scope.mouse.node.obj.resource_type;
            }
        };

        $scope.moveNode = function(node, d) {
            var parent_id = Nav.moveNode(node, d);
            if (parent_id && !node.id.startsWith('filter')) {
                ngProgress.set(10);
                Nodes.one(node.id, 'move').patch({
                    parent_id: parent_id,
                    order: node.order[parent_id]
                }).then(function(result) {
                    ngProgress.complete();
                    // $scope.$emit('update');
                }, error_print);
            }
        }

        state.set_plan = function(plan) {
            if (plan) {
                if (plan.resource_type == "plan") {
                    $scope.search_results = null;
                    state.currentPlan = plan;
                } else {
                    state.currentPlan = null;
                }
            }
        }

        state.get_plan = function(plan_id) {
            var index = -1;
            if (angular.isDefined(plan_id)) {
                for (i = 0; i < state.plan_list.length; i++) {
                    if (state.plan_list[i].id === plan_id) {
                        index = i;
                    }
                }
            }
            if (index != -1) {
                return state.plan_list[index];
            }
            return null;
        };

        var build_grid = function() {
            $scope.gridData = $tree.build_data($scope.planTree);
            
            $scope.grid.data = $scope.gridData;
        };

        var changeOrder = function(node, delta) {
            ngProgress.set(10);
            return Nodes.one(node.id, 'move').patch({delta: delta}).then(function(result) {
                ngProgress.set(70);
            }, error_print);
        };

        $scope.group = {
            delete: function() {
                if (confirm("Все елементы в данной группе будут удалены. Удалить?")) {
                    angular.forEach($scope.grid.selection, function(o) {
                        // console.log(o);
                        Groups.one(o.obj.id).remove().then(function() {
                            refresh_plans();
                            // var index = o.parent_obj.items.indexOf(o.obj);
                            // if (index != -1){
                            //   o.parent_obj.items.splice(index, 1);
                            //   build_grid();
                            // }
                        }, error_print);
                    });
                }
            },
            edit: function() {
                $scope.openAddGroup($scope.grid.selection[0].obj, $scope.grid.selection[0].parent_obj);
            },
            moveUpper: function() {
                changeOrder($scope.grid.selection[0], -1).then(function() {
                    $scope.$emit('update');
                });
            },
            moveLower: function() {
                changeOrder($scope.grid.selection[0], 1).then(function() {
                    $scope.$emit('update');
                });
            },
        };

        $scope.nav = {
            tree: {},
            toggle: function(id) {
                if (angular.isDefined($scope.nav.tree[id])) {
                    $scope.nav.tree[id] = !$scope.nav.tree[id];
                } else {
                    $scope.nav.tree[id] = true;
                }
            }
        };

        $scope.plan = {
            tree: {},
            delete: function() {
                if (confirm("Все елементы в данном плане будут удалены. Удалить?")) {
                    Plans.one(state.currentPlan.id).remove().then(function() {
                        refresh_plans();
                        // var index = state.plan_list.indexOf(state.currentPlan);
                        // if (index != -1){
                        //   state.plan_list.splice(index, 1);
                        //   $scope.$emit('updateFolders');
                        // }
                    }, error_print);
                }
            },
            curPlan: function() {
                if ($scope.mouse.getType() === "plan") {
                    return $scope.mouse.node.obj;
                }
                return state.currentPlan;
            },
            edit: function() {
                $scope.openAddPlan($scope.plan.curPlan());
            },
        };

        $scope.folder = {
            delete: function() {
                // if (current_user.extra.configurator !== "true"){
                //   no_access();
                //   return;
                // }
                if (confirm("Все планы в данном портфеле будут удалены. Удалить?")) {
                    Folders.one($scope.plan_tree.currentNode.id).remove().then(function() {
                        var index = state.plan_list.indexOf($scope.plan_tree.currentNode.obj);
                        if (index != -1) {
                            state.plan_list.splice(index, 1);
                            $scope.$emit('updateFolders');
                        }
                    }, error_print);
                }

            },
            edit: function() {
                // if (current_user.extra.configurator !== "true"){
                //   no_access();
                //   return;
                // }
                node = $scope.plan_tree.currentNode.obj;
                $scope.openAddFolder(node, node.parent_node);
            },
            create: function() {
                // if (current_user.extra.configurator !== "true"){
                //   no_access();
                //   return;
                // }
                $scope.openAddFolder();
            },
            moveUpper: function() {
                changeOrder($scope.plan_tree.currentNode.obj, -1).then(function() {
                    $scope.refresh_plans();
                });
            },
            moveLower: function() {
                changeOrder($scope.plan_tree.currentNode.obj, 1).then(function() {
                    $scope.refresh_plans();
                });
            },
            copy: function() {
                if (confirm("Вы уверены, что хотите скопировать данный портфель? Все портфели и планы в будут также скопированы, данный процесс может занять некоторое время.")) {
                    var new_folder_name = prompt("Введите имя для нового портфеля", $scope.plan_tree.currentNode.title);
                    if (new_folder_name) {
                        Folders.one($scope.plan_tree.currentNode.id).post('copy', {title: new_folder_name}).then(function(res) {
                            refresh_plans();
                        }, error_print);
                    }
                }
            }
        };

        $scope.event = {
            delete: function() {
                if (confirm("Вы действительно хотите удалить?")) {
                    angular.forEach($scope.grid.selection, function(o) {
                        // console.log(o);
                        Events.one(o.obj.id).remove().then(function() {
                            refresh_plans();
                            // var index = o.parent_obj.items.indexOf(o.obj);
                            // if (index != -1){
                            //   o.parent_obj.items.splice(index, 1);
                            //   build_grid();
                            // }
                        }, error_print);
                    });
                }
            },
            edit: function() {
                $scope.openAddEvent($scope.grid.selection[0].obj, $scope.grid.selection[0].parent_obj);
            },
            reassign: function() {
                $scope.openReassignEvent($scope.grid.selection[0].obj, $scope.grid.selection[0].parent_obj);
            },
            moveUpper: function() {
                changeOrder($scope.grid.selection[0], -1).then(function() {
                    $scope.$emit('update');
                });
            },
            moveLower: function() {
                changeOrder($scope.grid.selection[0], 1).then(function() {
                    $scope.$emit('update');
                });
            },
            open_plan: function() {
                var ev = $scope.search_grid.selection[0];
                if (ev && ev.plan_obj && ev.plan_obj.id) {
                    state.set_plan(state.get_plan(ev.plan_obj.id));
                }
            },
            openEditProgress: function(obj) {
                if (obj.node_type != 'event')
                    return;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/progressTemplate.html',
                    controller: 'ModalProgressCtrl',
                    keyboard: true,
                    resolve: {
                        current: function() {
                            return obj;
                        },
                    }
                });

                modalInstance.result.then(function(result) {
                    var newEvent = result.obj;
                    if (newEvent.id) {
                        Events.one(newEvent.id).patch({progress: newEvent.progress}).then(function() {
                            $scope.$emit('update');
                        }, error_print);
                    }
                }, function() {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            }
        };

        var refresh_plans = function(select) {
            //alert("asdasdasd");
            ngProgress.set(50);
            return Nodes.all('folders').getList().then(function(result) {
                state.plan_list = result;
                $scope.$emit('updateFolders');
                ngProgress.complete();
            }, error_print);
        };
        $scope.refresh_plans = refresh_plans;

        $scope.$watch('plan_tree.currentNode', function(newObj, oldObj) {
            if ($scope.plan_tree && angular.isObject($scope.plan_tree.currentNode)) {
                var node = $scope.plan_tree.currentNode.obj;
                $scope.search_results = null;
                // if (node.resource_type == "plan"){
                state.set_plan(node);
                if (node.resource_type == 'work' && node.doc_id) {
                    try {
                        window.top.processParams('submodule=common&action=open_document&document_identifier=' + node.doc_id);
                    } catch (err) {
                        window.top.location = synergy_host + '/Synergy/Synergy.html?locale=ru&submodule=common&action=open_document&document_identifier=' + node.doc_id;
                    }
                }
                // }
            }
        }, false);

        refresh_plans();

        var refresh = function() {
            if (state.currentPlan) {
                ngProgress.start();
                //empty selection
                $scope.eventDetail = null;
                // $scope.gridSelection.splice(0,$scope.gridSelection.length);
                Restangular.one('node', state.currentPlan.id).getList('tree').then(function(result) {
                    $scope.planTree = result[0];
                    ngProgress.set(50);
                    build_grid();
                    ngProgress.set(70);
                }, error_print);
                $scope.$emit('updateFolders');
            } else {
                $scope.planTree = null;
                build_grid();
            }
        };

        $scope.$watch('state.currentPlan', refresh);
        $scope.$on('update', refresh);
        $scope.plan_tree = {};
        $scope.$on('updateFolders', function() {
            var select = null;
            if ($scope.plan_tree.currentNode) {
                select = {id: $scope.plan_tree.currentNode.id};
                if (state.currentPlan && select.id != state.currentPlan.id) {
                    select.id = state.currentPlan.id;
                }
            }
            angular.extend($scope.plan_tree, Nav.build(state.plan_list, null, select));
        });

        $scope.gridSelection = [];

        var anOpen = [];
        var anOpen1 = [];
        var eventDetailTemplate = 'spt/event-detail.html';
        $http({method: 'GET', url: eventDetailTemplate}).
                success(function(data, status, headers, config) {
                    $templateCache.put(eventDetailTemplate, data);
                }).
                error(function(data, status, headers, config) {
                    console.log("Error loading " + eventDetailTemplate);
                    console.log(data, status);
                });

        $scope.toggleRow = function($event) {
            // console.log($event.target);
            var $tg = $($event.target);
            if (angular.isDefined($tg.data('toggled'))) {
                $tg.data('toggled', !$tg.data('toggled'));
            } else {
                $tg.data('toggled', true);
            }
            var toggled = $tg.data('toggled');
            var oTable = $('#gridTable').dataTable();
            var nTr = $($event.target).parents('tr:eq(0)')[0];
            var i = $.inArray(nTr, anOpen);
            if (toggled) {
                //angular.element($tg)
                //        .addClass('icon-chevron-down')
                //        .removeClass('icon-chevron-right');
                console.log("row " + i);
                if (i === -1) {
                    // var selectedEvent = row.obj;
                    ngProgress.reset();
                    ngProgress.start();
                    ngProgress.set(30);
                    Events.one($tg.attr('event-id')).get().then(function(data) {
                        ngProgress.set(60);
                        SharedState.eventDetail = data;
                        var newScope = $scope.$new(false);
                        newScope.eventDetail = data;
                        newScope.synergy_host = synergy_host;
                        var nDetailsRow = oTable.fnOpen(
                                nTr, $compile($templateCache.get(eventDetailTemplate))(newScope), 'details');
                        anOpen.push(nTr);
                        ngProgress.complete();

                    }, error_print);
                    anOpen1.splice(i, 1);
                }
            } else {
                //angular.element($event.target)
                //        .addClass('icon-chevron-right')
                //        .removeClass('icon-chevron-down');
                oTable.fnClose(nTr);
                anOpen.splice(i, 1);
            }
        };
        $scope.toggleBtns = function($event) {
            // console.log($event.target);
            var $tg = $($event.target);
            if (angular.isDefined($tg.data('toggled'))) {
                $tg.data('toggled', !$tg.data('toggled'));
            } else {
                $tg.data('toggled', true);
            }
            var toggled = $tg.data('toggled');
            //var oTable = $('#gridTable').dataTable();
            //var nTr = $($event.target).parents('tr:eq(0)')[0];
            //var i = $.inArray(nTr, anOpen);
            if (toggled) {
                $tg.next().toggle();
                angular.element($tg)
                        .addClass('icon-chevron-down')
                        .removeClass('icon-chevron-right');

            } else {
                $tg.next().toggle();
                angular.element($event.target)
                        .addClass('icon-chevron-right')
                        .removeClass('icon-chevron-down');
                //oTable.fnClose(nTr);
                //anOpen.splice(i, 1);
            }
        };





        var subEventDetailTemplate = 'spt/sub-event-detail.html';
        $http({method: 'GET', url: subEventDetailTemplate}).
                success(function(data, status, headers, config) {
                    $templateCache.put(subEventDetailTemplate, data);
                }).
                error(function(data, status, headers, config) {
                    console.log("Error loading " + subEventDetailTemplate);
                    console.log(data, status);
                });

        $scope.toggleDetalizationRow = function($event) {
            // console.log($event.target);
            var $tg = $($event.target);
            if (angular.isDefined($tg.data('toggled1'))) {
                $tg.data('toggled1', !$tg.data('toggled1'));
            } else {
                $tg.data('toggled1', true);
            }
            console.log($($event.target).data());
            var toggled = $tg.data('toggled1');
            var oTable = $('#gridTable').dataTable();
            var nTr = $($event.target).parents('tr:eq(0)')[0];
            var i = $.inArray(nTr, anOpen1);
            if (toggled) {
                console.log("detail " + i);
                if (i === -1) {
                    // var selectedEvent = row.obj;
                    ngProgress.reset();
                    ngProgress.start();
                    ngProgress.set(30);
                    Events.get($tg.attr('event-id')).then(function(data) {
                        ngProgress.set(60);
                        SharedState.subEventDetail = data;
                        var newScope = $scope.$new(false);
                        newScope.subEventDetail = data;
                        newScope.synergy_host = synergy_host;
                        var nDetailsRow = oTable.fnOpen(
                                nTr, $compile($templateCache.get(subEventDetailTemplate))(newScope), 'details');
                        anOpen1.push(nTr);
                        ngProgress.complete();
                    }, error_print);
                    anOpen1.splice(i, 1);
                }
            } else {
                console.log("detail " + i);
                oTable.fnClose(nTr);
                anOpen1.splice(i, 1);
            }
        };


        //$scope.alert = function(asd){
        //alert(asd);
        //};

        function assDuration(assignees) {
            if (!assignees || assignees.length == 0)
                return null;
            var res = [];
            //angular.forEach(assignees, function(obj) {
            var result = $filter('ass_duration')(assignees[0]);
            res.push(result);
            //});
            return res.join(", ");
        }

        function detDuration(detailers) {
            if (!detailers || detailers.length == 0)
                return null;
            // nasty hack
            if (!$scope.plan_tree.currentNode)
                return null;
            plan = $scope.plan_tree.currentNode.obj;
            if (plan.node_type != 'plan')
                return null;
            return $filter('date')(plan.from_date) + ' - ' + $filter('date')(plan.to_date);
        }

        $scope.grid = {
            selection: [],
            data: [],
            myCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                var index = -1;
                console.log($scope.grid.data);
                for (var i = 0; i < $scope.grid.data.length; i++) {
                    if ($scope.grid.data[i].id == aData.id) {
                        index = i;
                        break;
                    }
                }
                angular.element(nRow)
                        .attr('context', 'context')
                        .attr('ng-attr', aData.node_type)
                        .attr('ng-mouseover', 'grid.selection[0]=grid.data[' + index + '];');
                // context="context" ng-attr="{{ row.getProperty(\'obj\').resource_type }}"
                // $('td:eq(2)', nRow).bind('click', function() {
                //   $scope.$apply(function() {
                //     $scope.someClickHandler(aData);
                //   });
                // });
                return nRow;
            },
            columnDefs: [
                {
                    "sTitle": "№",
                    "mData": function(source, type, val) {
                        return source.num || "";
                    },
                    "aTargets": [0]
                },
                {
                    "sTitle": "Название",
                    // "mDataProp": "title",
                    "mData": function(source, type, val) {
                        if (source.node_type == "event") {
                            // popover-placement="bottom" popover-append-to-body="true" popover-event="grid.data[{{$index}}]" 
                            return '<i event-id="' + source.id + '" class="event-handle icon-chevron-right" ng-click="toggleBtns($event);"></i> ' + source.title +
                                    '<div class="event-btns" style="display:none;">' +
                                    '<button style="margin-top:5px; width: 124px;" event-id="' + source.id + '" class="event-handle" ng-click="toggleRow($event);">Исполнение</button><br/>' +
                                    '<button style="margin-top:5px;width: 124px;"event-id="' + source.id + '" class="event-handle" ng-click="toggleDetalizationRow($event);">Детализация</button><br/>' +
                                    '<button style="margin-top:5px;width: 124px;"event-id="' + source.id + '" class="event-handle" ng-click="event.reassign();">Перепоручение</button></div>';

                        }
                        return null;//source.title;
                    },
                    "aTargets": [1]
                },
                {
                    "sTitle": "Ответственные",
                    "mData": function(source, type, val) {
                        if (angular.isDefined(source.department)) {
                            return source.department.nameRu;
                        }
                        return "";
                    },
                    // "mRender": "resp[,].name",
                    "aTargets": [2]
                },
                {
                    "sTitle": "Срок",
                    "mData": function(source, type, val) {
                        if (source.node_type == "plan") {
                            return null;//$filter('date')(source.from_date) + ' - ' + $filter('date')(source.to_date);
                        }
                        if (source.node_type == "event") {
                            var ass = assDuration(source.assignees);
                            var det = detDuration(source.detailers);
                            var res = [];
                            if (ass) {
                                res.push(ass);
                            }
                            if (det) {
                                res.push(det);
                            }
                            return res.join(", ");
                        }
                        return "";
                    },
                    // "mRender": "resp[,].name",
                    "aTargets": [3]
                },
                //-----------------------------added shit below------------------------------
                {
                    "sTitle": "Тип Мероприятия",
                    "mData": function(source, type, val) {
                        if (angular.isDefined(source.extra) && source.extra['Тип_Мероприятия']) {
                            return DateUtils.event_types[source.extra['Тип_Мероприятия']];
                        }
                        return "";
                    },
                    "aTargets": [4]
                },
                //-------------------------------added shit above-------------------------------
                {
                    "sTitle": "Финансирование",
                    "mData": function(source, type, val) {
                        if (angular.isDefined(source.extra) && source.extra['Финансирование']) {
                            return DateUtils.budget_types[source.extra['Финансирование']];
                        }
                        return "";
                    },
                    "aTargets": [5]
                },
                {
                    "sTitle": "Сумма",
                    // "mDataProp": "title",
                    "mData": function(source, type, val) {

                        if (source.node_type == "event" && source.extra['Сумма']) {
                            // popover-placement="bottom" popover-append-to-body="true" popover-event="grid.data[{{$index}}]" 
                            //budget+=source.extra['Сумма'];
                            return  source.extra['Сумма'];

                        } else if (source.node_type == "plan") {

                        }
                        return null;
                    },
                    "aTargets": [6]
                },
                {
                    "sTitle": "Прогресс",
                    "mData": function(source, type, val) {
                        if (angular.isDefined(source.progress)) {
                            var tooltip = '';
                            var click = '';
                            if (source.node_type == "event") {
                                tooltip = 'tooltip-placement="left" tooltip-append-to-body="true" tooltip="Кликните для установки прогресса"';
                                //click = ' ng-click="event.openEditProgress(grid.data[{{$index}}].obj);"'
                            }else if (source.node_type == "plan") {
                                return null;
                            }   
                            // return '<div ng-if="isEvent(row)"  ng-click="openEditProgress(row.getProperty(\'obj\'));" class="progress" style="margin: 0 0;"><div class="bar" style="width: {{row.getProperty(col.field)}}%;">{{row.getProperty(col.field)}}%</div></div><div class="progress" style="margin: 0 0;"><div class="bar" style="width: {{row.getProperty(col.field)}}%;">{{row.getProperty(col.field)}}%</div></div>';
                            return '<div ' + tooltip + click + ' class="progress" style="margin: 0 0;"><div class="bar" title="' + source.progress + '%" style="width: ' + source.progress + '%;">' + source.progress + ' &#37;</div></div>';
                        }
                        return 0;
                    },
                    "aTargets": [7]
                },
            ],
            overrideOptions: {
                "bStateSave": false,
                "bAutoWidth": false,
                "iCookieDuration": 2419200, /* 1 month */
                "bJQueryUI": false,
                "bPaginate": false,
                "bLengthChange": false,
                // "sScrollY": 100,
                "bFilter": false,
                "bInfo": false,
                "bDestroy": true,
                "sDom": "Rlfrtip",
                "oLanguage": {
                    "sEmptyTable": "Нет данных"
                },
                "aoColumns": [
                    {"sType": "paragraph"},
                    null,
                    null,
                    null,
                    null,
                    null
                ],
                "fnDrawCallback": function(oSettings) {
                    if (ngProgress.status() != 0) {
                        ngProgress.complete();
                    }
                },
            }
        };

        $scope.search_grid = {
            on_data: function(res) {
                $scope.search_grid.meta = res.metadata;
                res = _.map(res, function(value) {
                    var nobj = angular.copy(value);
                    nobj.obj = value;
                    return nobj;
                })
                $scope.search_grid.data = res;
                $scope.search_results = res;
            },
            next: function() {
                if (!$scope.search_grid.meta)
                    return;
                if (!$scope.search_grid.meta.next)
                    return;
                var url = $scope.search_grid.meta.next.replace(/\/plan\/api\/v1\//, '');
                Restangular.allUrl(url).getList().then(function(res) {
                    $scope.search_grid.on_data(res);
                }, error_print);
            },
            prev: function() {
                if (!$scope.search_grid.meta)
                    return;
                if (!$scope.search_grid.meta.previous)
                    return;
                var url = $scope.search_grid.meta.previous.replace(/\/plan\/api\/v1\//, '');
                Restangular.allUrl(url).getList().then(function(res) {
                    $scope.search_grid.on_data(res);
                }, error_print);
            },
            selection: [],
            data: [],
            myCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                angular.element(nRow)
                        .attr('context', 'context')
                        .attr('ng-attr', 'search_' + aData.node_type)
                        .attr('ng-mouseover', 'search_grid.selection[0]=search_grid.data[{{$index}}];');
                return nRow;
            },
            columnDefs: [
                {
                    "sTitle": "План",
                    "mData": function(source, type, val) {
                        return source.plan_obj && source.plan_obj.title || "";
                    },
                    "aTargets": [0]
                },
                {
                    "sTitle": "Название",
                    // "mDataProp": "title",
                    "mData": function(source, type, val) {
                        if (source.node_type == "event") {
                            // popover-placement="bottom" popover-append-to-body="true" popover-event="grid.data[{{$index}}]" 
                            return '<i event-id="' + source.id + '" class="event-handle icon-chevron-right" ng-click="toggleRow($event);"></i> ' + source.title;

                        }
                        return source.title;
                    },
                    "aTargets": [1]
                },
                {
                    "sTitle": "Ответственные",
                    "mData": function(source, type, val) {
                        if (angular.isDefined(source.responsible)) {
                            return source.responsible;
                        }
                        return "";
                    },
                    // "mRender": "resp[,].name",
                    "aTargets": [2]
                },
                /*------------------------------added shit below--------------------------------------
                 {
                 "sTitle": "Тип_Мероприятия",
                 "mData": function(source, type, val){
                 if (angular.isDefined(source.event_type)){
                 return source.event_type;
                 }
                 return "";
                 },
                 // "mRender": "resp[,].name",
                 "aTargets":[3]
                 },
                 */
                //------------------------------added shit above---------------------------------------
                {
                    "sTitle": "Срок",
                    "mData": function(source, type, val) {
                        if (source.node_type == "plan") {
                            return $filter('date')(source.from_date) + ' - ' + $filter('date')(source.to_date);
                        }
                        if (source.node_type == "event") {
                            var ass = assDuration(source.assignees);
                            var det = detDuration(source.detailers);
                            var res = [];
                            if (ass) {
                                res.push(ass);
                            }
                            if (det) {
                                res.push(det);
                            }
                            return res.join(", ");
                        }
                        return "";
                    },
                    // "mRender": "resp[,].name",
                    "aTargets": [3]
                },
                {
                    "sTitle": "Примечание",
                    "mData": function(source, type, val) {
                        if (angular.isDefined(source.extra)) {
                            var res = [];
                            angular.forEach(source.extra, function(value, key) {
                                res.push(key + ": " + value);
                            });
                            return res.join(", ");
                        }
                        return "";
                    },
                    "aTargets": [4]
                },
                {
                    "sTitle": "Прогресс",
                    "mData": function(source, type, val) {
                        if (angular.isDefined(source.progress)) {
                            return '<div class="progress" style="margin: 0 0;"><div class="bar" title="' + source.progress + '%" style="width: ' + source.progress + '%;"></div></div>';
                        }
                        return "";
                    },
                    "aTargets": [5]
                },
            ],
            overrideOptions: {
                "bStateSave": false,
                "bAutoWidth": false,
                "iCookieDuration": 2419200, /* 1 month */
                "bJQueryUI": false,
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": false,
                "bInfo": false,
                "bDestroy": true,
                "sDom": "Rlfrtip",
                "oLanguage": {
                    "sEmptyTable": "Нет данных"
                },
                "fnDrawCallback": function(oSettings) {
                    if (ngProgress.status() != 0) {
                        ngProgress.complete();
                    }
                },
            }
        };

        $scope.state = SharedState;

        $scope.openAddGroup = function(obj, parent_obj) {
            var modalInstance = $modal.open({
                templateUrl: 'spt/createGroupTemplate.html',
                controller: 'ModalGroupCtrl',
                keyboard: false,
                resolve: {
                    plan: function() {
                        return $scope.planTree;
                    },
                    current: function() {
                        return {
                            obj: obj,
                            parent_obj: parent_obj
                        };
                    }
                }
            });

            modalInstance.result.then(function(result) {
                // newGroup.position.items = newGroup.position.items  || [];
                // newGroup.position.items.push({id: 'xx', title: newGroup.title, otype: 'group'});
                var newGroup = result.obj;
                if (newGroup.id) {
                    Groups.one(newGroup.id).patch(newGroup).then(function() {
                        if (result.parent_obj != parent_obj) {
                            var prnt = result.parent_obj;
                            prnt.items = prnt.items || [];
                            prnt.items.push(newGroup);

                            parent_obj.items = parent_obj.items || [];
                            var index = parent_obj.items.indexOf(newGroup);
                            if (index != -1) {
                                parent_obj.items.splice(index, 1);
                            }
                        }
                        // } else {          
                        //   parent_obj.items = parent_obj.items || [];
                        //   var index = parent_obj.items.indexOf(newGroup);
                        //   parent_obj.items[index] = newGroup;
                        // }
                        build_grid();
                    }, error_print);
                    // newGroup.$update();        
                } else {
                    Groups.post(newGroup).then(function(obj) {
                        newGroup = angular.extend(newGroup, obj);
                        newGroup.obj = angular.copy(newGroup);
                        // tmp = putResponseHeaders('Location');
                        // newGroup.resource_uri = tmp.replace(/\\/g,'/').replace(/http(s)?:\/\/[^\/]*/, '');
                        // newGroup.id = tmp.replace(/\\/g,'/').replace(/.*\//, '');
                        var parent_obj = result.parent_obj;
                        parent_obj.items = parent_obj.items || [];
                        parent_obj.items.push(newGroup);
                        build_grid();
                    }, error_print);
                }
                // $scope.$emit('update');
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.openAddFolder = function(obj) {
            var modalInstance = $modal.open({
                templateUrl: 'spt/createFolderTemplate.html',
                controller: 'ModalFolderCtrl',
                keyboard: false,
                resolve: {
                    current: function() {
                        return obj;
                    },
                    ext_data: function() {
                        return Users.load;
                    },
                }
            });

            modalInstance.result.then(function(newFolder) {
                // newGroup.position.items = newGroup.position.items  || [];
                // newGroup.position.items.push({id: 'xx', title: newGroup.title, otype: 'group'});
                // var newFolder = result.obj;
                if (newFolder.id) {
                    Folders.one(newFolder.id).patch({
                        parent_node: newFolder.parent_node,
                        title: newFolder.title,
                        manager: newFolder.manager,
                    }).then(function() {
                        refresh_plans();
                    }, error_print);
                    // newFolder.$update();        
                } else {
                    Folders.post(newFolder).then(function(obj) {
                        refresh_plans();
                    }, error_print);
                }
                // $scope.$emit('update');
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.openAddPlan = function(selectedPlan) {
            var modalInstance = $modal.open({
                templateUrl: 'spt/createPlanTemplate.html',
                controller: 'ModalPlanCtrl',
                keyboard: false,
                resolve: {
                    plan: function() {
                        return selectedPlan;
                    },
                }
            });

            modalInstance.result.then(function(obj) {
                if (obj.id) {
                    Plans.one(obj.id).patch(obj).then(function() {
                        console.log('PATCHED');
                        refresh_plans();
                    }, error_print);
                } else {
                    Plans.post(obj).then(function(resp) {
                        obj = angular.extend(obj, resp);
                        refresh_plans();
//          state.plan_list.push(obj);
                    }, error_print);
                }
                // $scope.$emit('update');
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        var search_attempt = 0;
        $scope.openSearch = function() {
            var modalInstance = $modal.open({
                templateUrl: 'spt/searchForm.html',
                controller: 'ModalSearchCtrl',
                keyboard: true,
                resolve: {
                    ext_data: function() {
                        return Users.load;
                    }
                }
            });

            modalInstance.result.then(function(query) {
                search_attempt++;
                Restangular.one('event').getList(null, {q: JSON.stringify(query)}).then(function(res) {
                    $scope.search_grid.on_data(res);
                }, error_print);
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.openAddEvent = function(obj, parent_obj) {
            var modalInstance = $modal.open({
                templateUrl: 'spt/createEventTemplate.html',
                controller: 'ModalEventCtrl',
                keyboard: false,
                resolve: {
                    plan: function() {
                        return $scope.planTree;
                    },
                    current: function() {
                        return {
                            obj: obj,
                            parent_obj: parent_obj
                        };
                    },
                    ext_data: function() {
                        return Users.load;
                    },
                }
            });

            modalInstance.result.then(function(result) {
                var patch_fields = [
                    'title', 'responsible', 'event_type', 'department', 'is_resp_auto', 'extra', 'parent_node', 'from_date', 'to_date',
                ];
                var create_fields = [
                    'title', 'responsible', 'event_type', 'department', 'is_resp_auto', 'extra', 'parent_node', 'from_date', 'to_date', 'plan',
                ];
                function updateTheRest(newEvent, promises) {
                    proms = [];
                    // Update assignees
                    proms.push(Events.one(newEvent.id, 'assignees').patch(
                            {
                                objects: newEvent.assignees,
                                deleted_objects: _.pluck(newEvent.deleted_assignees, 'resource_uri')
                            }).then(function() {
                        ngProgress.set(ngProgress.status() + 30);
                        return true;
                    }, error_print));
                    // update detailers
                    proms.push(Events.one(newEvent.id, 'detailers').patch(
                            {
                                objects: newEvent.detailers,
                                deleted_objects: _.pluck(newEvent.deleted_detailers, 'resource_uri')
                            }).then(function() {
                        ngProgress.set(ngProgress.status() + 30);
                        return true;
                    }, error_print));
                    if (angular.isArray(promises)) {
                        angular.forEach(promises, function(value) {
                            proms.push(value);
                        });
                    }
                    $q.all(proms).then(function() {
                        Events.get(newEvent.id).then(function(data) {
                            newEvent = angular.extend(newEvent, data);
                            ngProgress.complete();
                            build_grid();
                            refresh_plans(state.currentPlan.id);
                        }, error_print);
                    }, error_print);
                }
                ;
                var newEvent = result.obj;
                if (newEvent.id) {
                    // Update event
                    ngProgress.set(10);
                    var p1 = Events.one(newEvent.id).patch(
                            _.pick(newEvent, patch_fields)).then(function() {
                        ngProgress.set(ngProgress.status() + 30);
                        return true;
                    }, error_print);
                    updateTheRest(newEvent, [p1]);
                } else {
                    Events.post(_.pick(newEvent, create_fields)).then(function(obj) {
                        newEvent = angular.extend(newEvent, obj);
                        updateTheRest(newEvent);
                    }, error_print);
                }
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.openReassignEvent = function(obj, parent_obj) {
            //console.log(Users);
            var modalInstance = $modal.open({
                templateUrl: 'spt/reassignEventTemplate.html',
                controller: 'ModalReassignCtrl',
                keyboard: false,
                resolve: {
                    plan: function() {
                        return $scope.planTree;
                    },
                    current: function() {
                        return {
                            obj: obj,
                            parent_obj: parent_obj
                        };
                    },
                    ext_data: function() {
                        return Users.load;
                    },
                }
            });

            modalInstance.result.then(function(newFolder) {
                // newGroup.position.items = newGroup.position.items  || [];
                // newGroup.position.items.push({id: 'xx', title: newGroup.title, otype: 'group'});
                // var newFolder = result.obj;
                //alert("444");
                console.log(newFolder);
                Events.one(newFolder.id).patch({
                    assignees: newFolder.assignees,
                });
                refresh_plans();
//                if (newFolder.id) {
//                    Folders.one(newFolder.id).patch({
//                        parent_node: newFolder.parent_node,
//                        title: newFolder.title,
//                        manager: newFolder.manager,
//                    }).then(function() {
//                        refresh_plans();
//                    }, error_print);
//                    // newFolder.$update();        
//                } else {
//                    Folders.post(newFolder).then(function(obj) {
//                        refresh_plans();
//                    }, error_print);
//                }
                // $scope.$emit('update');
            }, function() {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        var file_event;
        var file_index;
        var file_exec;
//alert(csrftoken);

        var uploader = $scope.uploader = $fileUploader.create({
            scope: $scope, // to automatically update the html. Default: $rootScope

            headers: {
                'X-CSRFToken': csrftoken // X-CSRF-TOKEN is used for Ruby on Rails Tokens
            },
            formData: [{
                    //key: 'value'
                }],
            autoUpload: true,
        });



        $scope.onFileSelect = function(exec, event, index) {
            //alert();
            //uploader.queue[0]
            //$files: an array of files selected, each file has name, size, and type.
            uploader.formData = [{
                    index: index,
                    eventID: event.id
                }];
            file_event = event;
            file_index = index;
            file_exec = exec;
            uploader.url = 'api/v1/event/' + event.id + '/file_upload'
            //console.log(uploader.queue[0].);

        }
        // ADDING FILTERS

//        uploader.filters.push(function(item) { // second user filter
//            console.info('filter2');
//            return true;
//        });
//
//        // REGISTER HANDLERS
//
//        uploader.bind('afteraddingfile', function(event, item) {
//            console.info('After adding a file', item);
//        });
//
//        uploader.bind('afteraddingall', function(event, items) {
//            console.info('After adding all files', items);
//        });
//
        uploader.bind('beforeupload', function(event, item) {
            console.info('Before upload', item, event);
        });
//
//        uploader.bind('progress', function(event, item, progress) {
//            console.info('Progress: ' + progress, item);
//        });
//
        uploader.bind('success', function(event, xhr, item, response) {
//            file_event.sub_events[file_index].attachment = [{file_name: item.file.name}];
            console.log(file_event);
//            Events.one(file_event.id).patch({
//                sub_events: file_event.sub_events,
//            });
            file_event = null;
            file_index = null;
            file_exec = null;
            refresh_plans();
            console.info('Success', xhr, item, response);

        });
//
//        uploader.bind('cancel', function(event, xhr, item) {
//            console.info('Cancel', xhr, item);
//        });
//
//        uploader.bind('error', function(event, xhr, item, response) {
//            console.info('Error', xhr, item, response);
//        });
//
//        uploader.bind('complete', function(event, xhr, item, response) {
//            console.info('Complete', xhr, item, response);
//        });
//
//        uploader.bind('progressall', function(event, progress) {
//            console.info('Total progress: ' + progress);
//        });
//
//        uploader.bind('completeall', function(event, items) {
//            console.info('Complete all', items);
//        });

        $scope.updateToDate = function(data, exec, event, index) {
            console.log(exec);
            event.sub_events[index].to_date = data;
            Events.one(event.id).patch({
                sub_events: event.sub_events,
            });
        };
        $scope.updateFromDate = function(data, exec, event, index) {
            console.log(exec);
            event.sub_events[index].from_date = data;
            Events.one(event.id).patch({
                sub_events: event.sub_events,
            });
        };
        $scope.updateProgress = function(data, exec, event, index) {
            console.log(data);
            event.sub_events[index].progress = parseInt(data);
            Events.one(event.id).patch({
                sub_events: event.sub_events,
            });
            //refresh_plans();
        };
        $scope.updateComment = function(data, exec, event, index) {
            console.log(data);
            event.sub_events[index].comment = data;
            Events.one(event.id).patch({
                sub_events: event.sub_events,
            });
            //refresh_plans();
        };
        $scope.fileDelete = function(name, index, eventId){
            console.log(name+' '+index+' '+eventId);
            
            $.post('api/v1/event/'+eventId+'/file_delete', { file_name: name, index: index, eventID: eventId }, function(data) {
                if(data==1){
                alert("Файл успешно удален");
            }else{
                alert("Файл не находится в базе");
            }
            });
            refresh_plans();
            
        }
    }]);

app.run(function(editableOptions) {
    editableOptions.theme = 'bs2';

});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}



