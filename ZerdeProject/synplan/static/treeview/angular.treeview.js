/*
	@license Angular Treeview version 0.1.6
	ⓒ 2013 AHN JAE-HA http://github.com/eu81273/angular.treeview
	License: MIT


	[TREE attribute]
	angular-treeview: the treeview directive
	tree-id : each tree's unique id.
	tree-model : the tree model on $scope.
	node-id : each node's id
	node-label : each node's label
	node-children: each node's children

	<div
		data-angular-treeview="true"
		data-tree-id="tree"
		data-tree-model="roleList"
		data-node-id="roleId"
		data-node-label="roleName"
		data-node-children="children" >
	</div>
*/

(function ( angular ) {
	'use strict';

	angular.module( 'angularTreeview', [] ).directive( 'treeModel', ['$compile', 'Nav', function( $compile, Nav ) {
		return {
			restrict: 'A',
			link: function ( scope, element, attrs ) {
				//tree id
				var treeId = attrs.treeId;
			
				//tree model
				var treeModel = attrs.treeModel;

				//node id
				var nodeId = attrs.nodeId || 'id';

				var selectEnabled = !angular.isDefined(attrs.treeSelection) || attrs.treeSelection !== "false";

				//node label
				var nodeLabel = attrs.nodeLabel || 'label';

				//children
				var nodeChildren = attrs.nodeChildren || 'children';

				var nodeExtra = attrs.nodeExtra || '';

				var nodeTpl = '[[node.' + nodeLabel + ']]';
				if (attrs.nodeTpl){
					nodeTpl = attrs.nodeTpl;
				}
				var nodeTemplate = nodeTpl.replace(/\[\[/g, '{{').replace(/\]\]/g, '}}');

				//tree template
				var template =
					'<ul>' +
						'<li data-ng-repeat="node in ' + treeModel + '">' +
							'<i class="collapsed" data-ng-class="node.node_type" data-ng-show="node.' + nodeChildren + '.length && node.collapsed" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' +
							'<i class="expanded" data-ng-class="node.node_type" data-ng-show="node.' + nodeChildren + '.length && !node.collapsed" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' +
							'<i class="normal" data-ng-class="node.node_type" data-ng-hide="node.' + nodeChildren + '.length"></i> ' +
							'<span '+ nodeExtra +' '+ (selectEnabled ? 'data-ng-class="node.selected" data-ng-click="' + treeId + '.selectNodeLabel(node)"' : '') +'>' + nodeTemplate + '</span>' +
							'<div data-tree-selection="' + attrs.treeSelection +'" data-node-extra="' + nodeExtra + '" data-ng-hide="node.collapsed" data-tree-id="' + treeId + '" data-tree-model="node.' + nodeChildren + '" data-node-id=' + nodeId + ' data-node-tpl="'+ nodeTpl +'" data-node-children=' + nodeChildren + '></div>' +
						'</li>' +
					'</ul>';


				//check tree id, tree model
				if( treeId && treeModel ) {

					//root node
					if( attrs.angularTreeview ) {
					
						var container = scope.$eval(treeId);
						if (!angular.isDefined(container)){
							scope.$eval(treeId + ' = {};');
							var container = scope.$eval(treeId);
						}
						//create tree object if not exists
						// scope[treeId] = scope[treeId] || {};

						//if node head clicks,
						container.selectNodeHead = container.selectNodeHead || function( selectedNode ){
							//Collapse or Expand
							selectedNode.collapsed = !selectedNode.collapsed;
							Nav.set_cookie(selectedNode[nodeId], selectedNode.collapsed);
						};

						//if node label clicks,
						container.selectNodeLabel = container.selectNodeLabel || function( selectedNode ){

							//remove highlight from previous node
							if( container.currentNode && container.currentNode.selected ) {
								container.currentNode.selected = undefined;
							}

							//set highlight to selected node
							selectedNode.selected = 'selected';
							Nav.set_cookie('selected', selectedNode[nodeId]);

							//set currentNode
							container.currentNode = selectedNode;
						};
					}

					//Rendering template.
					element.html('').append( $compile( template )( scope ) );
				}
			}
		};
	}]);
})( angular );
