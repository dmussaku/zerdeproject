(function ( angular ) {
	'use strict';

	angular.module( 'ngGroupTable', [] ).directive( 'groupTable', ['$compile', function( $compile ) {
		return {
			restrict: 'A',
			link: function ( scope, element, attrs ) {
				console.log(attrs);
				//tree id
				var tableId = attrs.tableId;

				//tree model
				var groupModel = attrs.groupModel;

				//node id
				var nodeId = attrs.nodeId || 'id';

				//node label
				var nodeLabel = attrs.nodeLabel || 'label';

				//children
				var nodeChildren = attrs.nodeChildren || 'children';

				//tree template
				var template =
					'<table>' +
					'<tr>' +
					'<th>Номер</th>' +
					'<th>Название</th>' +
					'<th></th>' +
					'<th></th>' +
					'<th></th>' +
					'</tr>' +
						'<tr data-ng-repeat="node in ' + groupModel + '">' +
							'<span data-ng-click="' + tableId + '.selectNodeLabel(node)">{{node.' + nodeLabel + '}}</span>' +
							'<div  data-group-table="true" data-table-id="' + tableId + '" data-group-model="node.' + nodeChildren + '" data-node-id=' + nodeId + ' data-node-label=' + nodeLabel + ' data-node-children=' + nodeChildren + '></div>' +
						'</tr>' +
					'</table>';

							// '<i class="collapsed" data-ng-show="node.' + nodeChildren + '.length && node.collapsed" data-ng-click="' + tableId + '.selectNodeHead(node)">s</i>' +
							// '<i class="expanded" data-ng-show="node.' + nodeChildren + '.length && !node.collapsed" data-ng-click="' + tableId + '.selectNodeHead(node)">x</i>' +
							// '<i class="normal" data-ng-hide="node.' + nodeChildren + '.length">h</i> ' +


				//check tree id, tree model
				if( tableId && groupModel ) {

					// //root node
					// if( attrs.angularTreeview ) {

						//create tree object if not exists
						scope[tableId] = scope[tableId] || {};

						//if node head clicks,
						scope[tableId].selectNodeHead = scope[tableId].selectNodeHead || function( selectedNode ){

							//Collapse or Expand
							selectedNode.collapsed = !selectedNode.collapsed;
						};

						//if node label clicks,
						scope[tableId].selectNodeLabel = scope[tableId].selectNodeLabel || function( selectedNode ){

							//remove highlight from previous node
							if( scope[tableId].currentNode && scope[tableId].currentNode.selected ) {
								scope[tableId].currentNode.selected = undefined;
							}

							//set highlight to selected node
							selectedNode.selected = 'selected';

							//set currentNode
							scope[tableId].currentNode = selectedNode;
						};
					// }

					//Rendering template.
					element.html('').append( $compile( template )( scope ) );
				}
			}
		};
	}]);
})( angular );