# -*- coding: utf-8 -*-
# Django settings for synplan project.
import os
import sys
import imp
from configurations import Settings
from configurations.utils import uppercase_attributes
import mongoengine

import djcelery
djcelery.setup_loader()


def rel(*x):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)

sys.path.insert(0, rel('apps'))


def FileSettings(path):
    path = os.path.expanduser(path)

    class Holder(object):

        def __init__(self, *args, **kwargs):
            mod = imp.new_module('synplan.local')
            mod.__file__ = path

            try:
                execfile(path, mod.__dict__)
            except IOError, e:
                print("Notice: Unable to load configuration file %s (%s), "
                      "using default settings\n\n" % (path, e.strerror))

            for name, value in uppercase_attributes(mod).items():
                setattr(self, name, value)

    return Holder


class Base(Settings):

    @classmethod
    def post_setup(cls):
        super(Base, cls).post_setup()
        from django.conf import settings
        print 'Connecting to DB:', settings.MONGODB_NAME
        mongoengine.connect(settings.MONGODB_NAME, tz_aware=settings.USE_TZ)

    DEBUG = True
    TEMPLATE_DEBUG = DEBUG

    MONGODB_NAME = 'synplan'

    # direct to another sys path
    PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))

    ACCOUNTANT_EMAILS = (
    )
    ADMINS = (
    )
    MANAGERS = ()

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.dummy'
        }
    }
    SESSION_ENGINE = 'mongoengine.django.sessions'
    AUTH_USER_MODEL = 'mongo_auth.MongoUser'
    MONGOENGINE_USER_DOCUMENT = 'home.auth_backend.SynergyUser'
    AUTHENTICATION_BACKENDS = (
        'home.auth_backend.SynergyBackend',
        'mongoengine.django.auth.MongoEngineBackend',
    )
    # DATABASES = {
    #     'default': {
    #         'ENGINE': 'django.db.backends.sqlite3',
    #         'NAME': rel('..', '..', 'synplan.db'),
    #         # 'USER': '',
    #         # 'PASSWORD': '',
    #         # 'HOST': '',
    #         # 'PORT': '',
    #     }
    # }

    # CACHES = {
    #     'default': {
    #         'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
    #         'LOCATION': '127.0.0.1:11211',
    #     }
    # }
    # SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

    APPEND_SLASH = False
    TASTYPIE_ALLOW_MISSING_SLASH = True
    # API_LIMIT_PER_PAGE = 1

    LOGIN_REDIRECT_URL = '/'
    LOGIN_URL = '/plan/auth_failed/'
    LOGOUT_URL = '/logout/'
    MANAGERS = ADMINS

    # mailing
    # EMAIL_USE_TLS = False
    # EMAIL_HOST = 'mail.v3na.kz'
    EMAIL_HOST_USER = 'info@synplan.com'
    EMAIL_HOST_PASSWORD = ''
    # EMAIL_PORT = 25
    EMAIL_SUBJECT_PREFIX = '[synplan.com] '
    SERVER_EMAIL = u'KBB <info@synplan.com>'
    DEFAULT_FROM_EMAIL = u'KBB <info@synplan.com>'

    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

    WORK_FORM_FILE = rel('apps', 'home', 'form.asform')
    SUB_EVENTS={'0':[{'name': 'Формирование ТЭО/ФЭО', 'finish_form': 'Проект ТЭО'},{'name': 'Одобрение в ГЧП', 'finish_form': 'Письмо от ГЧП'},{'name': 'Одобрение в МБЭП', 'finish_form': 'Письмо от МБЭП'},{'name': 'Отраслевое заключение', 'finish_form': 'Письмо от МТК'},{'name': 'Утверждение ТЭО/ФЭО', 'finish_form': 'Утвержденный документ'},{'name': 'Отправка бюджетной заявки согласно ТЭО', 'finish_form': 'Бюджетная заявка'},{'name': 'Формирование ТС', 'finish_form': 'Утвержденная ТС'},{'name': 'Проведение конкурсов', 'finish_form': 'Информация'},{'name': 'Заключение договоров', 'finish_form': 'Информация'},{'name': 'Закупка оборудования/услуг по размещению', 'finish_form': 'Акты выполненых работ'},{'name': 'Закупка ЛПО', 'finish_form': 'Акты выполненых работ'},{'name': 'Ввод в опытную эксплуатацию', 'finish_form': 'Приказ ввода в опытную'},{'name': 'Проведение приемо-сдаточных испытаний', 'finish_form': 'Протокола'},{'name': 'Ввод в промышленную эксплуатацию', 'finish_form': 'Приказ ввода в промышленную'},{'name': 'Депонирование', 'finish_form': 'Форма подтверждения'},{'name': 'Проведение аттестации на ИБ', 'finish_form': 'Аттестат'}],'1':[{'name': 'Разработка проекта', 'finish_form': 'Проект'},{'name': 'Согласование с ГО по списку', 'finish_form': 'Письма от ГО по списку'},{'name': 'Общественные заслушивания', 'finish_form': 'Материалы, протокол'},{'name': 'Внесение законопроекта в КПМ', 'finish_form': 'Письмо в КПМ'},{'name': 'Внесение законопроекта в Мажилис', 'finish_form': 'Форма завершения?'},{'name': 'Внесение законопроекта в Сенат', 'finish_form': 'Форма завершения?'},{'name': 'Подписание закона', 'finish_form': 'Подписанный закон'}],'2':[{'name': 'Создание рабочей группы', 'finish_form': 'Приказ о создании рабочей группы'},{'name': 'Разработка проекта', 'finish_form': 'Проект документа'},{'name': 'Согласование внутри ГО', 'finish_form': 'Служебная записка'},{'name': 'Согласование с заинтересованными министерствами и ведомствами', 'finish_form': 'Письма ГО, Протокола РГ'},{'name': 'Внесение документа в правительство РК', 'finish_form': 'Письма и аналитический материал'}],'3':[{'name': 'Формирование бюджетной заявки', 'finish_form': 'Бюджетная заявка'},{'name': 'Объявление конкурса', 'finish_form': 'Приказ'},{'name': 'Заключение договоров', 'finish_form': 'Договор'},{'name': 'Принятие работ', 'finish_form': 'Акт'}],'4':[{'name': 'Анализ международного опыта', 'finish_form': 'Утвержденный отчет'},{'name': 'Анализ текущего состояния сферы', 'finish_form': 'Утвержденный отчет'},{'name': 'Выработка предложений', 'finish_form': 'Утвержденный отчет'},{'name': 'Внесение предложений в компетентный орган', 'finish_form': 'Письмо с приложенным отчетом'}]}
    SYNERGY_HOST = 'http://cloudoffice5.v3na.com'
    SYNERGY_PUBLIC = 'http://cloudoffice5.v3na.com'
    SYNERGY_DEV = False
    SYNERGY_MODULE_ID = 'external_planning'
    SYNERGY_RESOURCES = {
        'person': {
            'url': '/Synergy/rest/api/person/auth',
            'is_array': False,
        },
        'users': {
            'url': '/Synergy/rest/api/userchooser/search?showAll=true',
            'is_array': True
        },
        'groups': {
            'url': '/Synergy/rest/api/storage/groups/list',
            'is_array': True
        },
        'auth_key': {
            'url': '/Synergy/rest/api/person/generate_auth_key',
            'is_array': False
        },
        'settings': {
            'url': '/Synergy/rest/api/settings/get',
            'is_array': False
        },
        'create_work': {
            'url': '/Synergy/rest/api/workflow/work/create',
            'is_array': False
        },
        'save_work': {
            'url': '/Synergy/rest/api/workflow/work/save',
            'is_array': False
        },
        'work_document': {
            'url': '/Synergy/rest/api/workflow/work/{workID}/document',
            'is_array': False,
            'urlparams': ['workID'],
        },
        'delete_work': {
            'url': '/Synergy/rest/api/workflow/work/delete',
            'is_array': False,
        },
        'transfer_work': {
            'url': '/Synergy/rest/api/workflow/work/transfer',
            'is_array': False
        },
        'works': {
            'url': '/Synergy/rest/api/workflow/works_by_id',
            'is_array': True,
        },
        'subworks': {
            'url': '/Synergy/rest/api/workflow/subworks/{workID}',
            'is_array': True,
            'urlparams': ['workID'],
        },
        'departments': {
            'url': '/Synergy/rest/api/departments/list',
            'is_array': True
        },
        'execution_process': {
            'url': '/Synergy/rest/api/workflow/get_execution_process',
            'is_array': True,
        },
        'attach_file': {
            'url': '/Synergy/rest/api/workflow/work/{workID}/attachment/add',
            'urlparams': ['workID'],
            'is_array': False,
        }
    }

    # Local time zone for this installation. Choices can be found here:
    # http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
    # although not all choices may be available on all operating systems.
    # In a Windows environment this must be set to your system time zone.
    TIME_ZONE = 'Asia/Almaty'

    # Language code for this installation. All choices can be found here:
    # http://www.i18nguy.com/unicode/language-identifiers.html

    LANGUAGE_CODE = 'ru'
    ugettext = lambda s: s
    LANGUAGES = (
        ('kk', ugettext('Kazakh')),
        ('ru', ugettext('Russian')),
    )
    LANGUAGE_COOKIE_NAME = 'lang'
    LOCALE_PATHS = (
        rel('locale'),
    )

    SITE_ID = 1

    # If you set this to False, Django will make some optimizations so as not
    # to load the internationalization machinery.
    USE_I18N = True

    # If you set this to False, Django will not format dates, numbers and
    # calendars according to the current locale.
    USE_L10N = True

    # If you set this to False, Django will not use timezone-aware datetimes.
    USE_TZ = False

    # Absolute filesystem path to the directory
    # that will hold user-uploaded files.
    # Example: "/home/media/media.lawrence.com/media/"
    MEDIA_ROOT = rel('media')
    IMAGES_ROOT = rel('..', 'media', 'images')
    SITE_ROOT = os.path.dirname(os.path.realpath(__file__))
    # URL that handles the media served from MEDIA_ROOT. Make sure to use a
    # trailing slash.
    # Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
    MEDIA_URL = '/plan/media/'
    IMAGES_URL = '/plan/media/images/'

    FILE_UPLOAD_TEMP_DIR = rel('temp')
    # Absolute path to the directory static files should be collected to.
    # Don't put anything in this directory yourself; store your static files
    # in apps' "static/" subdirectories and in STATICFILES_DIRS.
    # Example: "/home/media/media.lawrence.com/static/"
    STATIC_ROOT = rel('..', 'static')
    # URL prefix for static files.
    # Example: "http://media.lawrence.com/static/"
    STATIC_URL = '/plan/static/'
    GALLERY_URL = '/plan/static/gallery/'
    # Additional locations of static files
    STATICFILES_DIRS = (
        rel('static'),
        # Put strings here, like "/home/html/static" or "C:/www/django/static".
        # Always use forward slashes, even on Windows.
        # Don't forget to use absolute paths, not relative paths.
    )

    # List of finder classes that know how to find static files in
    # various locations.
    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
        # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
    )

    # Hosts/domain names that are valid for this site; required if DEBUG is
    # False
    # See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
    ALLOWED_HOSTS = ['*']

    # Make this unique, and don't share it with anybody.
    SECRET_KEY = 'st)z1sxs34a0gy=90wygxd9l(2#uxsm!-ni=^6$-nil_^&amp;&amp;@zg'

    # List of callables that know how to import templates from various sources.
    TEMPLATE_LOADERS = (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
        # 'django.template.loaders.eggs.Loader',
    )

    MIDDLEWARE_CLASSES = (
        'django.middleware.common.CommonMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'home.auth_backend.SynergyAuthMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.locale.LocaleMiddleware',
        # 'django_extensions.middlewares.ForceDefaultLanguageMiddleware',
    )

    ROOT_URLCONF = 'synplan.urls'

    # Python dotted path to the WSGI application used by Django's runserver.
    WSGI_APPLICATION = 'synplan.wsgi.application'

    TEMPLATE_DIRS = (
        rel('templates')
        # Put strings here, like "/home/html/django_templates" or
        # "C:/www/django/templates".
        # Always use forward slashes, even on Windows.
        # Don't forget to use absolute paths, not relative paths.
    )

    # BROKER_URL = 'amqp://guest:guest@localhost:5672/'
    BROKER_URL = 'mongodb://localhost:27017/synplan_celery'
    CELERY_IGNORE_RESULT = True
    CELERY_TIMEZONE = 'UTC'
    CELERY_RESULT_BACKEND = "mongodb"
    CELERY_MONGODB_BACKEND_SETTINGS = {
        "host": "localhost",
        "port": 27017,
        "database": "synplan_celery",
        "taskmeta_collection": "taskmeta_collection",
    }
    TEMPLATE_CONTEXT_PROCESSORS = (
        'django.contrib.auth.context_processors.auth',
        'django.core.context_processors.debug',
        'django.core.context_processors.i18n',
        'django.core.context_processors.media',
        'django.core.context_processors.static',
        'django.core.context_processors.tz',
        'django.core.context_processors.request',
        'django.contrib.messages.context_processors.messages',
        'django.core.context_processors.csrf',
    )

    DJANGO_APPS = (
        'django.contrib.contenttypes',
        'django.contrib.auth',
        'django.contrib.sessions',
        # 'django.contrib.sites',
        'django.contrib.messages',
        'django.contrib.staticfiles',
    )

    THIRD_PARTY = (
        'mongoengine.django.mongo_auth',
        'djcelery',
        'django_ses',
        'djangular',
    )

    OUR_APPS = (
        'home',
    )

    INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY + OUR_APPS
    # A sample logging configuration. The only tangible logging
    # performed by this configuration is to send an email to
    # the site admins on every HTTP 500 error when DEBUG=False.
    # See http://docs.djangoproject.com/en/dev/topics/logging for
    # more details on how to customize your logging configuration.
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'root': {
            'level': 'DEBUG',
            'handlers': ['console'],
        },
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse'
            },
        },
        'formatters': {
            'verbose': {
                'format': (
                    '%(levelname)s %(asctime)s %(module)s'
                    ' %(process)d %(thread)d %(message)s')
            },
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'verbose',
            },
            'mail_admins': {
                'level': 'ERROR',
                'filters': ['require_debug_false'],
                'class': 'django.utils.log.AdminEmailHandler'
            }
        },
        'loggers': {
            'django.request': {
                'handlers': ['console'],
                'level': 'ERROR',
                'propagate': False,
            },
            'django.request.tastypie': {
                'handlers': ['console'],
                'level': 'ERROR',
                'propagate': False,
            },
        }
    }
    TASTYPIE_FULL_DEBUG = True
    TASTYPIE_DATETIME_FORMATTING = 'iso-8601-strict'
    ROSETTA_WSGI_AUTO_RELOAD = True
    ROSETTA_UWSGI_AUTO_RELOAD = True
    ROSETTA_REQUIRES_AUTH = False

    USE_X_FORWARDED_HOST = True
#    MEDIA_ROOT = '/Users/mailubai/Sites/zerdeproject/ZerdeProject/synplan/media/'
    

class DevSettings(FileSettings('~/.synplan/synplan.conf.py'), Base):

    SITE_DOMAIN = 'synplan.io:8000'
    DOMAIN_PREFIX = 'http://' + SITE_DOMAIN
    EMAIL_HOST_USER = 'def@synplan.com'
    SERVER_EMAIL = u'KBB <def@synplan.com>'
    DEFAULT_FROM_EMAIL = SERVER_EMAIL
    SYNERGY_DEV = '1:1'
    INSTALLED_APPS = Base.INSTALLED_APPS + (
        'django_extensions',
    )


class ProdSettings(FileSettings('~/.synplan/synplan.conf.py'), Base):

    DEBUG = False

    ADMINS = (
        ('German', 'germanilyin@gmail.com'),
    )

    MONGODB_NAME = 'prod_zerplan'
    MEDIA_ROOT = os.path.expanduser('~/.synplan/media/')
#    MEDIA_ROOT = '/Users/mailubai/Sites/zerdeproject/ZerdeProject/synplan/media/'
    STATIC_ROOT = os.path.expanduser('~/.synplan/static/')
    IMAGES_ROOT = rel(MEDIA_ROOT, 'images')
    SITE_DOMAIN = 'synplan.com'
    DOMAIN_PREFIX = 'http://' + SITE_DOMAIN
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.expanduser('~/.synplan/synplan.db'),
        }
    }
