from home.models import *
from home import events as wutils
from django.conf import settings
import traceback
import logging
log = logging.getLogger()


def m1():
    for w in Work.objects():
        w.data = w.data.get('work', None) or w.data
        w.save()


def m2():
    Folder.objects.update(set__manager=None)
    for f in Folder.objects.all():
        f.manager = ExtUser(
            userID=f.creator.extra.get('userid'),
            name=f.creator.extra.get('firstname'))
        f.save()


def m3():
    try:
        Event.objects()
    except:
        pass
    for ev in Event._collection.find():
        try:
            if ev.get('assignees'):
                for idx, ass in enumerate(ev.get('assignees')):
                    resp = ExtUser(userID=ass.get('resp'), name='Unknown')
                    coworkers = [
                        ExtUser(userID=u, name='Unknown') for u in ass.get('coworkers', [])]
                    Event.objects.filter(pk=ev.get('_id')).update(**{
                        'set__assignees__%s__resp' % idx: resp,
                        'set__assignees__%s__coworkers' % idx: coworkers,
                    })
        except Exception:
            traceback.print_exc()
        try:
            if ev.get('detailers'):
                for idx, det in enumerate(ev.get('detailers')):
                    resp = ExtUser(userID=det.get('resp'), name='Unknown')
                    Event.objects.filter(pk=ev.get('_id')).update(**{
                        'set__detailers__%s__resp' % idx: resp,
                    })
        except Exception:
            traceback.print_exc()


def m4():
    try:
        Plan.objects()
    except:
        pass
    for p in Plan._collection.find():
        Plan.objects.filter(pk=p.get('_id')).update(
            set__detailer=ExtUser(userID=p.get('detailer'), name='Unknown'))


def m5():
    for p in Plan.objects(parent_event__ne=None):
        p.parent_node = None
        p.save()


def m6():
    """ added fields to work """
    for e in Event.objects():
        for ass in e.assignees:
            for w in ass.works:
                w.work_type = 'assignment'
                w.resp = ass.resp
                w.coworkers = ass.coworkers
                w.parent_event = e
                w.parent_plan = e.plan
                if w.data and 'start_date' in w.data:
                    w.from_date = datetime.datetime.strptime(
                        w.data['start_date'], "%Y-%m-%d %H:%M:%S")
                if w.data and 'finish_date' in w.data:
                    w.to_date = datetime.datetime.strptime(
                        w.data['finish_date'], "%Y-%m-%d %H:%M:%S")
                if w.data and 'completionFormID' in w.data:
                    w.completion_form = w.data['completionFormID']
                w.save()
        for det in e.detailers:
            w = det.work
            if w:
                w.work_type = 'detalization'
                w.resp = det.resp
                w.parent_event = e
                w.parent_plan = e.plan
                if w.data and 'start_date' in w.data:
                    w.from_date = datetime.datetime.strptime(
                        w.data['start_date'], "%Y-%m-%d %H:%M:%S")
                if w.data and 'finish_date' in w.data:
                    w.to_date = datetime.datetime.strptime(
                        w.data['finish_date'], "%Y-%m-%d %H:%M:%S")
                if w.data and 'completionFormID' in w.data:
                    w.completion_form = w.data['completionFormID']
                w.save()


def m7():
    """ parent_plan in Plan """
    for p in Plan.objects(parent_event__ne=None):
        p.parent_plan = p.parent_event.plan
        p.save()


def m8():
    """ create static filters """
    col = me.connect(settings.MONGODB_NAME)[settings.MONGODB_NAME]['node']
    for e in col.find({}):
        if e['_cls'].endswith('Folder'):
            continue
        new_cls = e['_cls'].split('.')
        while 'Content' in new_cls:
            new_cls.remove('Content')
        new_cls.insert(1, 'Content')
        new_cls = ".".join(new_cls)
        col.update({'_id': e['_id']}, {'$set': {'_cls': new_cls}})
    # col.update({}, {'$set': {'_cls'}})


def m9():
    """ string uuid """
    for e in Event.objects():
        for ass in e.assignees:
            ass.uuid = unicode(ass.uuid)
        for det in e.detailers:
            det.uuid = unicode(det.uuid)
        e.save()


def restore_pw():
    """ restore works and det plans """
    for p in Plan.objects(parent_event__ne=None):
        p.parent_node = None
        p.parent_folders = []
        for det in p.parent_event.detailers:
            if det.resp.userID == p.detailer.userID:
                det.plan = p
                p.parent_folders = list(Folder.objects(
                    manager__userID=det.resp.userID))
        p.parent_event.save()
        p.save()

    for p in Plan.objects(parent_event=None, parent_node__ne=None):
        p.parent_folders = [p.parent_node]
        p.parent_node = None
        p.save()

    for w in Work.objects(parent_event__ne=None):
        w.parent_node = None
        if w.work_type == 'detalization':
            for det in w.parent_event.detailers:
                if det.resp.userID == w.resp.userID and det.plan_title in w.title:
                    det.work = w
                    w.parent_folders = list(Folder.objects(
                        manager__userID=det.resp.userID))
        else:
            for ass in w.parent_event.assignees:
                if w.parent_event.title in w.title:
                    ass.works = ass.works or []
                    ass.works.append(w)
                    userids = [c.userID for c in ass.coworkers]
                    userids.append(ass.resp.userID)
                    w.parent_folders = list(Folder.objects(
                        manager__userID__in=userids))
        w.parent_event.save()
        w.save()


def m10():
    """ recreate works """
    for e in Event.objects():
        for ass in e.assignees:
            for work in ass.works:
                if hasattr(work, 'work_id'):
                    work_utils.delete_work(work, e.plan.owner)
                    work.delete()

            ass.works = wutils.create_assignment_works(
                ass, e, user=e.plan.owner)


def m11():
    """ from_date, to_date on event """
    for e in Event.objects():
        e.from_date = e.plan.from_date
        e.to_date = e.plan.to_date
        e.save()


def m12():
    """ work has a creator """
    for w in Work.objects.all():
        w.creator = w.parent_plan.owner
        w.save()


def m13():
    """ event has a creator """
    for e in Event.objects.all():
        e.creator = e.plan.owner
        e.save()


def m14():
    """ get user.user """
    for user in SynergyUser.objects():
        user_id = user.extra['userid']
        name = user.extra['lastname']
        if user.auth_key:
            try:
                res = synergy.get(
                    'users',
                    search=name,
                    username='$key',
                    password=user.auth_key).json()
                for data in res:
                    if data['userID'] == user_id:
                        user.user = data
                        break
            except synergy.AuthError:
                log.error((
                    u'AUTH Failed with the old key for userID={} name={}'
                    u' requesting new one').format(
                        user_id, name))


def delete_junk_works():
    """ delete works that has no plan """
    Work.objects(parent_plan=None).delete()
    Work.objects(parent_event=None).delete()


def resave_nodes():
    """ resave nodes """
    for node in Node.objects():
        node.save()
