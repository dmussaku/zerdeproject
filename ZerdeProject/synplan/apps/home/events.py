# -*- coding: utf-8 -*-
from home import models
import traceback
import mongoengine as me
from home.model_utils import post_change
from django.conf import settings
from home.synergy import synergy
from home.utils import periodic_dates, get_quarter
from django.utils import timezone
from dateutil.parser import parse
import datetime
import logging
log = logging.getLogger()


def pick(obj, fields=None, exclude=None):
    odict = obj._data
    res = {}
    assert fields or exclude
    if exclude:
        fields = set(obj._fields.keys()) - set(exclude)
    if fields:
        print fields
        for key in fields:
            res[key] = odict[key]
    return res


def update_event(event_id, changes, user=None):
    event = models.Event.objects.get(id=event_id)
    user = user or event.creator
    if 'title' in changes:
        for ass in event.assignees:
            for w in ass.works:
                if not isinstance(w, models.Work):
                    continue
                w.title = event.title
                if w.work_id:
                    res = synergy.save_work(
                        actionID=w.work_id,
                        name=event.title,
                        startDate=w.from_date,
                        finishDate=w.to_date,
                        completionFormID=w.completion_form,
                        priority=1,
                        username='$key',
                        password=user.auth_key)
                    w.data = res.get('work')
                w.save()

        # for det in event.detailers:
        #     w = det.work
        #     if not isinstance(w, models.Work):
        #         continue
        #     w.title = u'Детализировать "{}"'.format(event.title)
        #     w.save()
        #     if w.work_id:
        #         res = synergy.save_work(
        #             actionID=w.work_id,
        #             name=event.title,
        #             startDate=w.from_date,
        #             finishDate=w.to_date,
        #             completionFormID=w.completion_form,
        #             priority=1,
        #             username='$key',
        #             password=user.auth_key)
        #         print res


class WorkUtils(object):

    def get_work_user(self, work):
        return models.SynergyUser.objects.filter(
            extra__userid=work.author_id).first()

    def save_work(self, work, user=None):
        user = user or self.get_work_user(work)
        if not work.work_id:
            return
        res = synergy.save_work(
            actionID=work.work_id,
            name=work.title,
            startDate=work.from_date,
            finishDate=work.to_date,
            priority=1,
            completionFormID=work.completion_form,
            username='$key',
            password=user.auth_key)
        log.debug(res)
        return res

    def change_duration(self, work, duration, user=None):
        work.to_date = work.from_date + datetime.timedelta(
            days=duration)
        return self.save_work(work, user=user)

    def get_det_title(self, plan_title):
        return u'Детализировать "{}"'.format(plan_title)

    def change_title(self, work, title, det=False, user=None):
        if det:
            title = self.get_det_title(title)
        work.title = title
        return self.save_work(work, user=user)

    def change_responsible(self, work, resp, user=None):
        user = user or self.get_work_user(work)
        work.resp = resp
        if not work.work_id:
            return
        res = synergy.post_json(
            'transfer_work',
            workID=work.work_id,
            name=work.title,
            userID=resp.userID,
            username='$key',
            password=user.auth_key)
        log.debug(res)
        return res

    def delete_work(self, work, with_subworks=True, user=None):
        user = user or self.get_work_user(work)
        if with_subworks and work.work_id:
            res = synergy.get_json(
                'subworks',
                workID=work.work_id,
                username='$key',
                password=user.auth_key)
            log.debug(res)
            for item in res:
                actionID = item.get('actionID', None)
                if actionID is None:
                    continue
                res = synergy.get_json(
                    'delete_work',
                    workID=actionID,
                    username='$key',
                    password=user.auth_key)
                log.debug(res)
        # raise synergy.RequestError('s')
        if work.work_id:
            res = synergy.get_json(
                'delete_work',
                workID=work.work_id,
                username='$key',
                password=user.auth_key)
            log.debug(res)
            work.work_id = None
            return res

work_utils = WorkUtils()


def on_work_delete(sender, document, **kw):
    work_utils.delete_work(document)


me.pre_delete.connect(on_work_delete, sender=models.Work)


class FolderCopyContainer(object):

    def __init__(self):
        self.map = {}

    def copy_node(self, node, plan=None, **kw):
        data = pick(node, exclude=[
            'id', 'seq', 'child_nodes', 'path', 'ts',
            'parent_folders', 'assignees', 'plan'])
        NodeCls = node.__class__
        new_node = NodeCls(**data)
        if NodeCls == models.Event:
            new_node.plan = plan
            for detailer in new_node.detailers:
                if detailer.plan:
                    new_plan = self.copy_plan(detailer.plan)
                    detailer.plan = new_plan
                if detailer.parent_nodes:
                    for key, value in detailer.parent_nodes.iteritems():
                        detailer.parent_nodes[key] = map(
                            self.get_new_node_id, value)
        if NodeCls == models.Plan:
            plan = new_node
        for key, value in kw.iteritems():
            setattr(new_node, key, value)
        new_node.save(force_insert=True)
        children = []
        self.map[str(node.id)] = {
            'orig': node,
            'new': new_node,
            'children': children
        }
        # copy child nodes
        for child_node in models.Node.objects.filter(parent_node=node):
            attrs = {
                'plan': plan,
                'parent_node': new_node
            }
            if isinstance(child_node, models.Event):
                # changes assignment dates
                pass
            new_child = self.copy_node(child_node, **attrs)
            children.append((child_node, new_child))
        return new_node

    def get_new_node_id(self, node_id):
        node = self.map.get(node_id)
        if node:
            return str(node.get('new').id)

    def get_new_folder_id(self, dbref):
        node = self.map.get(str(dbref.id))
        if node:
            return node.get('new')

    def copy_plan(self, plan):
        node = self.map.get(str(plan.id))
        if node:
            return node.get('new')
        parent_plan = None
        parent_event = None
        if plan.parent_plan is not None:
            node = self.map.get(str(plan.parent_plan.id))
            if not node:
                parent_plan = self.copy_plan(plan.parent_plan)
            else:
                parent_plan = node.get('new')

        if plan.parent_event is not None:
            node = self.map.get(str(plan.parent_event.id))
            if node:
                parent_event = node.get('new')

        folders = map(
            self.get_new_folder_id, plan._data['parent_folders'])
        new_plan = self.copy_node(
            plan,
            parent_event=parent_event,
            parent_plan=parent_plan,
            from_date=plan.from_date.replace(
                year=plan.from_date.year + 1),
            to_date=plan.to_date.replace(
                year=plan.to_date.year + 1),
            parent_folders=folders)
        return new_plan

    def copy_plans(self, folder):
        for plan in models.Plan.objects(parent_folders=folder):
            # copy original plans
            self.copy_plan(plan)
        node = self.map.get(str(folder.id))
        if node:
            children = node.get('children')
            for orig, new_child in children:
                self.copy_plans(orig)

    def copy(self, folder, **kw):
        new_folder = self.copy_node(folder, **kw)
        self.copy_plans(folder)
        return new_folder


def works_in_range(user, from_date, to_date, skip=None):
    filtr = {
        'resp__userID': user.userID,
        'to_date__gte': from_date,
        'from_date__lte': to_date,
        'parent_plan__ne': None,
    }
    if skip:
        filtr['pk__not__in'] = skip
    # print filtr
    works = models.Work.objects.filter(**filtr).only(
        'id', 'title', 'from_date', 'to_date', 'parent_plan')
    result = {}
    for work in works:
        # print work.id        
        plan_title = work.parent_plan.title
        obj = {
            'title': work.title,
            'from_date': work.from_date,
            'to_date': work.to_date,
            'plan_title': plan_title
        }
        lst = result.setdefault(plan_title, [])
        lst.append(obj)
    return result


def get_user_overlays(user, assign):
    result = []
    skip = map(lambda s: s.id, assign._data.get('works', []))
    if assign.period == 'once':
        from_date = assign.dates[0]
        to_date = assign.dates[1]
        if isinstance(from_date, basestring):
            from_date = parse(from_date)
        if isinstance(to_date, basestring):
            to_date = parse(to_date)
        works = works_in_range(
            user, from_date, to_date, skip=skip)
        if works:
            result.append({
                'from_date': from_date,
                'to_date': to_date,
                'works': works
            })
    else:
        now = timezone.now()
        dates = periodic_dates(
            assign.period,
            map(cast_dates, assign.dates),
            from_date=assign.from_date,
            to_date=assign.to_date)
        for from_date in dates:
            to_date = from_date + datetime.timedelta(days=assign.duration)
            works = works_in_range(
                user, from_date, to_date, skip=skip)
            if works:
                result.append({
                    'from_date': from_date,
                    'to_date': to_date,
                    'works': works
                })
    return result


def create_detalization_plan(det, event, user=None):
    parent_folders = []
    user = user or event.plan.owner
    for _, folders in det.parent_nodes.iteritems():
        parent_folders.extend(folders)
    plan = models.Plan(
        owner=user,
        detailer=det.resp,
        title=det.plan_title,
        parent_event=event,
        parent_plan=event.plan,
        # parent_node=event.plan,
        from_date=event.plan.from_date,
        to_date=event.plan.to_date,
        parent_folders=parent_folders
    )
    plan.save()
    return plan


def create_synergy_work(
    user,
    title,
    from_date,
    to_date,
    resp_id,
    coworker_ids,
        completion_form):
    work_id, data, doc_id = None, None, None
    res = synergy.create_work(
        startDate=from_date,
        finishDate=to_date,
        userID=resp_id,
        resUserID=coworker_ids,
        completionFormID=completion_form,
        authorID=user.extra.get('userid'),
        priority=1,
        name=title,
        username='$key',
        password=user.auth_key)
    work_id = res.get('workID')
    data = res.get('work')
    doc = synergy.get(
        'work_document',
        workID=work_id,
        username='$key',
        password=user.auth_key).json()
    try:
        result = synergy.post(
            'attach_file',
            workID=work_id,
            path='ase:workContainer',
            files=(
                ('body', open(settings.WORK_FORM_FILE, 'rb').read()),
            ),
            username='$key',
            password=user.auth_key).json()
        print 'ADD ATTACHMENT:', result
    except Exception, e:
        log.error(e)
    doc_id = doc.get('documentID')
    return work_id, data, doc_id


def create_work(
    user, event, title, from_date, to_date,
    resp, coworkers=None, completion_form=None,
        work_type='detalization', parent_folders=None):
    if completion_form == "no":
        completion_form = None
    coworker_ids = coworkers and [w.userID for w in coworkers] or []
    work_id = None
    data = {}
    doc_id = None
    now = timezone.now()
    if isinstance(from_date, basestring):
        from_date = parse(from_date)
    if isinstance(to_date, basestring):
        to_date = parse(to_date)
    if from_date.date() <= now.date() <= to_date.date():
        work_id, data, doc_id = create_synergy_work(
            user, title, from_date, to_date,
            resp.userID, coworker_ids, completion_form)
    work = models.Work(
        creator=user,
        title=title,
        work_id=work_id,
        from_date=from_date,
        to_date=to_date,
        completion_form=completion_form,
        resp=resp,
        coworkers=coworkers,
        doc_id=doc_id,
        work_type=work_type,
        parent_event=event,
        parent_plan=event.plan,
        parent_folders=parent_folders,
        data=data)
        # parent_node=event)
    work.save()
    return work


def create_synergy_works():
    now = timezone.now() + datetime.timedelta(days=2)
    for work in models.Work.objects.filter(
            work_id=None, from_date__lte=now):
        try:
            print 'CREATING WORK:', work.title, work.from_date, work.to_date
            coworker_ids = work.coworkers and [
                w.userID for w in work.coworkers] or []
            work_id, data, doc_id = create_synergy_work(
                work.creator, work.title, work.from_date, work.to_date,
                work.resp.userID, coworker_ids, work.completion_form)
            work.work_id = work_id
            work.data = data
            work.doc_id = doc_id
            work.save()
        except:
            traceback.print_exc()


def cast_dates(date):
    if isinstance(date, (list, tuple)):
        return map(cast_dates, date)
    if isinstance(date, basestring):
        if ',' in date:
            return map(int, date.split(','))
        return int(date)
    return date


def create_assignment_works(assign, event, from_now=False, user=None):
    user = user or event.plan.owner
    work_title = event.title
    works = []
    parent_folders = []
    for _, folders in assign.parent_nodes.iteritems():
        parent_folders.extend(folders)
    if assign.period in ('once', 'quarterly'):
        if assign.period == 'once':
            from_date = assign.dates[0]
            to_date = assign.dates[1]
        else:
            quarter, year = map(int, assign.dates)
            from_date, to_date = get_quarter(quarter, year)
        work = create_work(
            user,
            event,
            title=work_title,
            from_date=from_date,
            to_date=to_date,
            resp=assign.resp,
            completion_form=assign.completion_form,
            coworkers=assign.coworkers,
            work_type='assignment',
            parent_folders=parent_folders)
        works.append(work)
    else:
        now = timezone.now()
        from_date = assign.from_date
        if from_now is True:
            from_date = max(now, from_date)
        dates = list(periodic_dates(
            assign.period,
            map(cast_dates, assign.dates),
            from_date=from_date,
            to_date=assign.to_date))
        assign.count = len(dates)
        assign.actual_dates = dates
        assert assign.count < 1000, 'too much repeatitions of this task'
        for from_date in dates:
            # if from_date.date() > now.date():
            #     break
            to_date = from_date + datetime.timedelta(days=assign.duration)
            work = create_work(
                user,
                event,
                title=work_title,
                from_date=from_date,
                to_date=to_date,
                resp=assign.resp,
                completion_form=assign.completion_form,
                coworkers=assign.coworkers,
                work_type='assignment',
                parent_folders=parent_folders)
            works.append(work)
    return works


def recreate_assignment_works(assign, event, user=None):
    now = timezone.now().date()
    kept_works = []
    for work in assign.works:
        if work.from_date.date() < now:
            kept_works.append(work)
        else:
            work_utils.delete_work(work, with_subworks=True, user=user)
            work.delete()
    works = create_assignment_works(assign, event, from_now=True, user=user)
    return kept_works, works


def create_assignment_work(assign, event):
    user = event.plan.owner
    work_title = event.title
    if assign.period == 'once':
        from_date = assign.dates[0]
        to_date = assign.dates[1]
        return create_work(
            user,
            event,
            title=work_title,
            from_date=from_date,
            to_date=to_date,
            resp=assign.resp,
            completion_form=assign.completion_form,
            coworkers=assign.coworkers,
            work_type='assignment')


def create_detalization_work(det, event, plan, user=None):
    from_date = timezone.now()
    to_date = from_date + datetime.timedelta(days=det.duration)
    user = user or event.plan.owner
    work_title = work_utils.get_det_title(plan.title)
    parent_folders = []
    for _, folders in det.parent_nodes.iteritems():
        parent_folders.extend(folders)
    return create_work(
        user,
        event,
        title=work_title,
        from_date=from_date,
        to_date=to_date,
        resp=det.resp,
        work_type='detalization',
        parent_folders=parent_folders)


def event_saved(sender, document, created, changes, **kw):
    try:
        event = document
        if hasattr(event, '_fuck_recursion'):
            return
        event._fuck_recursion = True
        if created:
            update = {}
            if event.assignees:
                for index, assign in enumerate(event.assignees):
                    works = create_assignment_works(assign, event)
                    assign.works = works
                    # update['push__assignees__{}__works'.format(index)] = work
            if event.detailers:
                for index, det in enumerate(event.detailers):
                    plan = create_detalization_plan(det, event)
                    work = create_detalization_work(det, event, plan)
                    det.plan = plan
                    det.work = work
                    # update['set__detailers__{}__plan'.format(index)] = plan
                    # update['set__detailers__{}__work'.format(index)] = work
            # event.update(**update)
        else:
            pass
            # detailer_map = {
            #     str(x.uuid): x for x in changes.get('detailers', [])}
            # assignee_map = {
            #     str(x.uuid): x for x in changes.get('assignees', [])}
            # print assignee_map
            # update = {}
            # for index, det in enumerate(event.detailers):
            #     if det.uuid:
            #         old_detailer = detailer_map.pop(str(det.uuid), None)
            #     if old_detailer and old_detailer.plan and old_detailer.work:
            #         print old_detailer.plan, old_detailer.work
            #         det.plan = det.plan or old_detailer.plan
            #         det.work = det.work or old_detailer.work
            #         # update[
            #         #     'set__detailers__{}__plan'.format(index)] = plan
            #         # update[
            #         #     'set__detailers__{}__work'.format(index)] = work
            #         if det.resp.userID != old_detailer.resp.userID:
            #             # change detailer
            #             if det.plan:
            #                 det.plan.detailer = det.resp
            #                 det.plan.save()
            #             if det.work:
            #                 det.work.change_assignee(det.resp)
            #     else:
            #         if not det.plan:
            #             plan = create_detalization_plan(det, event)
            #             det.plan = plan
            #             # update[
            #             #     'set__detailers__{}__plan'.format(index)] = plan
            #         if not det.work:
            #             work = create_detalization_work(
            #                 det, event, det.plan)
            #             det.work = work
            #             # update[
            #             #     'set__detailers__{}__work'.format(index)] = work
            # # if update:
            # #     event.update(**update)
            # for uuid, det in detailer_map.iteritems():
            #     if det.plan:
            #         plan = det.plan
            #         try:
            #             plan.parent_node = plan.parent_plan.parent_node
            #         except:
            #             plan.parent_node = None
            #         plan.parent_event = None
            #         plan.detailer = None
            #         plan.save()
            #     if det.work:
            #         det.work.delete()
            # # ASSIGNEES
            # for index, assign in enumerate(event.assignees):
            #     if assign.uuid:
            #         old_assignee = assignee_map.pop(str(assign.uuid), None)
            #     if old_assignee:
            #         assign.works = assign.works or old_assignee.works
            #         if assign.resp.userID != old_assignee.resp.userID:
            #             if assign.works:
            #                 for work in assign.works:
            #                     work.change_assignee(assign.resp)
            #     else:
            #         if not assign.works:
            #             work = create_assignment_work(assign, event)
            #             if work:
            #                 event.assignees[index].works.append(work)
            # for uuid, assign in assignee_map.iteritems():
            #     if assign.works:
            #         for work in assign.works:
            #             work.delete()
        event.save()
    except:
        print 'ERROR'
        traceback.print_exc()

# post_change.connect(event_saved, sender=models.Event)


# {u'errorCode': u'0',
#  u'work': {u'actionID': u'777753dd-8615-4bbe-9813-5ccf33061426',
#            u'author': {u'id': u'1', u'name': u'Admin A.A.'},
#            u'can_be_resended': u'false',
#            u'can_change_progress': u'true',
#            u'can_change_state': u'true',
#            u'can_delete': u'true',
#            u'can_edit': u'true',
#            u'can_manage_comments': u'true',
#            u'can_mark_as_seen': u'true',
#            u'can_reassign': u'false',
#            u'can_send_acquaintance': u'true',
#            u'can_send_agreement': u'true',
#            u'can_send_approval': u'true',
#            u'can_send_by_route': u'true',
#            u'can_take_on_control': u'false',
#            u'completionFormID': u'',
#            u'completionResultID': u'',
#            u'finish_date': u'2013-12-05 19:22:00',
#            u'has_attachments': u'false',
#            u'has_subprocesses': u'false',
#            u'is_expired': u'false',
#            u'is_new': u'false',
#            u'is_soon_expired': u'false',
#            u'name': u'Test work',
#            u'parent_process': u'null',
#            u'percent': u'0',
#            u'priority': u'1',
#            u'procInstID': u'',
#            u'process_result': u'',
#            u'remained': u'22.0',
#            u'remained_label': u'22\u0434',
#            u'start_date': u'2013-11-05 19:22:00',
#            u'stateID': u'0',
#            u'user': {u'id': u'1', u'name': u'Admin A.A.'},
#            u'work_state_icon': u'progress_status_icon',
#            u'work_state_label': u'progress'},
#  u'workID': u'777753dd-8615-4bbe-9813-5ccf33061426'}
