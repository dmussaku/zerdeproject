# from django.db import models
from django.utils import timezone
import datetime

# Create your models here.
import mongoengine as me
from blinker import Namespace
_signals = Namespace()

post_change = _signals.signal('post_change')


def reset_state(sender, document, save=True, **kwargs):
    if save:
        changes = document.get_dirty_fields()
        if changes:
            post_change.send(
                sender, document=document, changes=changes, **kwargs)
    document._reset_state()


class DirtyFieldsMixin(object):

    def __init__(self, *a, **kw):
        super(DirtyFieldsMixin, self).__init__(*a, **kw)
        me.post_save.connect(reset_state, sender=self.__class__)
        reset_state(sender=self.__class__, document=self, save=False)

    def _reset_state(self):
        self._original_state = self._as_dict()
        self._clear_changed_fields()

    def _as_dict(self):
        fields = self._meta.get('watch_fields', None) or self._fields.keys()
        return {f: self._data.get(f) for f in fields}

    def get_dirty_fields(self):
        new_state = self._as_dict()
        changes = {}
        for key, value in self._original_state.iteritems():
            # print new_state[key]
            if (isinstance(new_state[key], datetime.datetime) and
                    timezone.is_aware(new_state[key]) and
                    timezone.is_naive(value)):
                new_state[key] = timezone.make_naive(
                    new_state[key], timezone.get_default_timezone())
            if value != new_state[key]:
                changes[key] = value
        return changes

    def is_changed(self, field):
        return self._original_state[field] != getattr(self, field)

    def get_original(self, field):
        return self._original_state[field]

    def is_dirty(self):
        # in order to be dirty we need to have been saved at least once, so we
        # check for a primary key and we need our dirty fields to not be empty
        if not self.pk:
            return True
        return {} != self.get_dirty_fields()
