# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url, include
from django.views.decorators.http import require_GET
from django.views.generic import TemplateView
from django.conf import settings
import os
# from django.conf.urls.static import static
from home.forms import EventForm
from tastypie.api import Api
from home.api import (
    PlanResource,
    EventResource,
    GroupResource,
    FolderResource,
    WorkResource,
    AssignmentOverlay,
    NodeResource
    #SubEventResource,
    #EventTypeResource
    )
from home.synergy import synergy
from django.views.decorators.csrf import ensure_csrf_cookie
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_page
from home import events
from home.views import grouped_users, get_uploaded, ReportDetailView
from django.conf.urls.static import static
import synplan
from home.models import Event


v1_api = Api(api_name='v1')           
v1_api.register(WorkResource())      # http://127.0.0.1:8000/plan/api/v1/work/?format=json
v1_api.register(PlanResource())     # http://127.0.0.1:8000/plan/api/v1/plan/?format=json
v1_api.register(FolderResource())   # http://127.0.0.1:8000/plan/api/v1/folder/?format=json
v1_api.register(EventResource())    # http://127.0.0.1:8000/plan/api/v1/event/?format=json
v1_api.register(GroupResource())    # http://127.0.0.1:8000/plan/api/v1/group/?format=json
v1_api.register(NodeResource()) #  http://127.0.0.1:8000/plan/api/v1/Node/?format=json
v1_api.register(AssignmentOverlay())    # http://127.0.0.1:8000/plan/api/v1/assignment/?format=json
#v1_api.register(EventTypeResource())    # http://127.0.0.1:8000/plan/api/v1/event_types/?format=json
# v1_api.register(SubEventResource()) #


def rel(*x):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)


class StaticViewMixinMixin(TemplateView):
    extra = None

    def get_context_data(self, **kwargs):
        ctx = super(StaticViewMixinMixin, self).get_context_data(**kwargs)
        if self.extra:
            ctx.update(self.extra)
        return ctx


def tmpl(template, name, extra=None):
    return url(
        r'^{}$'.format(template),
        StaticViewMixinMixin.as_view(
            template_name='home/partials/{}'.format(template),
            extra=extra),
        name=name)


partial_patterns = patterns(
    '',
    tmpl(
        'createEventTemplate.html',
        'create_event',
        extra={'form': EventForm()}),
)

urlpatterns = patterns(
    '',
    url(r'^$', login_required(ensure_csrf_cookie(require_GET(
        StaticViewMixinMixin.as_view(
            template_name='home/plan.html',
            extra={'synergy_host': settings.SYNERGY_PUBLIC})))),
        name='home'),
    url(r'^pt/', include(partial_patterns, namespace='partials')),
    url('^spt/(?P<path>.*)$', 'django.views.static.serve', {
        "document_root": rel('templates/home/static_partials')}),
    url(r'^hello/$', require_GET(StaticViewMixinMixin.as_view(
        template_name='home/homepage.html',
        extra={
            'msg': "Hello World!",
        })),
        name='home'),
    url(r'^auth_failed/$', require_GET(StaticViewMixinMixin.as_view(
        template_name='home/auth_fail.html',)),
        name='home'),
    url(r'^api/', include(v1_api.urls)),
    url(r'^synergy/', include(synergy.urls, namespace='synergy')),
    url(
        r'^synergy/grouped_users',
        cache_page(60 * 15)(grouped_users),
        name='synerge_grouped_users'),
#    url (
#        r'^media/$',get_uploaded, name='get_uploaded', 
#        ),


    url(r'^report/(?P<pk>\w+)/$', ReportDetailView.as_view(template_name='report.html'),
        name='reportdetailview'),
    
    url(r'^zerde_report_1/(?P<pk>\w+)/$', ReportDetailView.as_view(template_name='zerde_report_1.html'),
        name='reportdetailview'),

    url(r'^goal_achievement/(?P<pk>\w+)/$', ReportDetailView.as_view(template_name='goal_achievement.html'),
        name='reportdetailview'),

    url(r'^interaction/(?P<pk>\w+)/$', ReportDetailView.as_view(template_name='interaction.html'),
        name='reportdetailview'),

    url(r'^outside_interaction/(?P<pk>\w+)/$', ReportDetailView.as_view(template_name='outside_interaction.html'),
        name='reportdetailview'),

    
    
) 
# + static('/plan/spt/', document_root=rel('templates/home/static_partials'))