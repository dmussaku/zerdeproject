from celery import shared_task
from celery.task import periodic_task
from home import models
from home import events as wutils
from celery.schedules import crontab
from django.conf import settings


@shared_task
def copy_folder(folder_id, folder_title):
    folder = models.Folder.objects.with_id(folder_id)
    if folder:
        cnt = wutils.FolderCopyContainer()
        return cnt.copy(folder, title=folder_title)
    return None

if settings.DEBUG:
    cron = crontab()
else:
    cron = crontab(minute=0, hour='0,7,14,21')


@periodic_task(run_every=cron)
def create_synergy_works():
    print 'CREATING SYNERGY WORKS ON SCHEDULE'
    wutils.create_synergy_works()
