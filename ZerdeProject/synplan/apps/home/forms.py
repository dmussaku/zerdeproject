#coding: utf-8
from django import forms
from djangular.forms.angular_model import (
    NgModelFormMixin)
from djangular.forms.add_placeholder import AddPlaceholderFormMixin

SCOPE_PREFIX = 'my_prefix'


class EventForm(NgModelFormMixin, AddPlaceholderFormMixin, forms.Form):

    def __init__(self, *a, **kw):
        kw['scope_prefix'] = SCOPE_PREFIX
        super(EventForm, self).__init__(*a, **kw)

    title = forms.CharField(label=u'Введите название мероприятия')
    responsible = forms.CharField(label=u'Ответственный')
    assignees = forms.CharField(label=u'Исполнители')
    duration_period = forms.ChoiceField(
        label=u'Исполнители', choices=(
            ('once', u'Единожды'),
            ('weekly', u'Еженедельно'),
            ('monthly', u'Ежемесячно'),
            ('yearly', u'Ежегодно'),
        ))
    duration_date = forms.DateField(label=u'Дата')
