from home.synergy import synergy
from django.contrib import auth
import mongoengine as me
from mongoengine.django.auth import User
from django.conf import settings
import simplejson as json
import logging
log = logging.getLogger()


class SynergyUser(User):
    user = me.DictField()
    extra = me.DictField()
    auth_key = me.StringField()

    def is_configurator(self):
        cf = self.extra.get('configurator', 'false')
        if cf == 'true':
            return True
        return False

    def to_pub_json(self):
        res = {
            'id': str(self.id),
            'username': self.username,
            'extra': self.extra
        }
        return json.dumps(res)


class SynergyBackend(object):
    """Authenticate using Synergy and mongoengine.django.auth.User.
    """

    supports_object_permissions = False
    supports_anonymous_user = False
    supports_inactive_user = False

    def get_or_create_user(self, username, password):
        """
        {
            u'jid': u'null',
            u'code': u'admin',
            u'firstname': u'Admin',
            u'admin': u'true',
            u'lastname': u'Admin',
            u'userid': u'1',
            u'modified': u'1383223840000',
            u'access': u'true',
            u'hr-manager': u'false',
            u'patronymic': u'Admin',
            u'mail': u'null',
            u'pointers-access': u'false',
            u'configurator': u'false'}
        """
        person = synergy.get_person(username=username, password=password)
        user_id = person['userid']
        name = person['lastname']
        login_name = 'synergy${}'.format(user_id)[:30]
        user = SynergyUser.objects(username=login_name).first()
        email = person.get('mail', None)
        if email == u'null' or not email:
            email = '{}@synergy.kz'.format(user_id)
        if not user:
            user = SynergyUser(
                username=login_name,
                email=email,
                extra=person)
        else:
            user.email = email
            user.extra = person
        is_update_key = False
        if user.auth_key:
            try:
                res = synergy.get(
                    'users',
                    search=name,
                    username='$key',
                    password=user.auth_key).json()
                for data in res:
                    if data['userID'] == user_id:
                        user.user = data
                        break
            except synergy.AuthError:
                log.error((
                    u'AUTH Failed with the old key for userID={} name={}'
                    u' requesting new one').format(
                        user_id, name))
                is_update_key = True
        else:
            is_update_key = True
        if is_update_key:
            user.auth_key = synergy.get_auth_key(
                username=username, password=password)
            res = synergy.get(
                'users',
                search=name,
                username=username,
                password=password).json()
            for data in res:
                if data['userID'] == user_id:
                    user.user = data
                    break
        # log.debug("user=%s auth=%s", user.username, user.auth_key)
        user.save()
        return user

    def authenticate(self, username=None, password=None):
        try:
            user = self.get_or_create_user(username, password)
        except synergy.AuthError:
            return None
        except AssertionError, e:
            print e
            return None
        if user:
            return user
        return None

    def get_user(self, user_id):
        return User.objects.with_id(user_id)


class SynergyAuthMiddleware(object):
    sso_key = "sso_hash"

    def process_request(self, request):
        if self.sso_key in request.REQUEST:
            if request.user.is_authenticated():
                auth.logout(request)

            sso_hash = request.REQUEST.get(self.sso_key).replace(' ', '+')
            user = auth.authenticate(username='$session', password=sso_hash)
            if user:
                request.session["SYNERGY_HASH"] = sso_hash
                auth.login(request, user)
                return

        if settings.SYNERGY_DEV:
            if request.user.is_authenticated():
                return
            username, password = settings.SYNERGY_DEV.split(':')
            user = auth.authenticate(
                username=username,
                password=password)
            if user:
                auth.login(request, user)
            return
