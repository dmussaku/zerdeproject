from tastypie_mongoengine import resources
from tastypie import authentication, authorization
# from tastypie.exceptions import BadRequest, ImmediateHttpResponse
from django.core import exceptions
from home import models
from home.models import *
from tastypie_mongoengine import fields
from tastypie.fields import CharField, ListField, IntegerField
# from tastypie.resources import convert_post_to_patch, dict_strip_unicode_keys
from tastypie.exceptions import BadRequest
from tastypie.resources import Resource

from django.conf.urls import url
from tastypie.utils import trailing_slash
from tastypie import http
from mongoengine import Q
from mongoengine.errors import (
    DoesNotExist,
    MultipleObjectsReturned)
# from home.utils import rget
import simplejson as json
import logging
from dateutil import parser
from home.synergy import synergy
from home import events as wutils
from home.events import work_utils
from home import tasks
# import datetime
# from django.utils import timezone
import functools
import os

from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from synplan.settings import MEDIA_ROOT, MEDIA_URL
from tastypie.serializers import Serializer

log = logging.getLogger()

auth_class = authentication.SessionAuthentication
auz_class = authorization.Authorization

# make_naive = functools.partial(
#     timezone.make_naive, timezone=timezone.get_default_timezone())
parse = functools.partial(parser.parse, ignoretz=True)


def get_user_events(**kwargs):
    print "we are at get_user_events method in root and printing kwargs"
    print kwargs

def get_user_folders(user, only=None):
    userid = user.extra.get('userid', None)
    if only and 'seq' not in only:
        tmp = ['seq']
        tmp.extend(only)
        only = tmp
    if user.is_configurator():
        folders = models.Folder.objects.all()
    else:
        #folders = models.Folder.objects.filter(manager__userID=userid)
        folders = models.Folder.objects.all()
    if only:
        folders = folders.only(*only)
    result_set = set()
    for folder in folders:
        if folder in result_set:
            continue
        sub = models.Folder.objects(path__contains=',{},'.format(folder.seq))
        if only:
            sub = sub.only(*only)
        fset = set(sub)
        result_set |= fset
        result_set.add(folder)
    return result_set


def get_user_plans(user, only=None):
    folders = get_user_folders(user, only=['id'])
    qs = models.Plan.objects.filter(parent_folders__in=folders)
    
    print qs
    if only:
        qs = qs.only(*only)
    return qs


class EventAuthorization(authorization.Authorization):
    def read_list(self, object_list, bundle):
        plans = get_user_plans(bundle.request.user)
        qs = object_list.filter(plan__in=plans)
        return qs


class FolderAuthorization(authorization.Authorization):
    def read_list(self, object_list, bundle):
        return object_list.filter(creator=bundle.request.user)

    # def assert_configurator(self, bundle):
    #     if bundle.request.user.extra.get('configurator') != "true":
    #         raise Unauthorized('not a configurator')

    # def create_list(self, object_list, bundle):
    #     self.assert_configurator(bundle)
    #     return object_list

    # def create_detail(self, object_list, bundle):
    #     self.assert_configurator(bundle)
    #     return True

    # def update_list(self, object_list, bundle):
    #     allowed = []

    #     # Since they may not all be saved, iterate over them.
    #     for obj in object_list:
    #         if obj.user == bundle.request.user:
    #             allowed.append(obj)

    #     return allowed

    # def update_detail(self, object_list, bundle):
    #     return bundle.obj.user == bundle.request.user

    # def delete_list(self, object_list, bundle):
    #     # Sorry user, no deletes for you!
    #     raise Unauthorized("Sorry, no deletes.")

    # def delete_detail(self, object_list, bundle):
    #     raise Unauthorized("Sorry, no deletes.")


class PlanAuthorization(authorization.Authorization):
    def read_list(self, object_list, bundle):
        # This assumes a ``QuerySet`` from ``ModelResource``.
        user = bundle.request.user
        # userid = user.extra and user.extra.get('userid', None) or None
        return object_list.filter(owner=user)
    # def read_detail(self, object_list, bundle):
    #     # Is the requested object owned by the user?
    #     return bundle.obj.user == bundle.request.user

    # def create_list(self, object_list, bundle):
    #     # Assuming their auto-assigned to ``user``.
    #     return object_list

    # def create_detail(self, object_list, bundle):
    #     return bundle.obj.user == bundle.request.user

    # def update_list(self, object_list, bundle):
    #     allowed = []

    #     # Since they may not all be saved, iterate over them.
    #     for obj in object_list:
    #         if obj.user == bundle.request.user:
    #             allowed.append(obj)

    #     return allowed

    # def update_detail(self, object_list, bundle):
    #     return bundle.obj.user == bundle.request.user

    # def delete_list(self, object_list, bundle):
    #     # Sorry user, no deletes for you!
    #     raise Unauthorized("Sorry, no deletes.")

    # def delete_detail(self, object_list, bundle):
    #     raise Unauthorized("Sorry, no deletes.")


class PatchListMixin(object):

    def obj_get(self, bundle, **kwargs):
        try:
            print "PatchListMixin obj get bundle"
            print bundle
            print "PatchListMixin obj get kwargs"
            print kwargs
            object_list = getattr(self.instance, self.attribute)
            pk_field = getattr(self._meta, 'id_field', None)
            pk = kwargs.get('subresource_pk', None)
            if not pk:
                return bundle.obj
            # pk = bundle.obj._fields[pk_field].to_python(pk)
            if pk_field is None:
                return object_list[pk]
            else:
                idx = self.find_embedded_document(
                    object_list, pk_field, pk)
                if idx is not None:
                    return object_list[idx]
        except ValueError:
            pass
        raise exceptions.ObjectDoesNotExist

    def patch_list(self, request, **kwargs):
        old_save = self.instance.save

        def monkey_save(*a, **kw):
            pass

        self.instance.save = monkey_save
        res = Resource.patch_list(self, request, **kwargs)
        self.instance.save = old_save
        # # save fails on first element
        # self.instance.update(**{
        #     'set__{}'.format(self.attribute):
        #     getattr(self.instance, self.attribute)
        # })
        self.instance._mark_as_changed(self.attribute)
        self.instance.save()
        return res


class PatchMixin(object):

    def update_in_place(self, request, original_bundle, new_data):
        if set(new_data.keys()) & set(self._meta.unallowed_update_fields):
            raise BadRequest(
                'Update on %s is not allowed' % ', '.join(
                    self._meta.unallowed_update_fields
                )
            )

        return super(PatchMixin, self).update_in_place(
            request, original_bundle, new_data
        )


class BaseResource(resources.MongoEngineResource):
    parent_node = fields.ReferenceField(
        to='home.api.NodeResource',
        attribute='parent_node',
        full=False,
        null=True)
    parent_node_id = CharField(
        attribute='parent_node_id', null=True, readonly=True)

    class Meta:
        abstract = True


class WorkResource(BaseResource):
    resp = fields.EmbeddedDocumentField(
        embedded='home.api.EmbeddedExtUserResource',
        attribute='resp',
        null=True)
    coworkers = fields.EmbeddedListField(
        of='home.api.EmbeddedExtUserResource',
        attribute='coworkers',
        full=True,
        null=True)
    parent_folder_ids = ListField(
        attribute='parent_folder_ids',
        readonly=True,
        null=True)
    # parent_plan = fields.ReferenceField(
    #     to='home.api.PlanResource',
    #     attribute='parent_plan',
    #     full=False,
    #     null=True)

    class Meta:
        queryset = models.Work.objects().order_by('id')
        unallowed_update_fields = [
            'path', 'child_nodes', 'seq',
            'parent_event', 'parent_plan'
        ]
        allowed_methods = ('get', 'post', 'put', 'patch', 'delete')
        excludes = [
            'path', 'child_nodes', 'seq',
            'parent_event', 'parent_plan', 'parent_folders'
        ]
        resource_name = 'work'
        authentication = auth_class()
        authorization = auz_class()


# class FilterResource(BaseResource):
#     parent_node = fields.ReferenceField(
#         to='home.api.NodeResource',
#         attribute='parent_node',
#         full=False,
#         null=True)
#     parent_node_id = CharField(
#         attribute='parent_node__id', null=True, readonly=True)
#     items = fields.ReferenceField(
#         to='home.api.NodeResource',
#         attribute='items',
#         full=True,
#         null=True)

#     class Meta:
#         queryset = models.Work.objects().order_by('id')
#         unallowed_update_fields = [
#             'path', 'child_nodes', 'seq', 'lst',
#         ]
#         allowed_methods = ('get',)
#         excludes = [
#             'path', 'child_nodes', 'seq', 'lst',
#         ]
#         resource_name = 'filter'
#         authentication = auth_class()
#         authorization = auz_class()


class FolderResource(BaseResource):

    manager = fields.EmbeddedDocumentField(
        embedded='home.api.EmbeddedExtUserResource',
        attribute='manager',
        null=True)
    child_nodes = ListField(readonly=True, attribute='child_nodes')
    seq = IntegerField(readonly=True, attribute='seq')

    class Meta:
        queryset = models.Folder.objects().order_by('id')
        unallowed_update_fields = ['path', 'child_nodes', 'seq']
        allowed_methods = ('get', 'post', 'put', 'patch', 'delete')
        excludes = ['path', 'creator']
        resource_name = 'folder'
        authentication = auth_class()
        authorization = FolderAuthorization()

    def obj_create(self, bundle, **kwargs):
        bundle.data['creator'] = bundle.request.user
        return super(FolderResource, self).obj_create(
            bundle, creator=bundle.request.user)

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/(?P<pk>\w[\w/-]*)/copy%s$" % (
                    self._meta.resource_name, trailing_slash()),
                self.wrap_view('copy_folder'),
                name="api_copy_folder"),
        ]

    def copy_folder(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        self.throttle_check(request)
        try:
            bundle = self.build_bundle(
                data={'pk': kwargs['pk']}, request=request)
            obj = self.cached_obj_get(
                bundle=bundle, **self.remove_api_resource_names(kwargs))
        except DoesNotExist:
            return http.HttpGone()
        except MultipleObjectsReturned:
            return http.HttpMultipleChoices(
                "More than one resource is found at this URI.")

        deserialized = self.deserialize(
            request, request.body,
            format=request.META.get('CONTENT_TYPE', 'application/json'))
        if 'title' not in deserialized:
            raise BadRequest("New folder title must be provided")
        tasks.copy_folder(obj.id, deserialized['title'])
        self.log_throttled_access(request)
        return self.create_response(request, {})


class PlanResource(PatchMixin, BaseResource):
    # parent_plan = fields.ReferenceField(
    #     to='home.api.PlanResource',
    #     attribute='parent_plan',
    #     full=False,
    #     null=True)
    ownerID = CharField(
        attribute='ownerID',
        readonly=True,
        null=True)
    detailer = fields.EmbeddedDocumentField(
        embedded='home.api.EmbeddedExtUserResource',
        attribute='detailer',
        null=True)
    parent_folder_ids = ListField(
        attribute='parent_folder_ids',
        readonly=True,
        null=True)
    parent_folders = fields.ReferencedListField(
        of='home.api.FolderResource',
        attribute='parent_folders',
        full=False,
        null=True)

    class Meta:
        queryset = models.Plan.objects().order_by('id')
        unallowed_update_fields = [
            'path', 'child_nodes', 'seq',
            'parent_event', 'parent_plan'
        ]
        allowed_methods = ('get', 'post', 'put', 'patch', 'delete')
        excludes = [
            'path', 'child_nodes',
            'seq', 'owner', 'parent_event', 'parent_plan']
        resource_name = 'plan'
        authentication = auth_class()
        authorization = PlanAuthorization()

    def obj_create(self, bundle, **kwargs):
        return super(PlanResource, self).obj_create(
            bundle, owner=bundle.request.user)


class EmbeddedExtUserResource(PatchListMixin, resources.MongoEngineResource):

    class Meta:
        id_field = 'userID'
        object_class = models.ExtUser


class EmbeddedDepartmentResource(PatchListMixin, resources.MongoEngineResource):

    class Meta:
        id_field = 'departmentID'
        object_class = models.Department



class EmbeddedSynergyAttachmentResource(PatchMixin, resources.MongoEngineResource):
    
    class Meta:
        object_class=models.SynergyAttachment
        id_field='file_name'


class EmbeddedSubEventsResource(PatchListMixin, resources.MongoEngineListResource):
    '''
    synergy_attachment = fields.EmbeddedDocumentField(
        embedded='home.api.EmbeddedSynergyAttachmentResource',
        attribute='attachment',
        null=True,)
    '''
    synergy_attachments = fields.EmbeddedListField(
        of='home.api.EmbeddedSynergyAttachmentResource',
        attribute='attachments',
        full=True, 
        null=True)
    class Meta:
        object_class = models.SubEvent
        id_field = 'name'

    
class EmbeddedAssignmentResource(
        PatchListMixin, resources.MongoEngineListResource):
    
    resp = fields.EmbeddedDocumentField(
        embedded='home.api.EmbeddedExtUserResource',
        attribute='resp',
        null=True
        )
    coworkers = fields.EmbeddedListField(
        of='home.api.EmbeddedExtUserResource',
        attribute='coworkers',
        full=True,
        null=True)
    actual_dates = ListField(attribute='actual_dates', readonly=True)


    class Meta:
        object_class = models.Assignment
        id_field = 'uuid'
        excludes = ['works']
        # work_influencers = [
        #     'period', 'dates',
        #     'from_date', 'to_date', 'duration',
        #     'resp', 'coworkers', 'completion_form']

    def hydrate(self, bundle, **kw):
        print "Used Method Hydrate"
        bundle = super(EmbeddedAssignmentResource, self).hydrate(bundle, **kw)
        period = bundle.data.get('period')
        if period == 'once':
            dates = bundle.data.get('dates', [])
            bundle.data['dates'] = map(parse, dates)
        return bundle

    def obj_create(self, bundle, **kwargs):
        bundle = super(
            EmbeddedAssignmentResource, self).obj_create(bundle, **kwargs)
        bundle.obj.works = wutils.create_assignment_works(
            bundle.obj, self.instance)
        print 'CREATE', bundle.obj.uuid
        return bundle

    def on_dates_change(self, bundle, old_value=None, changes=None):
        period = bundle.data.get('period')
        if period == 'once':
            from_date, to_date = bundle.obj.dates
            work = bundle.obj.works and bundle.obj.works[0] or None
            if isinstance(work, models.Work):
                work.from_date = from_date
                work.to_date = to_date
                work_utils.save_work(work, user=bundle.request.user)
                work.save()
        return bundle

    def on_parent_nodes_change(self, bundle, **kw):
        from bson import DBRef, ObjectId
        parent_folders = []
        for _, folders in bundle.obj.parent_nodes.iteritems():
            for folder in folders:
                parent_folders.append(DBRef(
                    models.Folder._get_collection_name(),
                    ObjectId(folder)))
        for work in bundle.obj.works:
            if isinstance(work, models.Work):
                work.parent_folders = parent_folders
                work.save()
        return bundle

    def on_resp_change(self, bundle, **kw):
        for work in bundle.obj.works:
            if (isinstance(work, models.Work) and
                    work.resp.userID != bundle.obj.resp.userID):
                work_utils.change_responsible(
                    work, bundle.obj.resp,
                    user=bundle.request.user)
                work.save()
        return bundle

    def on_completion_form_change(self, bundle, **kw):
        val = bundle.obj.completion_form
        val = val != "no" and val or None
        for work in bundle.obj.works:
            if isinstance(work, models.Work) and work.completion_form != val:
                work.completion_form = val
                work_utils.save_work(work, user=bundle.request.user)
                work.save()
        return bundle

    def on_duration_change(self, bundle, changes=None, **kw):
        duration = bundle.obj.duration
        for work in bundle.obj.works:
            if isinstance(work, models.Work):
                td = work.to_date - work.from_date
                if td.days != duration:
                    work_utils.change_duration(
                        work, duration,
                        user=bundle.request.user)
                    work.save()
        return bundle

    # def pop_influencers(self, changes):
    #     for key in self._meta.work_influencers:
    #         changes.pop(key, None)

    def obj_update(self, bundle, **kwargs):

        bundle = super(
            EmbeddedAssignmentResource, self).obj_update(bundle, **kwargs)
        changes = bundle.obj.get_dirty_fields()
        event = self.instance
        kept_works, new_works = None, None
        if any(map(lambda s: s in changes, ['period', 'from_date', 'to_data'])):
            kept_works, new_works = wutils.recreate_assignment_works(
                bundle.obj, event, user=bundle.request.user)
            bundle.obj.works = kept_works
            # self.pop_influencers(changes)
        elif 'dates' in changes and bundle.obj.period != 'once':
            kept_works, new_works = wutils.recreate_assignment_works(
                bundle.obj, event, user=bundle.request.user)
            bundle.obj.works = kept_works
            # self.pop_influencers(changes)
        for field, value in changes.iteritems():
            func = getattr(self, 'on_{}_change'.format(field), None)
            if callable(func):
                bundle = func(bundle, old_value=value, changes=changes)
            else:
                log.debug('No change handler for {}.{}'.format(
                    bundle.obj.__class__.__name__,
                    field))
        if new_works and kept_works:
            kept_works.extend(new_works)
            bundle.obj.works = kept_works
        log.debug('UPDATE %s', bundle.obj.uuid)
        return bundle

    def obj_delete(self, bundle, **kwargs):
        super(EmbeddedAssignmentResource, self).obj_delete(bundle, **kwargs)
        for work in bundle.obj.works:
            if isinstance(work, models.Work):
                work_utils.delete_work(work, bundle.request.user)
                work.delete()
        print 'DELETE', bundle.obj.uuid
        # print bundle.obj._get_changed_fields()
        # self.works = wutils.create_assignment_works(self, self.instance)
        return bundle


class EmbeddedDetalizationResource(
        PatchListMixin, resources.MongoEngineListResource):
    resp = fields.EmbeddedDocumentField(
        embedded='home.api.EmbeddedExtUserResource',
        attribute='resp',
        null=True)
    # plan = fields.ReferenceField(
    #     to='home.api.PlanResource',
    #     attribute='plan',
    #     # readonly=True,
    #     full=False,
    #     null=True)
    # work = fields.ReferenceField(
    #     to='home.api.WorkResource',
    #     attribute='work',
    #     # readonly=True,
    #     full=False,
    #     null=True)

    class Meta:
        object_class = models.Detalization
        id_field = 'uuid'
        excludes = ['plan', 'work']

    def obj_create(self, bundle, **kwargs):
        bundle = super(
            EmbeddedDetalizationResource, self).obj_create(bundle, **kwargs)
        bundle.obj.plan = wutils.create_detalization_plan(
            bundle.obj, self.instance, user=bundle.request.user)
        bundle.obj.work = wutils.create_detalization_work(
            bundle.obj, self.instance, bundle.obj.plan,
            user=bundle.request.user)
        print 'CREATE', bundle.obj.uuid
        return bundle

    def on_duration_change(self, bundle, **kw):
        if isinstance(bundle.obj.work, models.Work):
            work = bundle.obj.work
            td = work.to_date - work.from_date
            if td.days != bundle.obj.duration:
                work_utils.change_duration(
                    work, bundle.obj.duration,
                    user=bundle.request.user)
                work.save()
        return bundle

    def on_plan_title_change(self, bundle, **kw):
        if isinstance(bundle.obj.plan, models.Plan):
            plan = bundle.obj.plan
            if plan.title != bundle.obj.plan_title:
                plan.title = bundle.obj.plan_title
                plan.save()
            if isinstance(bundle.obj.work, models.Work):
                work = bundle.obj.work
                work_utils.change_title(
                    work, bundle.obj.plan_title, det=True,
                    user=bundle.request.user)
                work.save()
        return bundle

    def on_resp_change(self, bundle, **kw):
        if isinstance(bundle.obj.work, models.Work):
            work = bundle.obj.work
            if work.resp.userID != bundle.obj.resp.userID:
                work_utils.change_responsible(
                    work, bundle.obj.resp,
                    user=bundle.request.user)
                work.save()
        if isinstance(bundle.obj.plan, models.Plan):
            plan = bundle.obj.plan
            plan.detailer = bundle.obj.resp
            plan.save()
        return bundle

    def on_parent_nodes_change(self, bundle, **kw):
        from bson import DBRef, ObjectId
        parent_folders = []
        for _, folders in bundle.obj.parent_nodes.iteritems():
            for folder in folders:
                parent_folders.append(DBRef(
                    models.Folder._get_collection_name(),
                    ObjectId(folder)))
        if isinstance(bundle.obj.work, models.Work):
            work = bundle.obj.work
            work.parent_folders = parent_folders
            work.save()
        if isinstance(bundle.obj.plan, models.Plan):
            plan = bundle.obj.plan
            plan.parent_folders = parent_folders
            plan.save()
        return bundle

    def obj_update(self, bundle, **kwargs):
        bundle = super(
            EmbeddedDetalizationResource, self).obj_update(bundle, **kwargs)
        changes = bundle.obj.get_dirty_fields()
        for field, value in changes.iteritems():
            func = getattr(self, 'on_{}_change'.format(field), None)
            if callable(func):
                bundle = func(bundle, old_value=value, changes=changes)
            else:
                log.debug('No change handler for {}.{}'.format(
                    bundle.obj.__class__.__name__,
                    field))

        log.debug('UPDATE %s', bundle.obj.uuid)
        return bundle

    def obj_delete(self, bundle, **kwargs):
        super(EmbeddedDetalizationResource, self).obj_delete(bundle, **kwargs)
        if isinstance(bundle.obj.plan, models.Plan):
            plan = bundle.obj.plan
            try:
                plan.parent_folders = plan.parent_plan.parent_folders
            except:
                userid = plan.owner and plan.owner.extra.get(
                    'userid', None) or None
                if userid:
                    plan.parent_folders = [models.Folder.objects(
                        manager__userID=userid).first()]
            plan.parent_event = None
            plan.parent_plan = None
            plan.detailer = None
            plan.save()
        if isinstance(bundle.obj.work, models.Work):
            work_utils.delete_work(bundle.obj.work, bundle.request.user)
            bundle.obj.work.delete()
            bundle.obj.work = None
        print 'DELETE', bundle.obj.uuid
        return bundle


class Query(object):
    pass



class EventResource(PatchMixin, BaseResource):
    plan = fields.ReferenceField(
        to='home.api.PlanResource',
        attribute='plan',
        full=False,
        null=True)
    department = fields.EmbeddedDocumentField(
        embedded='home.api.EmbeddedDepartmentResource',
        attribute='department',
        null=True)
    plan_obj = fields.ReferenceField(
        to='home.api.PlanResource',
        attribute='plan',
        readonly=True,
        full=True,
        null=True)
    assignees = fields.EmbeddedListField(
        of='home.api.EmbeddedAssignmentResource',
        attribute='assignees', 
        #readonly=True, 
        full=True, null=True)
    detailers = fields.EmbeddedListField(
        of='home.api.EmbeddedDetalizationResource',
        attribute='detailers', readonly=True, full=True, null=True)
    sub_events = fields.EmbeddedListField(
        of='home.api.EmbeddedSubEventsResource', attribute='sub_events', full=True, null=True)
    polymorphic={
        'event':'self',
    }
        


    # sub_events = fields.EmbeddedListField(
    #     of='home.api.EmbeddedSubEventsResource',
    #     attribute='sub_events',
    #     readonly=True,
    #     full=True,
    #     null=False)

    class Meta:

        queryset = models.Event.objects()
        unallowed_update_fields = [
            'path', 'child_nodes', 'seq',
            'detailers'
        ]
        excludes = ['path', 'child_nodes', 'seq', 'creator']
        allowed_methods = ('get', 'post', 'put', 'patch', 'delete')
        resource_name = 'event'
        authentication = auth_class()
        authorization = EventAuthorization()
        #objects_class=models.Event
        queryfields = [
            'event_title',
            'resp_name',
            'resp_position',
            'from_date',
            'to_date',
            'is_finished',
            'is_not_finished',
        ]
 

    def apply_filters(self, request, applicable_filters):
        print request
        query = Query()
        print query
        for field in self._meta.queryfields:
            setattr(query, field, None)
        if request.method == "GET" and 'q' in request.GET:
            query_data = self._meta.serializer.deserialize(
                request.GET.get('q'), format='application/json')
            for key in query_data:
                setattr(query, key, query_data[key])
            if isinstance(query.from_date, basestring):
                query.from_date = parse(query.from_date.replace('"', ''))
            if isinstance(query.to_date, basestring):
                query.to_date = parse(query.to_date.replace('"', ''))
        qs = self.get_object_list(request).filter(**applicable_filters)
        if query.from_date:
            qs = qs.filter(to_date__gte=query.from_date)
        if query.to_date:
            qs = qs.filter(from_date__lte=query.to_date)
        if query.event_title:
            qs = qs.filter(title__contains=query.event_title)
        if query.is_finished:
            qs = qs.filter(progress=100)
        elif query.is_not_finished:
            qs = qs.filter(progress__ne=100)
        if query.resp_name:
            qs = qs.filter(
                Q(assignees__resp__name__icontains=query.resp_name) |
                Q(assignees__coworkers__name__icontains=query.resp_name) |
                Q(detailers__resp__name__icontains=query.resp_name))
        if query.resp_position:
            qs = qs.filter(
                Q(assignees__resp__position__icontains=query.resp_position) |
                Q(assignees__coworkers__position__icontains=query.resp_position) |
                Q(detailers__resp__position__icontains=query.resp_position))
        return qs

    def obj_delete(self, bundle, **kw):
        super(EventResource, self).obj_delete(bundle, **kw)
        for assign in bundle.obj.assignees:
            resource = self.fields['assignees'].get_related_resource(assign)
            resource.instance = bundle.obj
            bdl = resource.build_bundle(obj=assign, request=bundle.request)
            resource.obj_delete(bdl)
        for det in bundle.obj.detailers:
            resource = self.fields['detailers'].get_related_resource(det)
            resource.instance = bundle.obj
            bdl = resource.build_bundle(obj=det, request=bundle.request)
            resource.obj_delete(bdl)

    def full_hydrate(self, bundle, **kw):
        
        bundle = super(EventResource, self).full_hydrate(bundle, **kw)

        if bundle.request.method == 'PATCH':
            bundle.changes = bundle.obj.get_dirty_fields()
        return bundle

    def obj_update(self, bundle, **kw):
        #print "---------------------------------obj update------------------------------------------"
        bundle = super(EventResource, self).obj_update(bundle, **kw)
        if hasattr(bundle, 'changes'):
            wutils.update_event(bundle.obj.id, bundle.changes)
        print 'UPDATE EVENT', bundle.obj.id
        return bundle

    def obj_create(self, bundle, **kwargs):
        #print "---------------------------------obj create------------------------------------------"
        bundle.data['creator'] = bundle.request.user
        return super(EventResource, self).obj_create(
            bundle, creator=bundle.request.user)

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/(?P<pk>\w[\w/-]*)/info%s$" % (
                    self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_info'),
                name="api_get_event_info"),
            url(
                #r"^(?P<resource_name>%s)/folders%s$" % (
                r"^(?P<resource_name>%s)/(?P<pk>\w[\w/-]*)/sub_events%s$" % (
                self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_sub_events'),
                name="api_get_sub_events"),
            url(
                #r"^(?P<resource_name>%s)/folders%s$" % (
                r"^(?P<resource_name>%s)/(?P<pk>\w[\w/-]*)/file_upload%s$" % (
                self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_uploaded'),
                name="api_file_upload"),
            url(
                r"^(?P<resource_name>%s)/(?P<pk>\w[\w/-]*)/file_delete%s$" % (
                self._meta.resource_name, trailing_slash()),
                self.wrap_view('file_delete'),
                name="api_file_delete"),

        ]

    '''
    Returning events by the responsible's name
    '''

    def get_sub_events(self, request, **kwargs):
        print "get_sub_events"
        #sub_events=get_sub_events(kwargs['pk'])
        sub_events=models.Event.objects.get(id=kwargs['pk']).sub_events
        print sub_events
        objects=[]
        #result_set=list(sub_events)
        for sub_event in sub_events:
            bundle = self.build_bundle(obj=sub_event, request=request)
            #bundle = self.full_dehydrate(bundle)
            #objects.append(bundle)   
        self.log_throttled_access(request)
        #print self.create_response(request, {'objects':objects})
        return self.create_response(request, {'objects':sub_events})


    def _get_cached_work(self, cache, dbref):
        if dbref:
            return cache.get(str(dbref.id))

    def _flatten(self, data):
        res = []

        def dive(arr, depth):
            for obj in arr:
                new_obj = {key: value for key, value in obj.iteritems() if key != 'subProcesses'}
                new_obj['tab'] = depth
                res.append(new_obj)
                if len(obj['subProcesses']):
                    try:
                        new_obj['assigneeName'] = obj[
                            'subProcesses'][0]['authorName']
                    except:
                        pass
                    dive(obj['subProcesses'], depth + 1)
        dive(data, 0)
        return res

    def _wec(self, auth_key, work_data, work):
        """Get execution process for work_data"""
        try:
            # print work_data
            work_data['exec_process'] = self._flatten(synergy.get(
                'execution_process',
                workID=work_data.get('actionID'),
                documentID=work.doc_id,
                username="$key",
                password=auth_key).json())
        except Exception, e:
            log.error(e)
        return work_data

    def get_info(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        try:
            bundle = self.build_bundle(
                data={'pk': kwargs['pk']}, request=request)
            obj = self.cached_obj_get(
                bundle=bundle, **self.remove_api_resource_names(kwargs))
        except DoesNotExist:
            return http.HttpGone()
        except MultipleObjectsReturned:
            return http.HttpMultipleChoices(
                "More than one resource is found at this URI.")

        wqs = models.Work.objects(
            parent_event=obj)
        work_map = dict((str(w.id), w) for w in wqs)
        work_map2 = dict((w.work_id, w) for w in wqs if w.work_id)
        work_ids = [work.work_id for work in wqs]
        works = synergy.get(
            'works',
            workID=work_ids,
            username="$key",
            password=request.user.auth_key).json()
        work_data_map = dict(
            (wd['actionID'], self._wec(
                request.user.auth_key, wd,
                work_map2[wd['actionID']])) for wd in works
            if wd.get('actionID', 'null') != 'null')



        detailers = []
        for det in obj.detailers:
            work = self._get_cached_work(work_map, det._data['work'])
            work_data = work and work_data_map.get(work.work_id) or None
            plan_ref = det._data.get('plan')
            tmp = {
                "resp": det.resp._data,
                "plan_id": plan_ref and str(plan_ref.id) or None,
                "work": {
                    "work_id": work and work.work_id or None,
                    "doc_id": work and work.doc_id or None,
                    "progress": work_data and work_data.get('percent', 0) or 0,
                },
                "progress": det.plan and det.plan.get_progress() or 0
            }
            detailers.append(tmp)

        assignees = []
        for ass in obj.assignees:
            progress = 0
            works = []
            for work_ref in ass._data['works']:
                work = self._get_cached_work(work_map, work_ref)
                if not work:
                    continue
                work_data = work_data_map.get(work.work_id)
                res = work_data and work_data.get('percent', 0) or 0
                execution = work_data and work_data.get('exec_process') or []
                works.append({
                    'title': work.title,
                    'work_id': work.work_id,
                    'doc_id': work.doc_id,
                    'from_date': work.from_date,
                    'to_date': work.to_date,
                    'progress': res,
                    'execution': execution,
                })
                progress += int(res)
            if ass.count > 0:
                progress = progress / float(ass.count)
            tmp = {
                "resp": ass.resp._data,
                "period": ass.period,
                "dates": ass.dates,
                "from_date": ass.from_date,
                "to_date": ass.to_date,
                "count": ass.count,
                "works": works,
                "progress": progress,
            }
            assignees.append(tmp)

        result = {
            'assignees': assignees,
            'detailers': detailers,
        }

        self.log_throttled_access(request)
        return self.create_response(request, result)
    
    
    def get_uploaded(self, request, **kwargs):

        file = request.FILES['file']
        index = request.POST['index']
        eventId=request.POST['eventID']
        if file :
            file_path=MEDIA_ROOT+'/'+eventId+'/'
            if not os.path.exists(file_path):
                os.makedirs(file_path)
            file_path+=file.name
            destination = open(file_path, 'wb+')
            for chunk in file.chunks():
                destination.write(chunk)
            destination.close()

            evt = Event.objects.get(pk=kwargs['pk'])    
            sub_evt = evt.sub_events[int(index)]
            syn_att=SynergyAttachment(file_name=str(file.name.encode('utf-8')), url=MEDIA_URL+eventId.encode('utf-8')+'/' +str(file.name.encode('utf-8')), path=MEDIA_ROOT + '/' +eventId.encode('utf-8')+'/'+ str(file.name.encode('utf-8')))
            if syn_att in sub_evt.attachments:
                return http.HttpResponse("File already attached")
            else:
                sub_evt.attachments.append(syn_att)
            evt.save()
            return http.HttpResponse("File Successfuly uploaded")


    def file_delete(self, request, **kwargs):
        file_name=request.POST['file_name']
        index = request.POST['index']
        eventID=request.POST['eventID']

        file_path=MEDIA_ROOT+'/'+eventID+'/'
        os.chdir(file_path)
        try:
            os.remove(file_name)
        except:
            return http.HttpResponse("File wasn't found")
        #return http.HttpResponse("File was deleted")
        evt = Event.objects.get(pk=kwargs['pk'])
        sub_evt = evt.sub_events[int(index)]
        for attachment in sub_evt.attachments:
            if attachment.file_name==file_name:
                sub_evt.attachments.remove(attachment)
                evt.save()
                return http.HttpResponse(1)
        return http.HttpResponse(0)

class GroupResource(PatchMixin, BaseResource):

    class Meta:
        queryset = models.Group.objects()
        unallowed_update_fields = ['path', 'child_nodes', 'seq']
        allowed_methods = ('get', 'post', 'put', 'patch', 'delete')
        excludes = ['path', 'child_nodes', 'seq']
        resource_name = 'group'
        authentication = auth_class()
        authorization = auz_class()




class NodeResource(PatchMixin, BaseResource):
    '''
    plan = fields.ReferenceField(
        to='home.api.PlanResource',
        attribute='plan',
        full=False,
        null=True)
    '''

    class Meta:
        queryset = models.Node.objects()
        unallowed_update_fields = ['path', 'child_nodes', 'seq']
        allowed_methods = ('get', 'post', 'put', 'patch', 'delete')
        resource_name = 'node'
        excludes = ['path', 'child_nodes', 'seq']
        authentication = auth_class()
        authorization = auz_class()

        prefer_polymorphic_resource_uri = True
        polymorphic = {
            'node': 'self',
            'work': WorkResource,
            # 'filter': FilterResource,
            'group': GroupResource,
            'folder': FolderResource,
            'plan': PlanResource,
            'event': EventResource,
        }

    def dehydrate(self, bundle):
        obj = bundle.obj
        if obj and hasattr(obj, 'parent_plan') and isinstance(
                obj.parent_plan, models.Plan):
            bundle.data['parent_plan_sc'] = {
                'title': obj.parent_plan.title,
                'id': str(obj.parent_plan.id),
            }
        bundle.data['path_sc'] = map(int, obj.path.split(',')[1:-1])
        return bundle

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/(?P<pk>\w[\w/-]*)/tree%s$" % (
                    self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_tree'),
                name="api_get_plan_tree"),
            url(
                r"^(?P<resource_name>%s)/(?P<pk>\w[\w/-]*)/move%s$" % (
                    self._meta.resource_name, trailing_slash()),
                self.wrap_view('change_order'),
                name="api_change_order"),
            url(
                r"^(?P<resource_name>%s)/folders%s$" % (
                    self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_user_folders'),
                name="api_get_user_folders"),
            url(
                r"^(?P<resource_name>%s)/resp_events%s$" % (
                self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_user_events'),
                name="api_get_user_events"),
        ]

    def _recursive_bundle(self, obj, request, mp):
        bundle = self.build_bundle(obj=obj, request=request)
        bundle = self.full_dehydrate(bundle)
        items = []
        for x in obj.child_nodes:
            node = mp.get(x)
            if not node:
                continue
            if isinstance(node, models.Plan) and node.parent_event:
                continue
            items.append(self._recursive_bundle(node, request, mp))
        bundle.data['items'] = items
        if 'progress' not in bundle.data:
            progress = 0
            if bundle.data['items']:
                psum = sum(
                    [bdl.data['progress'] for bdl in bundle.data['items']])
                progress = psum / len(bundle.data['items'])
            bundle.data['progress'] = progress
        del bundle.data['parent_node']
        return bundle

    def build_tree(self, obj, request):
        bundle = self.build_bundle(request=request)
        if request.user.extra.get('userid')=='1':

            result_list = self.obj_get_list(bundle, path__contains=',{},'.format(obj.seq)).order_by('path')
        else:
           # print "different user ---------------------------------------------------------"
            print request.user.extra.get('userid')
            result_list = list(models.Group.objects.filter(
                path__contains=',{},'.format(obj.seq)).order_by('path'))
            result_list.extend(list(models.Event.objects.filter(
                path__contains=',{},'.format(obj.seq), assignees__resp__userID=request.user.extra.get('userid', None)).order_by('path')))


        # models.Node.objects.filter(
        #     path__contains=',{},'.format(obj.seq)).order_by('path')
        mp = {x.seq: x for x in result_list}
        objects = []
        objects.append(self._recursive_bundle(obj, request, mp))

        return {
            'objects': objects,
        }

    def get_tree(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        try:
            bundle = self.build_bundle(
                data={'pk': kwargs['pk']}, request=request)
            obj = self.cached_obj_get(
                bundle=bundle, **self.remove_api_resource_names(kwargs))
        except DoesNotExist:
            return http.HttpGone()
        except MultipleObjectsReturned:
            return http.HttpMultipleChoices(
                "More than one resource is found at this URI.")

        tree = self.build_tree(obj, request)

        self.log_throttled_access(request)
        return self.create_response(request, tree)

    def change_order(self, request, **kwargs):
        self.method_check(request, allowed=['patch'])
        self.is_authenticated(request)
        self.throttle_check(request)
        try:
            bundle = self.build_bundle(
                data={'pk': kwargs['pk']}, request=request)
            obj = self.cached_obj_get(
                bundle=bundle, **self.remove_api_resource_names(kwargs))
        except DoesNotExist:
            return http.HttpGone()
        except MultipleObjectsReturned:
            return http.HttpMultipleChoices(
                "More than one resource is found at this URI.")

        deserialized = self.deserialize(
            request, request.body,
            format=request.META.get('CONTENT_TYPE', 'application/json'))
        if isinstance(obj, (models.Plan, models.Work)):
            if not ('parent_id' in deserialized or 'order' in deserialized):
                raise BadRequest("parent_id and order must be provided")
            parent_id = deserialized['parent_id']
            order = float(deserialized['order'])
            obj.order[parent_id] = order
            obj.save()
        else:
            if 'delta' not in deserialized:
                raise BadRequest("Delta must be provided")
            try:
                delta = int(deserialized['delta'])
            except ValueError:
                raise BadRequest("Delta must be an Integer")

            obj.move(delta)
        return http.HttpAccepted()

        # try:
        #     root_seq = int(obj.path.split(',')[1])
        # except (IndexError, ValueError):
        #     raise BadRequest(
        #         "Something wrong with node path: {}".format(obj.path))

        # try:
        #     plan = models.Node.objects.get(seq=root_seq)
        # except DoesNotExist:
        #     return http.HttpGone()
        # except MultipleObjectsReturned:
        #     return http.HttpMultipleChoices(
        #         "More than one resource is found at this URI.")
        # tree = self.build_tree(plan, request)

        # self.log_throttled_access(request)
        # return self.create_response(request, tree)

    def cmp_folders(self, a, b):
        if a.id < b.id:
            return -1
        if a.id > b.id:
            return 1
        return 0

    def get_user_folders(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        folders = get_user_folders(request.user)
        # userids = set([userid])
        result_set = folders | set(models.Plan.objects.filter(
            parent_folders__in=folders))
        result_set |= set(models.Work.objects.filter(
            parent_folders__in=folders, work_id__ne=None))
        objects = []
        result_set = list(result_set)

        result_set.sort(self.cmp_folders)
        for node in result_set:
            bundle = self.build_bundle(obj=node, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        self.log_throttled_access(request)
        #print self.create_response(request, {'objects': objects})
        return self.create_response(request, {'objects': objects})

    def get_user_events(self, request, **kwargs):
        print "We are at node.get_user_events and printing kwargs:"
        print kwargs
        if request.user:
            #events=get_user_events(self, **kwargs)
            events=Event.objects.filter(assignees__resp__userID=request.user.extra.get('userid', None))
            objects = []
            result_set=set()
            for event in events:
                result_set.add(event)
            
            result_set=list(result_set)
            print result_set
            for node in result_set:
                bundle=self.build_bundle(obj=node, request=request)
                bundle=self.full_dehydrate(bundle)
                objects.append(bundle)
            return self.create_response(request, {'objects':objects})
        else:
            return self.create_response(request, {'objects':"none"})


class AssignmentOverlay(resources.MongoEngineResource):
    uuid = CharField()
    resp = fields.EmbeddedDocumentField(
        embedded='home.api.EmbeddedExtUserResource',
        attribute='resp',
        null=True)
    coworkers = fields.EmbeddedListField(
        of='home.api.EmbeddedExtUserResource',
        attribute='coworkers',
        full=True,
        null=True)

    class Meta:
        resource_name = 'assignment'
        object_class = models.Assignment
        allowed_methods = ('post',)
        authentication = auth_class()
        authorization = auz_class()

    def prepend_urls(self):
        return [
            url(
                r"^(?P<resource_name>%s)/overlays%s$" % (
                    self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_assignment_overlays'),
                name="api_get_assignment_overlays"),
        ]

    def get_assignment_overlays(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data = json.loads(request.body)
        bundle = self.build_bundle(data=data['assign'], request=request)
        bundle = self.full_hydrate(bundle)

        assign = bundle.obj
        if 'uuid' in data['assign'] and data['event_id']:
            event = models.Event.objects.with_id(data['event_id'])
            if event:
                for ass in event.assignees:
                    if ass.uuid == assign.uuid:
                        assign.works = ass.works
                        break

        users = [user for user in assign.coworkers]
        if assign.resp:
            users.insert(0, assign.resp)
        overlays = {}
        for user in users:
            user_overlays = wutils.get_user_overlays(user, assign)
            if user_overlays:
                overlays[user.name] = user_overlays
        self.log_throttled_access(request)
        return self.create_response(request, {'overlays': overlays})


