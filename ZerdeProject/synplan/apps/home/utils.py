import datetime
from django.utils import timezone
MON, TUE, WED, THU, FRI, SUN, SAT = range(7)


def rget(d, *seq):
    res = d
    for node in seq:
        res = res and res.get(node, None) or None
        if not res:
            break
    return res


def monday(date):
    return date - datetime.timedelta(days=date.weekday())


def next_weekday(date, weekday):
    cur_weekday = date.weekday()
    if cur_weekday < weekday:
        date = date.replace(day=date.day + (weekday - cur_weekday))
    elif cur_weekday > weekday:
        date = date.replace(day=date.day + (7 - cur_weekday) + weekday)
    return date


def last_weekday(date, weekday):
    cur_weekday = date.weekday()
    if cur_weekday > weekday:
        date = date.replace(day=date.day - (cur_weekday - weekday))
    elif cur_weekday < weekday:
        date = date.replace(day=date.day + (weekday - 7) - cur_weekday)
    return date


def last_monthday(date):
    if date.month == 12:
        return date.replace(day=31)
    return date.replace(
        month=date.month + 1, day=1) - datetime.timedelta(days=1)


def get_quarter(quarter, year):
    a = datetime.datetime(year, 1 + quarter * 3, 1)
    b = last_monthday(datetime.datetime(year, (quarter + 1) * 3, 1))
    return a, b


def in_range(date, a, b):
    return a.date() <= date.date() <= b.date()


def le(date, b):
    return date.date() <= b.date()


def periodic_dates(period, dates, from_date=None, to_date=None):
    from_date = from_date or timezone.now()
    to_date = to_date or from_date.replace(year=from_date.year + 1)
    dates = [x for x in dates]
    dates.sort()
    if period == 'weekly':
        cur = monday(from_date)
        while le(cur, to_date):
            for weekday in dates:
                date = cur + datetime.timedelta(days=weekday-1)
                if in_range(date, from_date, to_date):
                    yield date
            cur = cur + datetime.timedelta(days=7)
    elif period == 'monthly':
        cur = from_date
        while le(cur, to_date):
            for monthday in dates:
                try:
                    date = cur.replace(day=monthday)
                    if in_range(date, from_date, to_date):
                        yield date
                except ValueError:
                    pass
            cur = (cur + datetime.timedelta(days=32)).replace(day=1)
    elif period == 'monthly_days':
        cur = from_date.replace(day=1)
        last_cur = last_monthday(cur)
        while le(cur, to_date):
            for num, weekday in dates:
                try:
                    if num < 6:
                        date = next_weekday(cur, weekday - 1)
                        date = date.replace(day=date.day + 7 * (num - 1))
                    else:
                        date = last_weekday(last_cur, weekday - 1)
                    if in_range(date, from_date, to_date):
                        yield date
                except ValueError:
                    pass
            cur = (cur + datetime.timedelta(days=32)).replace(day=1)
    elif period in ('yearly', 'quarterly'):
        cur = from_date
        while le(cur, to_date):
            for month, day in dates:
                try:
                    date = cur.replace(month=month, day=day)
                    if in_range(date, from_date, to_date):
                        yield date
                except ValueError:
                    pass
            cur = cur.replace(year=cur.year + 1, month=1, day=1)
