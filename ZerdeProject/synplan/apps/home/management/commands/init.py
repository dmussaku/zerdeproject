import os.path
import uuid

# from django.core.management import call_command
from django.core.management.base import BaseCommand
# from django.conf import settings


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        default_path = os.path.expanduser('~/.synplan')

        if not os.path.exists(default_path):
            os.mkdir(default_path)

        if not os.path.exists(os.path.join(default_path, 'static')):
            os.mkdir(os.path.join(default_path, 'static'))

        if not os.path.exists(os.path.join(default_path, 'media')):
            os.mkdir(os.path.join(default_path, 'media'))

        config_path = os.path.join(default_path, 'synplan.conf.py')
        if not os.path.exists(config_path):
            default_params = {
                'SECRET_KEY': uuid.uuid4()
            }

            with open(config_path, 'w') as fh:
                fh.write("""SECRET_KEY = '%(SECRET_KEY)s'
""" % default_params)
