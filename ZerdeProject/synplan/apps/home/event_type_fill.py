#event fill example

from synplan.apps.home.models import EventType, SubEvent

event_type1=EventType('event type 1')
event_type2=EventType('event type 2')


sub_event_a=SubEvent(name='sub event a', finishing_form="finishing form 1").save()
sub_event_b=SubEvent(name='sub event b', finishing_form="finishing form 2").save()
sub_event_c=SubEvent(name='sub event c',finishing_form="finishing form 3").save()
sub_event_d=SubEvent(name='sub event d', finishing_form="finishing form 4").save()
sub_event_e=SubEvent(name='sub event e', finishing_form="finishing form 5").save()
sub_event_f=SubEvent(name='sub event f', finishing_form="finishing form 6").save()
sub_event_g=SubEvent(name='sub event g', finishing_form="finishing form 7").save()
sub_event_h=SubEvent(name='sub event h', finishing_form="finishing form 8").save()
sub_event_i=SubEvent(name='sub event i', finishing_form="finishing form 9").save()
sub_event_j=SubEvent(name='sub event j', finishing_form="finishing form 10").save()
event_type1.sub_events.append(sub_event_a)
event_type1.sub_events.append(sub_event_b)
event_type1.sub_events.append(sub_event_c)
event_type2.sub_events.append(sub_event_d)
event_type2.sub_events.append(sub_event_e)
event_type2.sub_events.append(sub_event_f)
event_type2.sub_events.append(sub_event_g)
event_type2.sub_events.append(sub_event_h)
event_type2.sub_events.append(sub_event_i)

event_type1.save()
event_type2.save()


event_type1.name="Тип Мероприятия #1"
event_type2.name="Тип Мероприятия #2"
event_type3.name="Тип Мероприятия #3"
event_type4.name="Тип Мероприятия #4"
event_type5.name="Тип Мероприятия #5"
event_type1.save()
event_type2.save()
event_type3.save()
event_type4.save()
event_type5.save()