from django.conf import settings
from requests.auth import HTTPBasicAuth
from django import http
from django.conf.urls import patterns, url
import functools
import requests
from django.utils import timezone
from django.contrib.auth.decorators import login_required
import logging
log = logging.getLogger()


class AuthError(Exception):
    pass


class RequestError(Exception):
    pass


class SynergyBackend(object):

    def __init__(self, host, resources):
        self.host = host
        self.resources = resources

    AuthError = AuthError
    RequestError = RequestError

    def url(self, key, all_params):
        res = self.host + self.resources[key]['url']
        param_keys = self.resources[key].get('urlparams', None)
        params = {}
        if param_keys:
            for key in param_keys:
                params[key] = all_params.pop(key)
        res = res.format(**params)
        return res

    def get(self, resource, username=None, password=None, **kwargs):
        r = requests.get(
            self.url(resource, kwargs),
            auth=HTTPBasicAuth(username, password), params=kwargs)
        if r.status_code == 401:
            raise AuthError()
        assert r.status_code == 200, "Status code {}".format(r.status_code)
        return r

    def handle_res(self, res):
        if isinstance(res, dict):
            error_code = int(res.get('errorCode', 0))
            if error_code != 0:
                log.error(res)
                raise self.RequestError(res.get('errorMessage'))

    def get_json(self, *a, **kw):
        res = self.get(*a, **kw).json()
        self.handle_res(res)
        return res

    def post_json(self, *a, **kw):
        res = self.post(*a, **kw).json()
        self.handle_res(res)
        return res

    def post(
        self, resource, username=None,
            password=None, files=None, **kwargs):
        # import httplib
        # httplib.HTTPConnection.debuglevel = 1
        # requests_log = logging.getLogger("requests.packages.urllib3")
        # requests_log.setLevel(logging.DEBUG)
        # requests_log.propagate = True
        r = requests.post(
            self.url(resource, kwargs),
            auth=HTTPBasicAuth(username, password), data=kwargs, files=files)
        # print r.request.body
        # log.debug('SYN RESPONSE %s', r.url)
        # log.debug(r.content)
        if r.status_code == 401:
            raise AuthError()
        assert r.status_code == 200, "Status code {}\n{}".format(
            r.status_code, r.text)
        # print r.json()
        return r

    def create_work(self, **kw):
        """
        name - *
        startDate - * yyyy-MM-dd HH:mm:ss
        finishDate - * yyyy-MM-dd HH:mm:ss
        userID - *uuid
        authorID - *uuid
        resUserID - uuid
        priority - *
        completionFormID
        """
        date_format = '%Y-%m-%d %H:%M:%S'
        if kw['startDate'] and not isinstance(kw['startDate'], basestring):
            kw['startDate'] = kw['startDate'].replace(
                second=0, microsecond=0).strftime(date_format)
            kw['finishDate'] = kw['finishDate'].replace(
                second=0, microsecond=0).strftime(date_format)
        res = self.post_json('create_work', **kw)
        return res

    def save_work(self, **kw):
        """
        name - *
        startDate - * yyyy-MM-dd HH:mm:ss
        finishDate - * yyyy-MM-dd HH:mm:ss
        priority - *
        completionFormID
        """
        date_format = '%Y-%m-%d %H:%M:%S'
        if 'startDate' in kw and not isinstance(kw['startDate'], basestring):
            kw['startDate'] = kw['startDate'].replace(
                second=0, microsecond=0).strftime(date_format)
        if 'finishDate' in kw and not isinstance(kw['finishDate'], basestring):
            kw['finishDate'] = kw['finishDate'].replace(
                second=0, microsecond=0).strftime(date_format)
        res = self.post_json('save_work', **kw)
        return res

    def get_person(self, **kw):
        return self.get_json('person', **kw)

    def get_auth_key(self, **kw):
        if not 'moduleID' in kw:
            kw['moduleID'] = settings.SYNERGY_MODULE_ID
        data = self.get_json(
            'auth_key', **kw)
        assert data['errorCode'] == '0'
        return data['key']

    def synergy_proxy(self, resource, request):
        if settings.SYNERGY_DEV:
            username, password = settings.SYNERGY_DEV.split(':')
        elif 'SYNERGY_HASH' in request.session:
            username = '$session'
            password = request.session.get('SYNERGY_HASH')
        else:
            username = '$key'
            password = request.user.auth_key
        if request.method == "GET":
            return http.HttpResponse(self.get(
                resource,
                username=username,
                password=password,
                **request.GET
            ), content_type="application/json")
        raise http.Http404

    @property
    def urls(self):
        rst = patterns('')
        for resource in self.resources.keys():
            rst += (url(
                r'^{}'.format(resource),
                login_required(functools.partial(self.synergy_proxy, resource)),
                name=resource),)
        return rst


synergy = SynergyBackend(
    settings.SYNERGY_HOST, settings.SYNERGY_RESOURCES)


def test_create_work(**kw):
    import datetime
    from home.models import SynergyUser
    from_date = timezone.now().replace(second=0, microsecond=0)
    to_date = from_date + datetime.timedelta(days=30)
    user = SynergyUser.objects.all()[0]
    params = {
        'username': '$key', 'password': user.auth_key,
        'startDate': from_date,
        'finishDate': to_date}
    params['userID'] = user.extra['userid']
    params['authorID'] = user.extra['userid']
    params['priority'] = 1
    params['name'] = 'Test work'
    params.update(kw)
    return synergy.create_work(**params)
