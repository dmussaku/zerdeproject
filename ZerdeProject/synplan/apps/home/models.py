# -*- coding: utf-8 -*-

import mongoengine as me
# from bson import DBRef
from home.auth_backend import SynergyUser
from home.model_utils import DirtyFieldsMixin
# from home.synergy import synergy
# import datetime
from django.utils import timezone
import uuid
from home.utils import rget
from synplan.settings import SUB_EVENTS
from synplan.settings import MEDIA_ROOT, MEDIA_URL

class ExtUser(me.EmbeddedDocument):
    name = me.StringField()
    userID = me.StringField()
    position = me.StringField()

    @property
    def pk(self):
        return self.userID


class Node(DirtyFieldsMixin, me.Document):
    seq = me.SequenceField()
    title = me.StringField()
    node_type = me.StringField()
    path = me.StringField()
    parent_node = me.ReferenceField('self', reverse_delete_rule=me.CASCADE)
    child_nodes = me.ListField()
    ts = me.DateTimeField(default=timezone.now)

    meta = {
        'allow_inheritance': True,
        'cascade': True,
        'watch_fields': ['parent_node'],
    }

    @property
    def parent_node_id(self, value=None):
        if value is None:
            ref = self._data['parent_node']
            return ref and ref.id or None

    def __unicode__(self):
        return self.title

    def move(self, d):
        if not isinstance(self.parent_node, Node):
            return
        parent = self.parent_node
        n = len(parent.child_nodes)
        try:
            index = parent.child_nodes.index(self.seq)
        except ValueError:
            return
        new_index = min(max(index + d, 0), n - 1)
        if new_index != index:
            del parent.child_nodes[index]
            parent.child_nodes.insert(new_index, self.seq)
            parent.update(set__child_nodes=parent.child_nodes)

    def get_progress(self):
        if hasattr(self, 'progress'):
            return self.progress
        children = list(Node.objects.filter(seq__in=self.child_nodes))
        if children:
            psum = sum([child.get_progress() for child in children])
            return psum/float(len(children))
        return 0

    def save(self, force_insert=False, cascade=True, **kw):
        self.node_type = self._class_name.split('.')[-1].lower()
        new = ('id' not in self._data or self._created or force_insert)
        if self.parent_node:
            self.path = (self.parent_node.path or ',') + str(self.seq) + ','
        else:
            self.path = ',' + str(self.seq) + ','
        if self.parent_node:
            self.parent_node.update(add_to_set__child_nodes=self.seq)
        if not new and self.is_changed('parent_node'):
            orig_parent_node = self.get_original('parent_node')
            node_id = None
            # dbref
            if orig_parent_node:
                node_id = orig_parent_node.id
            if node_id is not None and self._data['parent_node'].id != node_id:
                Node.objects.filter(id=node_id).update(
                    pull__child_nodes=self.seq)
        super(Node, self).save(force_insert=force_insert, cascade=cascade, **kw)

    def delete(self, *a, **kw):
        parent = self.parent_node
        seq = self.seq
        super(Node, self).delete(*a, **kw)
        if parent and seq in parent.child_nodes:
            parent.update(pull__child_nodes=seq)


class Folder(Node):
    creator = me.ReferenceField(SynergyUser, required=True)
    manager = me.EmbeddedDocumentField(ExtUser)

    def delete(self, *a, **kw):
        for content in Content.objects.filter(parent_folders=self):
            idx = None
            for ix, dbref in enumerate(content._data['parent_folders']):
                if dbref.id == self.id:
                    idx = ix
                    break
            if idx is not None:
                content._data['parent_folders'].pop(idx)
            if not content._data['parent_folders']:
                print 'delete content'
                content.delete()
        super(Folder, self).delete(*a, **kw)


class Content(Node):
    parent_folders = me.ListField(
        me.ReferenceField(Node, reverse_delete_rule=me.NULLIFY))
    order = me.DictField()

    @property
    def parent_folder_ids(self, value=None):
        if value is None:
            return [ref.id for ref in self._data['parent_folders']]


def str_uuid():
    return unicode(uuid.uuid4())


class Assignment(DirtyFieldsMixin, me.EmbeddedDocument):

    def __init__(self, *a, **kw):
        super(Assignment, self).__init__(*a, **kw)
        if not self.uuid:
            self.uuid = str_uuid()

    uuid = me.StringField(required=True)
    resp = me.EmbeddedDocumentField(ExtUser)
    coworkers = me.ListField(me.EmbeddedDocumentField(ExtUser))
    period = me.StringField()
    dates = me.ListField()
    completion_form = me.StringField()
    duration = me.IntField()
    from_date = me.DateTimeField()
    to_date = me.DateTimeField()
    count = me.IntField(default=0)
    actual_dates = me.ListField(me.DateTimeField())
    works = me.ListField(me.ReferenceField('Work'))
    parent_nodes = me.DictField()

    meta = {
        'watch_fields': [
            'resp', 'coworkers', 'period', 'dates', 'completion_form',
            'duration', 'from_date', 'to_date', 'parent_nodes']
    }
    
    @property
    def pk(self, value=None):
        if value:
            self.uuid = value
        else:
            return self.uuid
    

class Detalization(DirtyFieldsMixin, me.EmbeddedDocument):

    def __init__(self, *a, **kw):
        super(Detalization, self).__init__(*a, **kw)
        if not self.uuid:
            self.uuid = str_uuid()

    uuid = me.StringField(required=True)
    plan = me.ReferenceField('Plan')
    plan_title = me.StringField()
    resp = me.EmbeddedDocumentField(ExtUser)
    duration = me.IntField()
    work = me.ReferenceField('Work')
    parent_nodes = me.DictField()

    meta = {
        'watch_fields': ['plan_title', 'resp', 'duration', 'parent_nodes']
    }

    @property
    def pk(self, value=None):
        if value:
            self.uuid = value
        else:
            return self.uuid


class Group(Content):
    pass


class Department(me.EmbeddedDocument):
    departmentID = me.StringField()
    nameRu = me.StringField()
    nameKz = me.StringField()
    nameEn = me.StringField()
    parentDepartmentID = me.StringField()
    pointersCode = me.StringField()
    manager = me.DictField()

class SynergyAttachment(me.EmbeddedDocument):
    file_name = me.StringField()
    mime_type = me.StringField()
    path = me.StringField()
    url = me.StringField()
    size = me.IntField()
    
    @property
    def pk(self):
        return self.file_name

class SubEvent(me.EmbeddedDocument):                #Changed type from EmbeddedDocument to Document
    name = me.StringField()
    from_date = me.DateTimeField()
    to_date = me.DateTimeField()
    finishing_form = me.StringField()
    sub_progress = me.IntField(default=0)
    attachments = me.ListField(me.EmbeddedDocumentField(SynergyAttachment))
    comment=me.StringField()

    @property
    def pk(self):
        return self.name
    #def __unicode__(self):
    #    return self.to_json()


class Event(Content):
    creator = me.ReferenceField(SynergyUser)
    #event_type = me.ListField(me.EmbeddedDocumentField(SubEvent))                         #new added_field
    sub_events = me.ListField(me.EmbeddedDocumentField(SubEvent))
    department = me.EmbeddedDocumentField(Department)
    responsible = me.StringField()
    is_resp_auto = me.BooleanField(default=True)
    assignees = me.ListField(me.EmbeddedDocumentField(Assignment))
    detailers = me.ListField(me.EmbeddedDocumentField(Detalization))
    extra = me.DictField()
    from_date = me.DateTimeField()
    to_date = me.DateTimeField()
    progress = me.IntField(default=0)
    plan = me.ReferenceField('Plan', required=True)
    child_plans = me.ListField()

    meta = {
        'watch_fields': ['title', 'parent_node']
    }
    
    @property
    def finance_status(self):
        return self.extra.get(u'Сумма', 0)
        
    @property
    def assignees_cap(self):
        return ','.join([assignee.resp.name for assignee in self.assignees])
    
    @property
    def remain_days(self):
        import datetime
        pat = "%s/%s"
        remain = (self.to_date - datetime.datetime.utcnow()).days
        if remain < 0:
            pat %= (0, abs(remain))
        else:
            pat %= (remain, 0)
        return pat
       # return datetime.timedelta(self.to_date, self.from_date)
       

    def save(self, **kwargs):
        print "------------------------------------------------"
        print self.assignees
        print "------------------------------------------------"

        if self.sub_events:
            sum_progress = 0
            self.assignees=self.assignees
            for elem in self.sub_events:
                sum_progress+=int(elem.sub_progress)

            self.progress=sum_progress/len(self.sub_events)
            print self.progress
        else:
            try:
                self.sub_events=[]
                for elem in SUB_EVENTS[self.extra.values()[0]]:
                    self.sub_events.append(SubEvent(name=elem['name'], finishing_form=elem['finish_form'], from_date=self.from_date, to_date=self.to_date))
            except:
                pass
        #self.progress=get_progress(self)

        super(Event, self).save(**kwargs)    


class Plan(Content):
    owner = me.ReferenceField(SynergyUser, required=True)
    detailer = me.EmbeddedDocumentField(ExtUser)
    from_date = me.DateTimeField()
    to_date = me.DateTimeField()
    permissions = me.ListField()
    parent_event = me.ReferenceField(Event, reverse_delete_rule=me.NULLIFY)
    parent_plan = me.ReferenceField('self', reverse_delete_rule=me.NULLIFY)

    @property
    def ownerID(self, value=None):
        if value is None:
            return self.owner.extra and self.owner.extra.get('userid') or None

    def save(self, force_insert=False, cascade=True, **kw):
        # new = ('id' not in self._data or self._created or force_insert)
        super(Plan, self).save(force_insert=force_insert, cascade=cascade, **kw)
        if self.parent_event:
            self.parent_event.update(add_to_set__child_plans=self.seq)

    def delete(self, *a, **kw):
        parent = self.parent_event
        seq = self.seq
        pk = self.id
        if parent and seq in parent.child_plans:
            update = {'pull__child_plans': seq}
            for index, det in enumerate(parent.detailers):
                if det.plan and det.plan.pk == pk:
                    det.work.delete()
                    update['set__detailers__{}__plan'.format(index)] = None
                    update['set__detailers__{}__work'.format(index)] = None
            parent.update(**update)
        super(Plan, self).delete(*a, **kw)


class Work(Content):
    creator = me.ReferenceField(SynergyUser)
    work_id = me.StringField()
    doc_id = me.StringField()
    work_type = me.StringField()
    resp = me.EmbeddedDocumentField(ExtUser)
    coworkers = me.ListField(me.EmbeddedDocumentField(ExtUser))
    completion_form = me.StringField()
    from_date = me.DateTimeField()
    to_date = me.DateTimeField()
    parent_event = me.ReferenceField(
        Event, reverse_delete_rule=me.CASCADE)
    parent_plan = me.ReferenceField(
        Plan, reverse_delete_rule=me.CASCADE)
    data = me.DictField()

    def get_data(self, *seq):
        return rget(self.data, *seq)

    @property
    def author_id(self):
        return self.get_data('author', 'id')
