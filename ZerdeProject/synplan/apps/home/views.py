# -*- coding: utf-8 -*-
from django import http
import simplejson as json
from django.contrib.auth.decorators import login_required
# from django.views.decorators.http import require_POST
from django.conf import settings
from synplan.settings import MEDIA_ROOT, MEDIA_URL
from home.synergy import synergy
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.detail import DetailView
from home.models import Event, Node, Plan, Node
# from home import models

#@csrf_exempt
def get_uploaded(request):
    print "--------------------------------------------------------------------"
    print request.FILES['file']
    file = request.FILES['file']
    print request.POST['index']
    print request.POST['event']
    if file :
        print MEDIA_ROOT
        file_path=MEDIA_ROOT+'/'
        print file_path
        file_path+=file.name
        print file_path
        destination = open(file_path, 'wb+')
        for chunk in file.chunks():
            destination.write(chunk)
        destination.close()
        #filename =file
        #file.save(MEDIA_ROOT, filename)
        return http.HttpResponse("File Successfuly uploaded")

class ReportDetailView(DetailView):
    document=Plan
    
    def get_queryset(self):
        return Plan.objects.filter(id=self.kwargs['pk'])
    def get_context_data(self, *args, **kwargs):
        context = super(ReportDetailView, self).get_context_data(**kwargs)
        child_nodes = Plan.objects.get(id=self.kwargs['pk']).child_nodes
#        context['events']=(Event.objects.filter(plan__in=child_nodes))
        context['events'] = Event.objects.filter(seq__in=child_nodes)
#        print child_nodes
        return context



def JsonR(result):
    return http.HttpResponse(
        json.dumps(result),
        content_type='application/json; charset=utf-8')


@login_required
def grouped_users(request):
    if settings.SYNERGY_DEV:
        username, password = settings.SYNERGY_DEV.split(':')
    elif 'SYNERGY_HASH' in request.session:
        username = '$session'
        password = request.session.get('SYNERGY_HASH')
    else:
        username = '$key'
        password = request.user.auth_key
    groups = synergy.get(
        'groups',
        username=username,
        password=password,
    ).json()
    groups.append({
        'groupID': '0',
        'name': u'Без группы'
    })
    result = []
    user_ids = set()
    for group in groups:
        startRecord = 0
        while True:
            users = synergy.get(
                'users',
                username=username,
                password=password,
                startRecord=startRecord,
                groupID=group.get('groupID'),
                recordsCount=2000,
            ).json()
            for idx, user in enumerate(users):
                user_id = user.get('userID')
                if user_id in user_ids:
                    continue
                user_ids.add(user_id)
                user['group'] = group
                result.append(user)
            if len(users) != 2000:
                break
            startRecord += len(users)
    return JsonR(result)


def file_upload(request):
    print request.POST
    print request.FILES
    return HttpResponse("bla bla")
