from django.conf.urls import patterns, url, include
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = patterns(
    '',
    url(r'^plan/', include('home.urls')),
)
print settings.MEDIA_ROOT
urlpatterns += patterns('',(r'^plan/media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes':True}))
        
if settings.DEBUG:
    if 'rosetta' in settings.INSTALLED_APPS:
        urlpatterns += patterns(
            '', url(r'^rosetta/', include('rosetta.urls'))
        )
    urlpatterns += static(
        settings.STATIC_URL, document_root=settings.STATIC_URL)
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
